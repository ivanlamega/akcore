//***********************************************************************************
//
//	File		:	LkFile.h
//
//	Begin		:	2007-03-19
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************


#pragma once

#include "LkString.h"

#include <io.h>
#include <fcntl.h>
#include <share.h>
#include <sys/stat.h>
#include <sys/types.h>


//-----------------------------------------------------------------------------------
// General File Class
//-----------------------------------------------------------------------------------
class CLkFile
{
public:

	CLkFile(void);


	virtual ~CLkFile(void);


public:

	int				Create(LPCTSTR lpszFileName, int nOperationFlag = _O_CREAT | _O_APPEND | _O_RDWR, int nSharingFlag = _SH_DENYNO, int nPermissionMode = _S_IREAD | _S_IWRITE, bool bAutoClose = false);

	void			Close();

	bool			IsOpened() { return ( HFILE_ERROR != m_hFile ) ? true : false; }

	HFILE			GetFileHandle() { return m_hFile; }

	void			SetAutoClose(bool bAutoClose) { m_bAutoClose = bAutoClose; }


private:

	void			Init();

	void			Destroy();


private:

	HFILE			m_hFile;

	CLkString		m_strFileName;

	bool			m_bAutoClose;
};



//-----------------------------------------------------------------------------------
// File Stream Class
//-----------------------------------------------------------------------------------
class CLkFileStream
{
public:

	CLkFileStream(void);

	virtual ~CLkFileStream(void);


public:

	int				Create(LPCTSTR lpszFileName, int nOperationFlag = _O_CREAT | _O_APPEND | _O_RDWR, int nSharingFlag = _SH_DENYNO, int nPermissionMode = _S_IREAD | _S_IWRITE);

	int				Attach(CLkFile & rFile, LPCTSTR lpszMode = TEXT("w+") );

	FILE *			GetFilePtr() { return m_pFilePtr; }

	void			Close();

	bool			IsOpened() { return ( NULL != m_pFilePtr ) ? true : false; }


private:

	void			Init();

	void			Destroy();


private:

	FILE *			m_pFilePtr;
};
