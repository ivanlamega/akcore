#pragma once

#pragma warning(disable : 4201)

#include "SharedType.h"

#include <float.h>
#include <math.h>
#include <crtdbg.h>


//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
#define FLOAT_EPSILON 0.0001
#define FLOAT_EQ(a, b)	( ( ( b - FLOAT_EPSILON) < a ) && ( a < ( b + FLOAT_EPSILON ) ) )
//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
const float MAX_REAL = FLT_MAX;
const float ZERO_TOLERANCE = 1e-06f;
//-----------------------------------------------------------------------------------


struct sVECTOR2
{
	float x;
	float z;
};

struct sVECTOR3
{
	float x;
	float y;
	float z;
};


class CLkVector
{
public:

	CLkVector();

	CLkVector(sVECTOR3 & rVector);

	CLkVector(sVECTOR2 & rVector);

	CLkVector(float _x, float _y, float _z);

	CLkVector (const float* _afTuple);

	CLkVector (const CLkVector& rhs);


public:

	void			CopyFrom(float _x, float _y, float _z);

	void			CopyFrom(sVECTOR3 * pVector);

	void			CopyFrom(sVECTOR3 & rVector);

	void			CopyFrom(sVECTOR2 * pVector);

	void			CopyFrom(sVECTOR2 & rVector);


	void			CopyTo(float & rX, float &rY, float &rZ);

	void			CopyTo(sVECTOR3 * pVector);

	void			CopyTo(sVECTOR2 * pVector);


	void			CopyTo(sVECTOR3 & rVector);

	void			CopyTo(sVECTOR2 & rVector);



public:


	CLkVector& operator= (const CLkVector& rhs);
	CLkVector& operator= (const sVECTOR3& rhs);
	CLkVector& operator= (const sVECTOR2& rhs);

	bool operator== (const CLkVector& rhs) const;
	bool operator== (const sVECTOR3& rhs) const;
	bool operator!= (const CLkVector& rhs) const;
	bool operator<  (const CLkVector& rhs) const;
	bool operator<= (const CLkVector& rhs) const;
	bool operator>  (const CLkVector& rhs) const;
	bool operator>= (const CLkVector& rhs) const;


	CLkVector operator+ (const CLkVector& rhs) const;
	CLkVector operator- (const CLkVector& rhs) const;
	CLkVector operator+ (float fScalar) const;
	CLkVector operator- (float fScalar) const;
	CLkVector operator* (float fScalar) const;
	CLkVector operator/ (float fScalar) const;
	CLkVector operator- () const;

	CLkVector& operator+= (const CLkVector& rhs);
	CLkVector& operator-= (const CLkVector& rhs);
	CLkVector& operator+= (float fScalar);
	CLkVector& operator-= (float fScalar);
	CLkVector& operator*= (float fScalar);
	CLkVector& operator/= (float fScalar);


	void				Reset();

	float				Length () const;
	float				SquaredLength () const;
	float				Dot (const CLkVector& rhs) const;
	float				Normalize ();
	bool				SafeNormalize();
	float				Normalize (float fLength);

	CLkVector			Cross (const CLkVector& rhs) const;
	CLkVector			UnitCross (const CLkVector& rhs) const;

	bool				IsZero() const;
	bool				IsEqual(CLkVector &rhs) const;
	bool				IsInvalid(bool bCheckY = true) const;



public:

	union {
		struct {
			float		x;
			float		y;
			float		z;
		};
		float			afTuple[3];		
	};


public:

	// special vectors
	static CLkVector				ZERO;
	static CLkVector				UNIT_X;
	static CLkVector				UNIT_Y;
	static CLkVector				UNIT_Z;
	static CLkVector				INVALID;
	static CLkVector				INVALID_XZ;

};


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector::CLkVector()
:x(0.0f), y(0.0f), z(0.0f)
{
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector::CLkVector(sVECTOR3 & rVector)
:x(rVector.x), y(rVector.y), z(rVector.z)
{
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector::CLkVector(sVECTOR2 & rVector)
:x(rVector.x), y(0.0f), z(rVector.z)
{
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector::CLkVector(float _x, float _y, float _z)
:x(_x), y(_y), z(_z)
{
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector::CLkVector(const float* _afTuple)
{
	afTuple[0] = _afTuple[0];
	afTuple[1] = _afTuple[1];
	afTuple[2] = _afTuple[2];
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector::CLkVector(const CLkVector& rhs)
:x(rhs.x), y(rhs.y), z(rhs.z)
{
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkVector::CopyFrom(float _x, float _y, float _z)
{
	x = _x;
	y = _y;
	z = _z;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkVector::CopyFrom(sVECTOR3 * pVector)
{
	x = pVector->x;
	y = pVector->y;
	z = pVector->z;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkVector::CopyFrom(sVECTOR3 & rVector)
{
	x = rVector.x;
	y = rVector.y;
	z = rVector.z;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkVector::CopyFrom(sVECTOR2 * pVector)
{
	x = pVector->x;
	y = 0.0f;
	z = pVector->z;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkVector::CopyFrom(sVECTOR2 & rVector)
{
	x = rVector.x;
	y = 0.0f;
	z = rVector.z;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkVector::CopyTo(float & rX, float & rY, float &rZ)
{
	rX = x;
	rY = y;
	rZ = z;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkVector::CopyTo(sVECTOR3 * pVector)
{
	pVector->x = x;
	pVector->y = y;
	pVector->z = z;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkVector::CopyTo(sVECTOR2 * pVector)
{
	pVector->x = x;
	pVector->z = z;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkVector::CopyTo(sVECTOR3 & rVector)
{
	rVector.x = x;
	rVector.y = y;
	rVector.z = z;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkVector::CopyTo(sVECTOR2 & rVector)
{
	rVector.x = x;
	rVector.z = z;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector& CLkVector::operator= (const CLkVector& rhs)
{
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector& CLkVector::operator= (const sVECTOR3& rhs)
{
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector& CLkVector::operator= (const sVECTOR2& rhs)
{
	x = rhs.x;
	y = 0.0f;
	z = rhs.z;

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline bool CLkVector::operator== (const CLkVector& rhs) const
{
	return memcmp( afTuple, rhs.afTuple, sizeof(afTuple) ) == 0;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline bool CLkVector::operator== (const sVECTOR3& rhs) const
{
	return memcmp( afTuple, &rhs, sizeof(afTuple) ) == 0;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline bool CLkVector::operator!= (const CLkVector& rhs) const
{
	return memcmp( afTuple, rhs.afTuple, sizeof(afTuple) ) != 0;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline bool CLkVector::operator<  (const CLkVector& rhs) const
{
	return memcmp( afTuple, rhs.afTuple, sizeof(afTuple) ) < 0;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline bool CLkVector::operator<= (const CLkVector& rhs) const
{
	return memcmp( afTuple, rhs.afTuple, sizeof(afTuple) ) <= 0;
}

//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline bool CLkVector::operator>  (const CLkVector& rhs) const
{
	return memcmp( afTuple, rhs.afTuple, sizeof(afTuple) ) > 0;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline bool CLkVector::operator>= (const CLkVector& rhs) const
{
	return memcmp( afTuple, rhs.afTuple, sizeof(afTuple) ) >= 0;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector CLkVector::operator+ (const CLkVector& rhs) const
{
	return CLkVector( x + rhs.x , y + rhs.y, z + rhs.z );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector CLkVector::operator- (const CLkVector& rhs) const
{
	return CLkVector( x - rhs.x , y - rhs.y, z - rhs.z );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector CLkVector::operator+ (float fScalar) const
{
	return CLkVector( x + fScalar , y + fScalar, z + fScalar );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector CLkVector::operator- (float fScalar) const
{
	return CLkVector( x - fScalar , y - fScalar, z - fScalar );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector CLkVector::operator* (float fScalar) const
{
	return CLkVector( x * fScalar , y * fScalar, z * fScalar );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector CLkVector::operator/ (float fScalar) const
{
	return CLkVector( x / fScalar , y / fScalar, z / fScalar );
}

//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector CLkVector::operator- () const
{
	return CLkVector( -x , -y, -z );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector& CLkVector::operator+= (const CLkVector& rhs)
{
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector& CLkVector::operator-= (const CLkVector& rhs)
{
	x -= rhs.x;
	y -= rhs.y;
	z -= rhs.z;

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector& CLkVector::operator+= (float fScalar)
{
	x += fScalar;
	y += fScalar;
	z += fScalar;

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector& CLkVector::operator-= (float fScalar)
{
	x -= fScalar;
	y -= fScalar;
	z -= fScalar;

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector& CLkVector::operator*= (float fScalar)
{
	x *= fScalar;
	y *= fScalar;
	z *= fScalar;

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector& CLkVector::operator/= (float fScalar)
{
	if( fScalar != (float) 0.0f ) 
	{
		float fInvScalar = ((float)1.0f) / fScalar;

		x *= fInvScalar;
		y *= fInvScalar;
		z *= fInvScalar;
	}
	else
	{
		x = MAX_REAL;
		y = MAX_REAL;
		z = MAX_REAL;
	}


	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkVector::Reset()
{
	x = y = z = 0.0f;
}

//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline float CLkVector::Length () const
{
	return ::sqrt( afTuple[0] * afTuple[0] + afTuple[1] * afTuple[1] + afTuple[2] * afTuple[2] );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline float CLkVector::SquaredLength () const
{
	return afTuple[0] * afTuple[0] + afTuple[1] * afTuple[1] + afTuple[2] * afTuple[2];

}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline float CLkVector::Dot (const CLkVector& rhs) const
{
	return afTuple[0] * rhs.afTuple[0] + afTuple[1] * rhs.afTuple[1] + afTuple[2] * rhs.afTuple[2];
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline float CLkVector::Normalize ()
{
	float fLength = Length();

	return Normalize( fLength );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline bool CLkVector::SafeNormalize()
{
	float fLength = Length();

	if (ZERO_TOLERANCE > fLength)
	{
		return false;
	}

	Normalize(fLength);

	return true;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline float CLkVector::Normalize(float fLength)
{
	if ( fLength > ZERO_TOLERANCE )
	{
		float fInvLength = ( 1.0f ) / fLength;

		afTuple[0] *= fInvLength;
		afTuple[1] *= fInvLength;
		afTuple[2] *= fInvLength;
	}
	else
	{
		fLength = 0.0f;
		afTuple[0] = 0.0f;
		afTuple[1] = 0.0f;
		afTuple[2] = 0.0f;
	}


	if( Length() < 0.8f )
	{
		//_ASSERT( 0 );
	}


	return fLength;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector CLkVector::Cross (const CLkVector& rhs) const
{
	return CLkVector(	afTuple[1]*rhs.afTuple[2] - afTuple[2]*rhs.afTuple[1],
						afTuple[2]*rhs.afTuple[0] - afTuple[0]*rhs.afTuple[2],
						afTuple[0]*rhs.afTuple[1] - afTuple[1]*rhs.afTuple[0] );

}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkVector CLkVector::UnitCross (const CLkVector& rhs) const
{
	CLkVector kCross(	afTuple[1]*rhs.afTuple[2] - afTuple[2]*rhs.afTuple[1],
						afTuple[2]*rhs.afTuple[0] - afTuple[0]*rhs.afTuple[2],
						afTuple[0]*rhs.afTuple[1] - afTuple[1]*rhs.afTuple[0]);

	kCross.Normalize();

	return kCross;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline bool CLkVector::IsZero() const
{
	if( afTuple[0] || afTuple[1] || afTuple[2] )
	{
		return false;
	}

	return true;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline bool CLkVector::IsInvalid(bool bCheckY) const
{
	if( false == bCheckY )
	{
		if( INVALID_FLOAT == afTuple[0] && INVALID_FLOAT == afTuple[2] )
		{
			return true;
		}
	}
	else
	{
		if( INVALID_FLOAT == afTuple[0] && INVALID_FLOAT == afTuple[1] && INVALID_FLOAT == afTuple[2] )
		{
			return true;
		}
	}

	return false;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline bool CLkVector::IsEqual(CLkVector &rhs) const
{
	if( FLOAT_EQ( afTuple[ 0 ], rhs.afTuple[ 0 ] ) &&
		FLOAT_EQ( afTuple[ 1 ], rhs.afTuple[ 1 ] ) &&
		FLOAT_EQ( afTuple[ 2 ], rhs.afTuple[ 2 ] ) )
	{
		return true;
	}

	return false;
}