//***********************************************************************************
//
//	File		:	LkIniFile.h
//
//	Begin		:	2006-01-05
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Myoung Jin, Choi		( yoshiki@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#ifndef __NTLINIFILE_H__
#define __NTLINIFILE_H__

#include "LkString.h"

class CLkIniFile
{
public:

	CLkIniFile(void);

	virtual ~CLkIniFile(void);


	int							Create(const char * lpszFullName);

	int							Create(const char * lpszPathName, const char * lpszFileName);


public:


	CLkString					Read(const char *group, const char *key);

	bool						Read(const char *group, const char *key, CLkString &val);


	bool						Read(const char *group, const char *key, bool &flag);	

	bool						Read(const char *group, const char *key, char &num);

	bool						Read(const char *group, const char *key, short &num);

	bool						Read(const char *group, const char *key, int &num);

	bool						Read(const char *group, const char *key, float &num);

	bool						Read(const char *group, const char *key, unsigned char &num);

	bool						Read(const char *group, const char *key, unsigned short &num);

	bool						Read(const char *group, const char *key, unsigned int &num);

	bool						Read(const char *pszGroup, const char *pszKey, DWORD &dwNumber);


public:

	const char *				GetConfigFileName() { return m_strConfigFileName.c_str(); }

	const char *				GetLastReadGroup() { return m_strLastReadGroup.c_str(); }

	const char *				GetLastReadKey() { return m_strLastReadKey.c_str(); }

private:

	CLkString					m_strConfigFileName;

	CLkString					m_strLastReadGroup;

	CLkString					m_strLastReadKey;

};

#endif // __NTLINIFILE_H__
