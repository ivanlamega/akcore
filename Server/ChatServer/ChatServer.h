#pragma once

#include "LkSfx.h"
#include "LkPacketEncoder_RandKey.h"
#include "mysqlconn_wrapper.h"

#include "LkFile.h"

#include "LkPacketUT.h"
#include "LkPacketTU.h"
#include "ChatFunctions.h"
#include "ResultCode.h"

#include <iostream>
#include <map>
#include <list>

enum APP_LOG
{
	PRINT_APP = 2,
};
enum CHAT_SESSION
{
	SESSION_CLIENT,
	SESSION_SERVER_ACTIVE,
};
struct sSERVERCONFIG
{
	CLkString		strClientAcceptAddr;
	WORD			wClientAcceptPort;
	CLkString		Host;
	CLkString		User;
	CLkString		Password;
	CLkString		Database;
};

const DWORD					MAX_NUMOF_GAME_CLIENT = 3;
const DWORD					MAX_NUMOF_SERVER = 1;
const DWORD					MAX_NUMOF_SESSION = MAX_NUMOF_GAME_CLIENT + MAX_NUMOF_SERVER;
static unsigned int			m_uiSerialId = 0;

class CChatServer;

//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
class CClientSession : public CLkSession
{
public:
	RwUInt32 AcquireSerialId(void)
	{
		if(m_uiSerialId++)
		{
			if(m_uiSerialId == 0xffffffff)//INVALID_SERIAL_ID)
				m_uiSerialId = 0;
		}
		return m_uiSerialId;
	}
	CClientSession(bool bAliveCheck = false, bool bOpcodeCheck = false)
		:CLkSession( SESSION_CLIENT )
	{
		SetControlFlag( CONTROL_FLAG_USE_SEND_QUEUE );

		if( bAliveCheck )
		{
			SetControlFlag( CONTROL_FLAG_CHECK_ALIVE );
		}
		if( bOpcodeCheck )
		{
			SetControlFlag( CONTROL_FLAG_CHECK_OPCODE );
		}

		SetPacketEncoder( &m_packetEncoder );
	}

	~CClientSession();

public:
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CLkPacket * pPacket);

	unsigned int				GetavatarHandle() { return avatarHandle; }
	int							GetAccountId() { return accountID; }
	int							GetCharacterId() { return characterID; }
	int							GetGuildID() { return guildID; }
	std::string					GetCharName() { return charName; }
	std::string					GetGuildName() { return guildName; }

	// Packet functions
	void						SendEnterChat(CLkPacket * pPacket, CChatServer * app);
	void						SendLoadGuildInfo(CLkPacket * pPacket, CChatServer * app);
	void						SendLoadGuildMember(CLkPacket * pPacket, CChatServer * app);
	void						SendGuildChangeNotice(CLkPacket * pPacket, CChatServer * app);
	void						SendDisbandGuild(CLkPacket * pPacket, CChatServer * app);
	void						SendLeaveGuildReq(CLkPacket * pPacket, CChatServer * app);
	void						SendKickFromGuildReq(CLkPacket * pPacket, CChatServer * app);
	void						SendNewSecondGuildMaster(CLkPacket * pPacket, CChatServer * app);
	void						SendRemoveSecondGuildMaster(CLkPacket * pPacket, CChatServer * app);
	void						SendUpdateGuildMaster(CLkPacket * pPacket, CChatServer * app);
	//Friend and Blacklist functions
	void						SendFriendList(CLkPacket * pPacket, CChatServer * app);
	void						SendAddFriend(CLkPacket * pPacket, CChatServer * app);
	void						SendDelFriend(CLkPacket * pPacket, CChatServer * app);
	void						SendMoveFriend(CLkPacket * pPacket, CChatServer * app);
	void						SendBlackListAdd(CLkPacket * pPacket, CChatServer * app);
	void						SendBlackListDel(CLkPacket * pPacket, CChatServer * app);
	//Rank Battle
	void						SendRankList(CLkPacket * pPacket, CChatServer * app);
	//Chat Channel Functions
	void						SendSayReq(CLkPacket * pPacket, CChatServer * app);
	void						SendShoutReq(CLkPacket * pPacket, CChatServer * app);
	void						SendWhisperReq(CLkPacket * pPacket, CChatServer * app);
	void						SendTradeChatReq(CLkPacket * pPacket, CChatServer * app);
	void						SendPartyChatReq(CLkPacket * pPacket, CChatServer * app);
	void						SendGuildChatReq(CLkPacket * pPacket, CChatServer * app);
	// End Packet functions
private:
	CLkPacketEncoder_RandKey	m_packetEncoder;

private:
	unsigned int				avatarHandle;
	int							accountID;
	int							characterID;
	int							guildID;
	std::string					charName;
	std::string					guildName;

};

//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//

class CChatSessionFactory : public CLkSessionFactory
{
public:

	CLkSession * CreateSession(SESSIONTYPE sessionType)
	{
		CLkSession * pSession = NULL;
		switch( sessionType )
		{
		case SESSION_CLIENT: 
			{
				pSession = new CClientSession;
			}
			break;

		default:
			break;
		}

		return pSession;
	}
};

//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//

class CChatServer : public CLkServerApp
{
public:

	int	OnInitApp()
	{
		m_nMaxSessionCount = MAX_NUMOF_SESSION;

		m_pSessionFactory =  new CChatSessionFactory;
		if( NULL == m_pSessionFactory )
		{
			return LK_ERR_SYS_MEMORY_ALLOC_FAIL;
		}

		return LK_SUCCESS;
	}
	const char*		GetConfigFileHost()
	{
		return m_config.Host.c_str();
	}
	const char*		GetConfigFileUser()
	{
		return m_config.User.c_str();
	}
	const char*		GetConfigFilePassword()
	{
		return m_config.Password.c_str();
	}
	const char*		GetConfigFileDatabase()
	{
		return m_config.Database.c_str();
	}
	int	OnCreate()
	{
		int rc = LK_SUCCESS;
		rc = m_clientAcceptor.Create(	m_config.strClientAcceptAddr.c_str(), m_config.wClientAcceptPort, SESSION_CLIENT, 
										MAX_NUMOF_GAME_CLIENT, 5, 2, MAX_NUMOF_GAME_CLIENT );
		if ( LK_SUCCESS != rc )
		{
			return rc;
		}
		rc = m_network.Associate( &m_clientAcceptor, true );
		if( LK_SUCCESS != rc )
		{
			return rc;
		}
		return LK_SUCCESS;
	}
	void	OnDestroy()
	{
	}
	int	OnCommandArgument(int argc, _TCHAR* argv[])
	{
		return LK_SUCCESS;
	}
	int	OnConfiguration(const char * lpszConfigFile)
	{
		CLkIniFile file;
		int rc = file.Create( lpszConfigFile );
		if( LK_SUCCESS != rc )
		{
			return rc;
		}
		if( !file.Read("Chat Server", "Address", m_config.strClientAcceptAddr) )
		{
			return LK_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if( !file.Read("Chat Server", "Port",  m_config.wClientAcceptPort) )
		{
			return LK_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if( !file.Read("DATABASE", "Host",  m_config.Host) )
		{
			return LK_ERR_DBC_HANDLE_ALREADY_ALLOCATED;
		}
		if( !file.Read("DATABASE", "User",  m_config.User) )
		{
			return LK_ERR_SYS_MEMORY_ALLOC_FAIL;
		}
		if( !file.Read("DATABASE", "Password",  m_config.Password) )
		{
			return LK_ERR_SYS_LOG_SYSTEM_INITIALIZE_FAIL;
		}
		if( !file.Read("DATABASE", "Db",  m_config.Database) )
		{
			return LK_ERR_DBC_CONNECTION_CONNECT_FAIL;
		}
		return LK_SUCCESS;
	}
	int	OnAppStart()
	{
		return LK_SUCCESS;
	}
	void	Run()
	{
		DWORD dwTickCur, dwTickOld = ::GetTickCount();

		while( IsRunnable() )
		{		
			dwTickCur = ::GetTickCount();
			if( dwTickCur - dwTickOld >= 10000 )
			{
			//	LK_PRINT(PRINT_APP, "Chat Server Run()");
				dwTickOld = dwTickCur;
			}
			Sleep(2);
		}
	}

private:
	CLkAcceptor				m_clientAcceptor;
	CLkLog  					m_log;
	sSERVERCONFIG				m_config;
public:
	MySQLConnWrapper *			db;
	ChatFunctionsClass *		csf;
public:
	bool						AddUser(const char * lpszUserID, CClientSession * pSession)
	{
		if( false == m_userList.insert( USERVAL(CLkString(lpszUserID), pSession)).second )
		{
			return false;
		}
		return true;		
	}
	void						RemoveUser(const char * lpszUserID)
	{
		m_userList.erase( CLkString(lpszUserID) );
	}
	bool						FindUser(const char * lpszUserID)
	{
		USERIT it = m_userList.find( CLkString(lpszUserID) );
		if( it == m_userList.end() )
			return false;

		return true;
	}
	CClientSession*						FindUserSession(const char * lpszUserID)
	{
		USERIT it = m_userList.find(CLkString(lpszUserID));
		if (it == m_userList.end())
			return NULL;

		return it->second;
	}
	void						UserBroadcast(CLkPacket * pPacket)
	{
		for( USERIT it = m_userList.begin(); it != m_userList.end(); it++ )
		{
			it->second->PushPacket( pPacket );
		}
	}
	void						UserBroadcastothers(CLkPacket * pPacket, CClientSession * pSession)
	{
		for( USERIT it = m_userList.begin(); it != m_userList.end(); it++ )
		{
			if(pSession->GetavatarHandle() != it->second->GetavatarHandle())
			it->second->PushPacket( pPacket );
		}
	}
	typedef std::map<CLkString, CClientSession*> USERLIST;
	typedef USERLIST::value_type USERVAL;
	typedef USERLIST::iterator USERIT;
	USERLIST					m_userList;
};