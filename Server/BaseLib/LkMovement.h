#pragma once

#include "Vector.h"

#define LK_MAX_RADIUS_OF_VISIBLE_AREA		(100.0f)
#define LK_MAX_NUMBER_OF_PLAYERS_IN_VISIBLE_AREA		(100)
#define LK_REQUIRED_TIME_FOR_COMPLETE_CIRCULAR_MOVEMENT_IN_MILLISECS		(3000)
#define LK_DEFAULT_MOVEMENT_SPEED		(7.0f)
#define LK_BACKWARD_MOVEMENT_SPEED_RATE		(0.5f)		// 50% of forward movement speed
#define LK_DEFAULT_MOVEMENT_SPEED_B		(LK_DEFAULT_MOVEMENT_SPEED * LK_BACKWARD_MOVEMENT_SPEED_RATE)

#define LK_DASH_DISTANCE_F		(15.0f)
#define LK_DASH_DISTANCE_B		(10.0f)
#define LK_DASH_DISTANCE_L		(10.0f)
#define LK_DASH_DISTANCE_R		(10.0f)
#define LK_DASH_SPEED			(25.0f)
#define LK_MAX_DASH_DISTANCE_FOR_SKILL		(25.0f)
#define LK_ACTIVE_DASH_SPEED				(40.0f)
#define DBO_DASH_INVERVAL_WITHOUT_EP_LOSS_IN_MILLISECS						(4000)

#define DBO_VEHICLE_TURNING_RATIO		(1.0f)		// no turning speed change when pc is on the vehicle
#define DBO_SWIMMING_SPEED_RATIO		(0.5f)		// 50% of movement speed on land.
#define DBO_FALLING_SPEED_RATIO			(0.5f)		// 50% of movement speed on land.

#define DBO_DISTANCE_CHECK_TOLERANCE		(10.0f)

const BYTE DBO_MAX_NEXT_DEST_LOC_COUNT = 10;

enum ELkMovementDirection
{
	LK_MOVE_NONE = 0,

	LK_MOVE_F,		// Forward
	LK_MOVE_B,		// Backward
	// DON'T delete these lines permanently!
	// 완전히 삭제하지 마시오!
	// by YOSHIKI(2006-09-22)
//	LK_MOVE_L,		// Left
//	LK_MOVE_R,		// Right

//	LK_MOVE_F_L,		// Forward + Left
//	LK_MOVE_F_R,		// Forward + Right
//	LK_MOVE_B_L,		// Backward + Left
//	LK_MOVE_B_R,		// Backward + Right

	LK_MOVE_TURN_L,		// Turning Left
	LK_MOVE_TURN_R,		// Turning Right

	LK_MOVE_F_TURN_L,		// Forward + Turning Left
	LK_MOVE_F_TURN_R,		// Forward + Turning Right
	LK_MOVE_B_TURN_L,		// Backward + Turning Left
	LK_MOVE_B_TURN_R,		// Backward + Turning Right

	LK_MOVE_F_TURN_L_JUMP,		// Forward + Turning Left + Jump
	LK_MOVE_F_TURN_R_JUMP,		// Forward + Turning Right + Jump
	LK_MOVE_B_TURN_L_JUMP,		// Backward + Turning Left + Jump
	LK_MOVE_B_TURN_R_JUMP,		// Backward + Turning Right + Jump

	LK_MOVE_MOUSE_MOVEMENT,		// Mouse Movement
	LK_MOVE_FOLLOW_MOVEMENT,		// Follow Movement

	LK_MOVE_DASH_F,				// Dash + Forward
	LK_MOVE_DASH_B,				// Dash + Backward
	LK_MOVE_DASH_L,				// Dash + Left
	LK_MOVE_DASH_R,				// Dash + Right

	LK_MOVE_COUNT,

	LK_MOVE_UNKNOWN = 0xFF,

	LK_MOVE_KEYBOARD_FIRST = LK_MOVE_F,
	LK_MOVE_KEYBOARD_LAST = LK_MOVE_B_TURN_R_JUMP,

	LK_MOVE_DASH_PASSIVE_FIRST = LK_MOVE_DASH_F,
	LK_MOVE_DASH_PASSIVE_LAST = LK_MOVE_DASH_R,

	LK_MOVE_FIRST = LK_MOVE_NONE,
	LK_MOVE_LAST = LK_MOVE_DASH_R
};

enum ELkMovementFlag
{
	LK_MOVE_FLAG_RUN = 0x01 << 0
};

enum ELkMovementStatus
{
	LK_MOVE_STATUS_NONE = 0,

	LK_MOVE_STATUS_JUMP = 1,			// JUMPING
	LK_MOVE_STATUS_FALLING = 2,		// FALLING

	LK_MOVE_STATUS_UNKNOWN = 0xFFui8,

	LK_MOVE_STATUS_FIRST = LK_MOVE_STATUS_NONE,
	LK_MOVE_STATUS_LAST = LK_MOVE_STATUS_FALLING
};

bool LkSin(float fX, float fZ, float* pfSin);
bool LkCos(float fX, float fZ, float* pfCos);

bool RotateVector45DegreeToLeft(float fX, float fZ, float* pfResultX, float* pfResultZ);
bool RotateVector45DegreeToRight(float fX, float fZ, float* pfResultX, float* pfResultZ);
bool RotateVector90DegreeToLeft(float fX, float fZ, float* pfResultX, float* pfResultZ);
bool RotateVector90DegreeToRight(float fX, float fZ, float* pfResultX, float* pfResultZ);
bool RotateVector135DegreeToLeft(float fX, float fZ, float* pfResultX, float* pfResultZ);
bool RotateVector135DegreeToRight(float fX, float fZ, float* pfResultX, float* pfResultZ);
bool RotateVector180Degree(float fX, float fZ, float* pfResultX, float* pfResultZ);
bool RotateVector(float fX, float fZ, float fAngleInRadian, float* pfResultX, float* pfResultZ);
bool GetVectorWithDegree(int nDegree, float* pfVectorX, float* pfVectorZ);

CLkVector& RotateVector45DegreeToLeft(CLkVector* pVector);
CLkVector& RotateVector45DegreeToRight(CLkVector* pVector);
CLkVector& RotateVector90DegreeToLeft(CLkVector* pVector);
CLkVector& RotateVector90DegreeToRight(CLkVector* pVector);
CLkVector& RotateVector135DegreeToLeft(CLkVector* pVector);
CLkVector& RotateVector135DegreeToRight(CLkVector* pVector);
CLkVector& RotateVector180Degree(CLkVector* pVector);
CLkVector& RotateVector(CLkVector* pVector, float fAngleInRadian);

bool LkGetDestination(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
						float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
						float fDestinationX, float fDestinationY, float fDestinationZ,
						BYTE byMoveDirection, DWORD dwDeltaTimeInMillisecs,
						float fAttackDistance,
						float* pfNewHeadingVectorX, float* pfNewHeadingVectorZ,
						float* pfDestinationX, float* pfDestinationY, float* pfDestinationZ,
						float fTurningSpeedRatio);

// It's assumed that byMoveDirecton is valid. Please check if it has valid value before calling this function.
// by YOSHIKI(2006-07-06)
void LkGetDestination_Keyboard(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
								float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
								BYTE byMoveDirection, DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
								CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Keyboard_NONE(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
										float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
										DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
										CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Keyboard_F(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
									float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
									DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
									CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Keyboard_B(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
									float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
									DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
									CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Keyboard_L(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
									float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
									DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
									CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Keyboard_R(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
									float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
									DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
									CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Keyboard_F_L(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
									float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
									DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
									CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Keyboard_F_R(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
									float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
									DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
									CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Keyboard_B_L(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
									float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
									DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
									CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Keyboard_B_R(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
									float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
									DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
									CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Keyboard_TURN_L(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
										float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
										DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
										CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Keyboard_TURN_R(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
										float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
										DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
										CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Keyboard_F_TURN_L(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
											float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
											DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
											CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Keyboard_F_TURN_R(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
											float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
											DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
											CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Keyboard_B_TURN_L(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
											float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
											DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
											CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Keyboard_B_TURN_R(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
											float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
											DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
											CLkVector* pNewHeadingVector, CLkVector* pDestination);

void LkGetDestination_Jump(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
							float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
							float fJumpDirectionX, float fJumpDirectionZ,
							BYTE byMoveDirection, DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
							CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Jump_NONE(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
									float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
									float fJumpDirectionX, float fJumpDirectionZ,
									DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
									CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Jump_F(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
								float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
								float fJumpDirectionX, float fJumpDirectionZ,
								DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
								CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Jump_B(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
								float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
								float fJumpDirectionX, float fJumpDirectionZ,
								DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
								CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Jump_L(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
								float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
								float fJumpDirectionX, float fJumpDirectionZ,
								DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
								CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Jump_R(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
								float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
								float fJumpDirectionX, float fJumpDirectionZ,
								DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
								CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Jump_TURN_L(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
									float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
									float fJumpDirectionX, float fJumpDirectionZ,
									DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
									CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Jump_TURN_R(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
									float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
									float fJumpDirectionX, float fJumpDirectionZ,
									DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
									CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Jump_F_TURN_L(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
										float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
										float fJumpDirectionX, float fJumpDirectionZ,
										DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
										CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Jump_F_TURN_R(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
										float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
										float fJumpDirectionX, float fJumpDirectionZ,
										DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
										CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Jump_B_TURN_L(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
										float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
										float fJumpDirectionX, float fJumpDirectionZ,
										DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
										CLkVector* pNewHeadingVector, CLkVector* pDestination);
void LkGetDestination_Jump_B_TURN_R(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
										float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
										float fJumpDirectionX, float fJumpDirectionZ,
										DWORD dwDeltaTimeInMillisecs, float fTurningSpeedRatio,
										CLkVector* pNewHeadingVector, CLkVector* pDestination);

void LkGetDestination_Mouse(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
								float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
								float fDestinationX, float fDestinationY, float fDestinationZ,
								DWORD dwDeltaTimeInMillisecs,
								CLkVector* pNewHeadingVector, CLkVector* pDestination);

void LkGetDestination_Follow(float fCurrentHeadingVectorX, float fCurrentHeadingVectorZ, float fSpeedInSecs,
								float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
								float fDestinationX, float fDestinationY, float fDestinationZ,
								DWORD dwDeltaTimeInMillisecs,
								float fTargetDistance,
								CLkVector* pNewHeadingVector, CLkVector* pDestination);

void LkGetDestination_Dash(float fCurrentMoveVectorX, float fCurrentMoveVectorZ, float fSpeedInSecs,
							float fCurrentPositionX, float fCurrentPositionY, float fCurrentPositionZ,
							float fDestinationX, float fDestinationY, float fDestinationZ,
							DWORD dwDeltaTimeInMillisecs,
							CLkVector* pDestination);

float LkGetDistance(float fPositionX1, float fPositionZ1, float fPositionX2, float fPositionZ2);
float LkGetRandomPosition(float fPosition1, float fPosition2);
bool LkIsInsideOrNot(float fPositionX,float fPositionZ, float fDestX, float fDestZ, float fInputX, float fInputZ);