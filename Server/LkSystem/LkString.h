//***********************************************************************************
//
//	File		:	LkString.h
//
//	Begin		:	2006-05-17
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#pragma once

#include <string>

class CLkString
{
public:
	CLkString() {}
	CLkString(const char * lpszStr):m_str(lpszStr) {}
	CLkString(const WCHAR * pwszString);

public:
	std::string & GetString(void) { return m_str; }
	CLkString & operator=(const char * lpszStr);
	CLkString & operator=(const WCHAR * pwszString);
	
	int Format(const char *format, ...);
	const char* c_str(void) const { return m_str.c_str(); }
	void clear(void) { m_str.clear(); }

	bool operator<(const CLkString &right) const
	{
		if ( m_str<right.m_str ) return true;
		else return false;
	}

private:
	 std::string m_str;
};

