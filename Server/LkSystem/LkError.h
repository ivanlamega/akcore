//***********************************************************************************
//
//	File		:	LkError.h
//
//	Begin		:	2005-11-30
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	Error ó�� ���� 
//
//***********************************************************************************

#ifndef __NTLERROR_H__
#define __NTLERROR_H__

#include "LkString.h"

#define LK_DEFINE_ERROR(x)		x,

enum
{
	LK_ERR_BEGIN = 100000,

	#include "LkErrorCodes.h"

	MAX_LK_ERROR
};

void LkGetErrorString( CLkString & rStrError,int iErrorCode );

const char * LkGetErrorMessage( int iErrorCode );

#endif // __NTLERROR_H__
