//-----------------------------------------------------------------------------------
//		Chat Server by Daneos @ Ragezone 
//-----------------------------------------------------------------------------------

#include "stdafx.h"
#include "ChatServer.h"


using namespace std;

//-----------------------------------------------------------------------------------
CClientSession::~CClientSession()
{
	//LK_PRINT(PRINT_APP, "CClientSession Destructor Called");
}


int CClientSession::OnAccept()
{
	//LK_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	avatarHandle = AcquireSerialId();
	return LK_SUCCESS;
}


void CClientSession::OnClose()
{
	//LK_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	CChatServer * app = (CChatServer*) LkSfxGetApp();
	app->RemoveUser(this->GetCharName().c_str());
}

int CClientSession::OnDispatch(CLkPacket * pPacket)
{
	CChatServer * app = (CChatServer*) LkSfxGetApp();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	if(pHeader->wOpCode > 1) {
		//printf("~~~ opcode %i received ~~~ \n", pHeader->wOpCode);
	}
	switch( pHeader->wOpCode )
	{
	case UT_ENTER_CHAT:
	{
		CClientSession::SendEnterChat(pPacket, app);
		CClientSession::SendFriendList(pPacket, app);
		CClientSession::SendLoadGuildInfo(pPacket, app);
		/*CClientSession::SendLoadGuildMember(pPacket, app);*/
	}
		break;
	case UT_CHAT_MESSAGE_SAY:
	{
		CClientSession::SendSayReq(pPacket, app);
	}
		break;
	case UT_CHAT_MESSAGE_SHOUT:
	{
		CClientSession::SendShoutReq(pPacket, app);
	}
		break;
	case UT_CHAT_MESSAGE_WHISPER:
	{
		CClientSession::SendWhisperReq(pPacket, app);
	}
		break;
	case UT_CHAT_MESSAGE_PARTY:
	{
		printf("UT_CHAT_MESSAGE_PARTY");
		//CClientSession::SendPartyChatReq(pPacket, app);
	}
		break;
	case UT_CHAT_MESSAGE_GUILD:
	{
		printf("UT_CHAT_MESSAGE_GUILD");
	//	CClientSession::SendGuildChatReq(pPacket, app);
	}
		break;

	case UT_GUILD_DISBAND_REQ:
	{
		CClientSession::SendDisbandGuild(pPacket, app);
	}
		break;
	case UT_GUILD_DISBAND_CANCEL_REQ:
	{
		printf("UT_GUILD_DISBAND_CANCEL_REQ");
	}
		break;
	case UT_GUILD_RESPONSE_INVITATION:
	{
		printf("UT_GUILD_RESPONSE_INVITATION");
	}
		break;
	case UT_GUILD_LEAVE_REQ:
	{
		CClientSession::SendLeaveGuildReq(pPacket, app);
	}
		break;
	case UT_GUILD_KICK_OUT_REQ:
	{
		CClientSession::SendKickFromGuildReq(pPacket, app);
	}
		break;
	case UT_GUILD_APPOINT_SECOND_MASTER_REQ:
	{
		CClientSession::SendNewSecondGuildMaster(pPacket, app);
	}
		break;
	case UT_GUILD_DISMISS_SECOND_MASTER_REQ:
	{
		CClientSession::SendRemoveSecondGuildMaster(pPacket, app);
	}
		break;
	case UT_GUILD_CHANGE_GUILD_MASTER_REQ:
	{
		CClientSession::SendUpdateGuildMaster(pPacket, app);
	}
		break;
	case UT_CHAT_MESSAGE_PRIVATESHOP_BUSINESS:
	{
		printf("UT_CHAT_MESSAGE_PRIVATESHOP_BUSINESS");
	}
		break;
	case UT_FRIEND_ADD_REQ:
	{
		printf("UT_FRIEND_ADD_REQ");
		CClientSession::SendAddFriend(pPacket, app);
	}
		break;
	case UT_FRIEND_DEL_REQ:
	{
		printf("UT_FRIEND_DEL_REQ");
		CClientSession::SendDelFriend(pPacket, app);
	}
		break;
	case UT_FRIEND_MOVE_REQ:
	{
		printf("UT_FRIEND_MOVE_REQ");
		CClientSession::SendMoveFriend(pPacket, app);
	}
		break;
	case UT_FRIEND_BLACK_ADD_REQ:
	{
		printf("UT_FRIEND_BLACK_ADD_REQ");
		CClientSession::SendBlackListAdd(pPacket, app);
	}
		break;
	case UT_FRIEND_BLACK_DEL_REQ:
	{
		printf("UT_FRIEND_BLACK_DEL_REQ");
		CClientSession::SendBlackListDel(pPacket, app);
	}
		break;
	case UT_RANKBATTLE_RANK_LIST_REQ:
	{
		printf("UT_RANKBATTLE_RANK_LIST_REQ");
		CClientSession::SendRankList(pPacket, app);
	}
		break;
	case UT_RANKBATTLE_RANK_FIND_CHARACTER_REQ:
	{
		printf("UT_RANKBATTLE_RANK_FIND_CHARACTER_REQ");
	}
		break;
	case UT_RANKBATTLE_RANK_COMPARE_DAY_REQ:
	{
		printf("UT_RANKBATTLE_RANK_COMPARE_DAY_REQ");
	}
		break;
	case UT_GUILD_CHANGE_NOTICE_REQ:
	{
		CClientSession::SendGuildChangeNotice(pPacket, app);
	}
		break;
	case UT_TMQ_RECORD_LIST_REQ:
	{
		printf("UT_TMQ_RECORD_LIST_REQ");
	}
		break;
	case UT_TMQ_MEMBER_LIST_REQ:
	{
		printf("UT_TMQ_MEMBER_LIST_REQ");
	}
		break;
	case UT_BUDOKAI_TOURNAMENT_INDIVIDUAL_LIST_REQ:
	{
		printf("UT_BUDOKAI_TOURNAMENT_INDIVIDUAL_LIST_REQ");
	}
		break;
	case UT_BUDOKAI_TOURNAMENT_INDIVIDUAL_INFO_REQ:
	{
		printf("UT_BUDOKAI_TOURNAMENT_INDIVIDUAL_INFO_REQ");
	}
		break;
	case UT_BUDOKAI_TOURNAMENT_TEAM_LIST_REQ:
	{
		printf("UT_BUDOKAI_TOURNAMENT_TEAM_LIST_REQ");
	}
		break;
	case UT_BUDOKAI_TOURNAMENT_TEAM_INFO_REQ:
	{
		printf("UT_BUDOKAI_TOURNAMENT_TEAM_INFO_REQ");
	}
		break;
	case UT_PETITION_CHAT_START_RES:
	{
		printf("UT_PETITION_CHAT_START_RES");
	}
		break;
	case UT_PETITION_CHAT_USER_SAY_REQ:
	{
		printf("UT_PETITION_CHAT_USER_SAY_REQ");
	}
		break;
	case UT_PETITION_CHAT_USER_END_NFY:
	{
		printf("UT_PETITION_CHAT_USER_END_NFY");
	}
		break;
	case UT_PETITION_CHAT_GM_SAY_RES:
	{
		printf("UT_PETITION_CHAT_GM_SAY_RES");
	}
		break;
	case UT_PETITION_USER_INSERT_REQ:
	{
		printf("UT_PETITION_USER_INSERT_REQ");
	}
		break;
	case UT_PETITION_CONTENT_MODIFY_REQ:
	{
		printf("UT_PETITION_CONTENT_MODIFY_REQ");
	}
		break;
	case UT_PETITION_SATISFACTION_NFY:
	{
		printf("UT_PETITION_SATISFACTION_NFY");
	}
		break;
	case UT_PETITION_USER_CANCEL_REQ:
	{
		printf("UT_PETITION_USER_CANCEL_REQ");
	}
		break;
	case UT_DOJO_BUDOKAI_SEED_ADD_REQ:
	{
		printf("UT_DOJO_BUDOKAI_SEED_ADD_REQ");
	}
		break;
	case UT_DOJO_BUDOKAI_SEED_DEL_REQ:
	{
		printf("UT_DOJO_BUDOKAI_SEED_DEL_REQ");
	}
		break;
	case UT_DOJO_NOTICE_CHANGE_REQ:
	{
		printf("UT_DOJO_NOTICE_CHANGE_REQ");
	}
		break;



	default:
		return CLkSession::OnDispatch( pPacket );
	}

	return LK_SUCCESS;
}



//-----------------------------------------------------------------------------------
//		ChatServerMain
//-----------------------------------------------------------------------------------
int ChatServerMain(int argc, _TCHAR* argv[])
{
	CChatServer app;
	CLkFileStream traceFileStream;

// LOG FILE
	int rc = traceFileStream.Create( "chatlog" );
	if( LK_SUCCESS != rc )
	{
		printf( "log file CreateFile error %d(%s)", rc, LkGetErrorMessage( rc ) );
		return rc;
	}
// CHECK INI FILE AND START PROGRAM
	LkSetPrintStream( traceFileStream.GetFilePtr() );
	LkSetPrintFlag( PRINT_APP | PRINT_SYSTEM );
	rc = app.Create(argc, argv, ".\\Server.ini");
	if( LK_SUCCESS != rc )
	{
		LK_PRINT(PRINT_APP, "Server Application Create Fail %d(%s)", rc, LkGetErrorMessage(rc) );
		return rc;
	}

	// CONNECT TO MYSQL
	app.db = new MySQLConnWrapper;
	app.db->setConfig(app.GetConfigFileHost(), app.GetConfigFileUser(), app.GetConfigFilePassword(), app.GetConfigFileDatabase());
	try
	{
		app.db->connect();
		printf("Connected to database server.\n\r");
	}
	catch (exception e)
	{
		printf("couldn't connect to database server ErrID:%s\n\r", e.what());
	}
	try
	{
		app.db->switchDb(app.GetConfigFileDatabase());
	}
	catch (exception e)
	{
		printf("Couldn't switch database to %s Error:%s\n\r", app.GetConfigFileDatabase(), e.what());
	}

	app.Start();
	Sleep(500);
	std::cout << "\n\n" << std::endl;
	std::cout << "\t  ____                              ____        _ _ " << std::endl;
	std::cout << "\t |  _ \\ _ __ __ _  __ _  ___  _ __ | __ )  __ _| | |" << std::endl;
	std::cout << "\t | | | | '__/ _` |/ _` |/ _ \\| '_ \\|  _ \\ / _` | | |" << std::endl;
	std::cout << "\t | |_| | | | (_| | (_| | (_) | | | | |_) | (_| | | |" << std::endl;
	std::cout << "\t |____/|_|  \\__,_|\\__, |\\___/|_| |_|____/ \\__,_|_|_|" << std::endl;
	std::cout << "\t                  |___/                             " << std::endl;
	std::cout << "\t______           AKCore :O 2014					______\n\n" << std::endl;
	app.WaitForTerminate();
	

	return 0;
}
