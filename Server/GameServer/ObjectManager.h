#pragma once
#include "SharedType.h"
#include "LkThread.h"
#include "ObjectTable.h"
#ifndef OBJECT_MANAGER_H
#define OBJECT_MANAGER_H


/*
Order of Members
1-Class Pointers/StructsPointers
2-Voids
3-Int,double
*/
class CObjectManager : public CLkRunObject
{
public:
	CObjectManager();
	~CObjectManager();

public:
	sOBJECT_TBLDAT*    GetObjectTbldatByHandle(RwUInt32 object_serialHandle);
	void			Init();
	void			CreateObject();
	void			Release();
	void			Run();
	int				GetTotalObjects();

	std::map<RwUInt32, sOBJECT_TBLDAT*> m_mapObject;
	typedef std::map<RwUInt32, sOBJECT_TBLDAT*>::const_iterator itterType;
	itterType i;
private:
	CLkThread * pThread;
	bool m_bRun;
	int ObjectCounter;

};
extern CObjectManager * g_pObjectManager;
#endif //OBJECT_MANAGER_H