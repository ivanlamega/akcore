@echo off
color 0a
cls
goto home
:home
title DBO Server Start Script
cls
echo.
echo Dragonball Online Server Options
echo =========================
echo.
echo 1) Start All Servers
echo 2) Kill All Servers
echo 3) Test Individual Servers
echo 4) Quit Manager
echo.
echo =========================
set /p toilet=Enter Numeral Option:
if %toilet%==1 (
	start AuthServer.exe
	start CharServer.exe
	start ChatServer.exe	
	start GameServer.exe
) 
if %toilet%==2 (
	taskkill /IM AuthServer.exe
	taskkill /IM CharServer.exe
	taskkill /IM ChatServer.exe	
	taskkill /IM GameServer.exe	
)
if %toilet%==3 goto testing
if %toilet%==4 exit
goto home
:testing
cls
echo.
echo Dragonball Online Server Testing
echo =========================
echo.
echo 1)  Start AuthServer
echo 2)  Start CharServer
echo 3)  Start ChatServer
echo 4)  Start GameServer
echo 5)  Back
echo 6)  Quit Manager
echo.
echo =========================
set /p test=Enter Numeral Option:
if %test%==1 (taskkill /IM AuthServer.exe
start AuthServer.exe
)
if %test%==2 (taskkill /IM CharServer.exe
			  start CharServer.exe)
if %test%==3 (taskkill /IM ChatServer.exe
			  start ChatServer.exe)
if %test%==4 (taskkill /IM GameServer.exe
			  start GameServer.exe)
if %test%==5 goto home
if %test%==6 exit
goto testing

