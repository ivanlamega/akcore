//***********************************************************************************
//
//	File		:	LkLog.cpp
//
//	Begin		:	2006-01-05
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#include "stdafx.h"
#include "LkLog.h"

#include <stdio.h>
#include <tchar.h>


//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
LPCTSTR s_log_channel_string[ LOG_USER ] = 
{
	TEXT( "GENERAL" ),
	TEXT( "SYSTEM" ),
	TEXT( "WARNING" ),
	TEXT( "ASSERT" ),
	TEXT( "NETWORK" ),
	TEXT( "TRAFFIC" ),
};
//-----------------------------------------------------------------------------------
FILE * s_log_stream = stderr; // basic log stream
//-----------------------------------------------------------------------------------
const unsigned int BUFSIZE_LOG = 1024;
//-----------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
LPCTSTR GetLogChannelString(BYTE byLogChannel)
{
	if( byLogChannel >= LOG_USER )
	{
		return "NOT DEFINED TYPE";
	}

	return s_log_channel_string[ byLogChannel ];
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkLog::SetDefaultLogSource(DWORD dwDefaultLogSource)
{
	m_dwDefaultLogSource = dwDefaultLogSource;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkLog::AttachLogStream(CLkLogStream & rLogStream)
{
	CLkLogStream_FilePtr * pLogStream_File = dynamic_cast<CLkLogStream_FilePtr*>( &rLogStream );
	if( pLogStream_File )
	{
		s_log_stream = pLogStream_File->m_fp;
	}

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkLog::DetachLogStream()
{
	s_log_stream = NULL;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkLog::RegisterSource(DWORD dwSource, LPCTSTR lpszSourceName)
{
	UNREFERENCED_PARAMETER( dwSource );
	UNREFERENCED_PARAMETER( lpszSourceName );

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkLog::RegisterChannel(DWORD dwSource, BYTE byChannel, LPCTSTR lpszChannelName, LPCTSTR lpszFileNamePrefix, LPCTSTR lpszFileNameSuffix)
{
	UNREFERENCED_PARAMETER( dwSource );
	UNREFERENCED_PARAMETER( byChannel );
	UNREFERENCED_PARAMETER( lpszChannelName );
	UNREFERENCED_PARAMETER( lpszFileNamePrefix );
	UNREFERENCED_PARAMETER( lpszFileNameSuffix );

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkLog::RegisterBaseChannel(DWORD dwSource, LPCTSTR lpszSourceName)
{
	RegisterChannel( dwSource,	LOG_GENERAL,	"LOG_GENERAL",	lpszSourceName,	"GENERAL" );
	RegisterChannel( dwSource,	LOG_SYSTEM,		"LOG_SYSTEM",	lpszSourceName,	"SYSTEM" );
	RegisterChannel( dwSource,	LOG_WARNING,	"LOG_WARNING",	lpszSourceName,	"WARNING" );
	RegisterChannel( dwSource,	LOG_ASSERT,		"LOG_ASSERT",	lpszSourceName,	"ASSERT" );
	RegisterChannel( dwSource,	LOG_NETWORK,	"LOG_NETWORK",	lpszSourceName,	"NETWORK" );
	RegisterChannel( dwSource,	LOG_TRAFFIC,	"LOG_TRAFFIC",	lpszSourceName,	"TRAFFIC" );

	LK_LOG( LOG_TRAFFIC, "SessionType,Address,Port,ConnectTime,TotalSize,BytesRecvSize,BytesRecvSizeMax,BytesSendSize,BytesSendSizeMax,"
							"PacketTotalCount,PacketRecvCount,PacketSendCount,RecvQueueMaxUseSize,SendQueueMaxUseSize,RecvBufferLoopCount,SendBufferLoopCount");

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkLog::Log(BYTE byLogChannel, bool bDate, LPCTSTR lpszFile, int nLine, LPCTSTR lpszFunc, LPCTSTR lpszText, ...)
{
	TCHAR szLogBuffer[BUFSIZE_LOG + 1] = { 0x00, };
	int nBuffSize = sizeof( szLogBuffer );
	int nWriteSize = 0;

	nWriteSize += _stprintf_s( szLogBuffer, nBuffSize, TEXT("[%s]\t"), GetLogChannelString(byLogChannel) );

	if( bDate )
	{
		SYSTEMTIME	systemTime;
		GetLocalTime( &systemTime );
		nWriteSize += _stprintf_s( szLogBuffer + nWriteSize, nBuffSize - nWriteSize, TEXT("[%d-%02d-%02d %d:%d:%d:%d]\t"), systemTime.wYear, systemTime.wMonth, systemTime.wDay, systemTime.wHour, systemTime.wMinute, systemTime.wSecond, systemTime.wMilliseconds );
	}


	va_list args;
	va_start( args, lpszText );
	nWriteSize += _vstprintf_s( szLogBuffer + nWriteSize, nBuffSize - nWriteSize, lpszText, args );
	va_end( args );


	if( lpszFile )
	{
		nWriteSize += _stprintf_s( szLogBuffer + nWriteSize, nBuffSize - nWriteSize, TEXT(" file[%s]\tline[%d]\t"), lpszFile, nLine );
	}


	if( lpszFunc )
	{
		nWriteSize += _stprintf_s( szLogBuffer + nWriteSize, nBuffSize - nWriteSize, TEXT(" function[%s]\t"), lpszFunc );
	}


	if( s_log_stream )
	{
		_ftprintf( s_log_stream, "%s\n", szLogBuffer );
		fflush( s_log_stream );
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkLog::UnitTest()
{
	int nTemp = 1;
	LPCTSTR lpszTemp = "Hello World!!";

	LK_LOG( LOG_GENERAL, TEXT( "test : [%d] [%s]" ) , nTemp, lpszTemp );
	LK_LOGD( LOG_SYSTEM, TEXT( "test : [%d] [%s]" ), nTemp, lpszTemp );
	LK_LOGL( LOG_ASSERT, TEXT( "test : [%d] [%s]" ), nTemp, lpszTemp );
	LK_LOGDL( LOG_GENERAL, TEXT( "test : [%d] [%s]" ), nTemp, lpszTemp );
	LK_LOG_ASSERT( TEXT( "test : [%d] [%s]" ), nTemp, lpszTemp );

	return 0;
}

//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkLog::ResetList()
{
	::EnterCriticalSection(&m_loglock);
	for (LOGLISTIT iter = m_listLog.begin() ; m_listLog.end() != iter ; iter++)
	{
		SAFE_DELETE(*iter);
	}
	m_listLog.clear();

	::LeaveCriticalSection(&m_loglock);
}

//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
bool CLkLog::IsNew(LPCTSTR lpszFile, int nLine, LPCTSTR lpszFunc)
{
	::EnterCriticalSection(&m_loglock);
	for (LOGLISTIT iter = m_listLog.begin() ; m_listLog.end() != iter ; iter++)
	{
		sLOG_BRIEF* psData = *iter;
		if ( NULL != psData )
		{
			if ( (0 == strcmp(psData->szFile.c_str(), lpszFile ) ) && (nLine == psData->nLine) && (0 == strcmp(psData->szFuntion.c_str(), lpszFunc ) ) )
			{
				::LeaveCriticalSection(&m_loglock);
				return false;
			}
		}
	}

	sLOG_BRIEF* psData = new sLOG_BRIEF;
	if (NULL == psData)
	{
		::LeaveCriticalSection(&m_loglock);
		return false;
	}

	psData->szFile = lpszFile;
	psData->nLine = nLine;
	psData->szFuntion = lpszFunc;

	m_listLog.push_back(psData);

	::LeaveCriticalSection(&m_loglock);
	return true;
}
