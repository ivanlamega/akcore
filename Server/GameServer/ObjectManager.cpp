//////////////////////////////////////////////////////////////////////
// ObjectManager.cpp: implementation of the CObjectManager class.
// By Luiz45
//////////////////////////////////////////////////////////////////////
#include "Stdafx.h"
#include "GameServer.h"

CObjectManager* g_pObjectManager = NULL;
CObjectManager::CObjectManager()
{
	m_bRun = true;
}

CObjectManager::~CObjectManager()
{
	Release();
}

void CObjectManager::Init()
{
	CreateObject();
}

void CObjectManager::Release()
{

}
//Create our object map
void CObjectManager::CreateObject()
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	CObjectTable* pObjectTable = app->g_pTableContainer->GetObjectTable(1);
	int counter = 0;
	for (CTable::TABLEIT itObject = pObjectTable->Begin(); itObject != pObjectTable->End(); ++itObject)
	{
		counter++;
		sOBJECT_TBLDAT* pObjectTbldat = (sOBJECT_TBLDAT*)itObject->second;

		if (pObjectTbldat)
		{
			RwUInt32 handle = (100000 + pObjectTbldat->dwSequence);
			m_mapObject.insert(std::make_pair(handle, (sOBJECT_TBLDAT*)pObjectTbldat));
		}
	}
}

void CObjectManager::Run()
{
	
}
sOBJECT_TBLDAT* CObjectManager::GetObjectTbldatByHandle(RwUInt32 object_serialHandle)
{
	itterType serialHandle = m_mapObject.find(object_serialHandle);
	if (serialHandle != m_mapObject.end())
	{
		sOBJECT_TBLDAT * objectTblidx = serialHandle->second;
		return objectTblidx;
	}
	return NULL;
}
//Get Total of Player in Manager
int CObjectManager::GetTotalObjects()
{
	return this->ObjectCounter;
}