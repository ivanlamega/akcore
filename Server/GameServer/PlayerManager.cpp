//////////////////////////////////////////////////////////////////////
// PlayerManager.cpp: implementation of the CPlayerManager class.
// By Kalisto
//////////////////////////////////////////////////////////////////////

#include "Stdafx.h"
#include "GameServer.h"

DWORD ServerTick;
DWORD DuelTime;
CPlayerManager* g_pPlayerManager = NULL;

CPlayerManager::CPlayerManager()
{
	m_bRun = true;
}

CPlayerManager::~CPlayerManager()
{
	Release();
}

void CPlayerManager::Init()
{
	CreatePlayerThread();
}

void CPlayerManager::Release()
{
	
}


void CPlayerManager::Run()
{
	DoWork();
	Wait(500);
}

void CPlayerManager::CreatePlayerThread()
{

	pThread = CLkThreadFactory::CreateThread(this, "CPMThread");
	pThread->Start();

	this->PlayerCounter = 0;
	ServerTick = timeGetTime();

}

void CPlayerManager::DoWork()
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	DWORD dwTickCur, dwTickOld = ::timeGetTime();
		for (itterType it = m_map_Player.begin(); it != m_map_Player.end(); it++)
		{
			PlayersMain* plr = it->second;
			if (plr)
			{
				/*dwTickLogin = ::timeGetTime(); //Going over a way to check if you are logged in otherwise lp thread crashes you or rp thread crashes you.
				if (((dwTickLogin - plr->GetLoginTime()) > 3000) && plr->GetLoggedIn() == false )
				{
				plr->SetLoggedIn(true);
				}
				else
				plr->SetLoginTime(dwTickLoginOld);

				if (plr->GetLoggedIn() == true)
				{
				plr->isLoggedIn
				*/

				if (plr->GetPlayerDead() == false)
				{
					if (plr->GetPlayerFight() == true)
					{
						if ((timeGetTime() > plr->GetLastFightTime() + 750) && plr->GetAttackTarget() != 0) //I think attacks are faster than every second. so lets try this
						{	//printf("I'm fighting.\n");
							//Send Function AddAttackBegin here for now. this is how we do it right now. otherwise we do it in mob aggro thread.
							if (plr->GetSkillInUse() == false)
							{
								plr->myCCSession->AddAttackBegin(plr->myCCSession->GetavatarHandle(), plr->GetAttackTarget());
								plr->myCCSession->SendCharActionAttack(plr->myCCSession->GetavatarHandle(), plr->GetAttackTarget(), NULL);
								plr->SetLastTimeFight(timeGetTime());
							}
							else
							{
								//Sleep(500);
								plr->SetSkillInUse(false);
								if (plr->GetAttackTarget() != 0 && g_pMobManager->GetMobByHandle(plr->GetAttackTarget())->IsDead == false)
								{
									plr->myCCSession->AddAttackBegin(plr->myCCSession->GetavatarHandle(), plr->GetAttackTarget());
									plr->myCCSession->SendCharActionAttack(plr->myCCSession->GetavatarHandle(), plr->GetAttackTarget(), NULL);
									plr->SetLastTimeFight(timeGetTime());
								}
							}
						}
						if (dwTickCur - dwTickOld >= 10000)
						{
							plr->SetAttackTarget(0); // just for insurance we want to make sure he isnt going to randomly attack someone else.
							plr->SetPlayerFight(false);
							plr->SetLastTimeFight(timeGetTime());
							dwTickOld = dwTickCur;
						}
					}
					//else if (plr->GetPlayerFight() == false && plr->GetCharState()->sCharStateBase.byStateID != (CHARSTATE_SPAWNING || CHARSTATE_DESPAWNING))
					//{
					//	//Regeneration Threading
					//	if (timeGetTime() > plr->GetLastRegen() + 1000)
					//	{
					//		if (plr->GetPcProfile()->wCurLP <= 0)
					//			plr->SendThreadUpdateDeathStatus();
					//		else if (plr->GetPcProfile()->wCurLP < plr->GetPcProfile()->avatarAttribute.wBaseMaxLP || plr->GetPcProfile()->wCurLP > plr->GetPcProfile()->avatarAttribute.wBaseMaxLP)
					//		{
					//			plr->SendThreadUpdateOnlyLP();
					//			plr->SetLastRegenTime(timeGetTime());
					//		}
					//		if (plr->GetPcProfile()->wCurEP < plr->GetPcProfile()->avatarAttribute.wBaseMaxEP || plr->GetPcProfile()->wCurEP > plr->GetPcProfile()->avatarAttribute.wBaseMaxEP)
					//		{
					//			plr->SendThreadUpdateOnlyEP();
					//			plr->SetLastRegenTime(timeGetTime());
					//		}
					//			plr->SendThreadUpdateEPLP();
					//			plr->SetLastRegenTime(timeGetTime());
					//	}
					//}
					//if (GetTickCount64() > plr->GetLastRpUpdate() + 100 && ((plr->GetPcProfile()->wCurRP > 0) || plr->GetRpBallFull() > 0))
					//{
					//	if ((plr->GetPcProfile()->wCurRP > 0) || plr->GetRpBallFull() > 0)
					//	{
					//		if (plr->GetPcProfile()->wCurRP <= 0)
					//			if (plr->GetRpBallFull() > 0)
					//			{
					//				plr->SetRpBallFull(1);
					//				plr->GetPcProfile()->wCurRP = (plr->GetPcProfile()->avatarAttribute.wBaseMaxRP / plr->GetRpBallCounter()) - 1;
					//			}
					//			else;
					//		else
					//			plr->GetPcProfile()->wCurRP -= 1;
					//		//plr->SendThreadUpdateRP();
					//		plr->SetLastRpUpdateTime(timeGetTime());
					//	}
					//}
					//if (plr->CanStartBuffCheck())
					//{
					//	if ((timeGetTime() > plr->GetBuffTimer() + SECOND))
					//	{
					//		for (int i = 0; i < plr->cPlayerSkills->GetSkillBuffCount(); i++)
					//		{
					//			//this thread is execute by each second?
					//			if (((plr->cPlayerSkills->GetSkillBuff()[i].dwTimeRemaining -= SECOND) == SECOND) && ((plr->cPlayerSkills->GetSkillBuff()[i].sourceTblidx != INVALID_TBLIDX)))
					//			{
					//				//Dropp Event Prepare
					//				CLkPacket packet2(sizeof(sGU_BUFF_DROPPED));
					//				sGU_BUFF_DROPPED * pBuffDrop = (sGU_BUFF_DROPPED*)packet2.GetPacketData();
					//				pBuffDrop->hHandle = plr->GetAvatarHandle();
					//				pBuffDrop->bySourceType = DBO_OBJECT_SOURCE_SKILL;//Need be rechecked because this can be a type DBO_OBJECT_SOURCE_ITEM
					//				pBuffDrop->wOpCode = GU_BUFF_DROPPED;
					//				pBuffDrop->tblidx = plr->cPlayerSkills->GetSkillBuff()[i].sourceTblidx;

					//				//First Drop,Second Resp to client
					//				packet2.SetPacketLen(sizeof(sGU_BUFF_DROPPED));
					//				g_pApp->Send(plr->GetSession(), &packet2);
					//				app->UserBroadcastothers(&packet2, plr->myCCSession);

					//				plr->cPlayerSkills->CleanBuffPosition(i);;

					//				//Next thing is Remove the Extra Stats given for each buff
					//			}

					//		}
					//		plr->SetBuffTimer(timeGetTime());
					//	}
					//}
					/*if (plr->isKaioken == true) /* TEST */
					/*{
					plr->GetPcProfile()->wCurLP -= (500 * plr->GetCharState()->sCharStateBase.aspectState.sAspectStateDetail.sKaioken.byRepeatingCount);
					plr->GetPcProfile()->wCurEP -= (500 * plr->GetCharState()->sCharStateBase.aspectState.sAspectStateDetail.sKaioken.byRepeatingCount);
					}*/

					if (timeGetTime() > plr->GetMob_SpawnTime() + 300)
					{
						g_pMobManager->RunSpawnCheck(NULL, plr->GetPlayerPosition(), plr->myCCSession);
						plr->SetMob_SpawnTime(timeGetTime());
					}

					if ((timeGetTime() - ServerTick) >= 30 * SECOND && !plr->myCCSession->IsConnected())
					{
						PlayerCounter--;
						plr->myCCSession->SendCharExitReq(NULL, app);
					}

					if ((timeGetTime() - ServerTick) >= MINUTE)
					{
						printf("\n\r");
						printf("%d Players Online\n\r", GetTotalPlayers());
						printf("AKCore> ");
						ServerTick = timeGetTime();
						//Just to avoid the Invalid data 127
						if (plr->GetPcProfile()->byLevel <= 50)
							plr->SavePlayerData(app);
					}


				}
			}
		}
		//Not sleeping anymore due to timers as part of members in Plr;
}
//-------------------------NEW METHODS-----------------------//
//Add new player in our MAP
void CPlayerManager::AddNewPlayer(RwUInt32 playerHandle, HSESSION PlayerSession, int CharID, int AccountID)
{
	PlayersMain* tmpPlayer = new PlayersMain(PlayerSession, CharID, AccountID);
	tmpPlayer->SetAvatarHandle(playerHandle);
	this->m_map_Player.insert(std::make_pair(playerHandle,tmpPlayer));
	this->PlayerCounter++;
	tmpPlayer = NULL;
	delete tmpPlayer;
}
//Get PlayerMain By AvatarHandle
PlayersMain* CPlayerManager::GetPlayer(RwUInt32 playerHandle)
{
	PlayersMain* player;
	for (itterType it = this->m_map_Player.begin(); it != this->m_map_Player.end(); it++)
	{
		player = it->second;
		if (player)
		{
			if (player->GetAvatarHandle() == playerHandle)
			{
				return player;
			}
		}
	}
	return NULL;//If return null then you got a fucking error
}
//Get PlayerMain by CharID
PlayersMain* CPlayerManager::GetPlayerByID(RwUInt32 playerID)
{
	PlayersMain* player;
	for (itterType it = this->m_map_Player.begin(); it != this->m_map_Player.end(); it++)
	{
		player = it->second;
		if (player)
		{
			if (player->GetCharID() == playerID)
			{
				return player;
			}
		}
	}
	return NULL;//If return null then you got a fucking error
}
//Remove the player from our map
void CPlayerManager::RemovePlayer(RwUInt32 playerHandle)
{
	this->PlayerCounter--;
}
//Get Total of Player in Manager
int CPlayerManager::GetTotalPlayers()
{
	return this->PlayerCounter;
}