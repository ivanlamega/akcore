//***********************************************************************************
//
//	File		:	LkSessionMap.h
//
//	Begin		:	2007-01-23
//
//	Copyright	:	ⓒ NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#pragma once


#include <map>

#include "LkSharedType.h"
#include "LkMutex.h"
#include "LkSession.h"


template<class TKEY>
class CLkSessionMap
{
public:

	CLkSessionMap(void)
		: m_dwCurCount( 0 ), m_dwMaxCount( 0 ) { }

	virtual ~CLkSessionMap(void) {}


public:


	bool						AddSession(TKEY keyVal, CLkSession * pSession);

	void						RemoveSession(TKEY keyVal);

	CLkSession *				FindSession(TKEY keyVal);

	CLkSession *				AcquireSession(TKEY keyVal);


public:

	DWORD						GetCurCount() { return m_dwCurCount; }

	DWORD						GetMaxCount() { return m_dwMaxCount; }

	CLkSession  *				GetFirst();

	CLkSession  *				GetNext();



public:

	void						Lock() { m_mutex.Lock(); }

	void						Unlock() { m_mutex.Unlock(); }


private:


	typedef std::map<TKEY, CLkSession*> SESSIONMAP;
	typedef typename std::map<TKEY, CLkSession*>::value_type SESSIONMAP_VAL;
	typedef typename std::map<TKEY, CLkSession*>::iterator SESSIONMAP_IT;
	

	CLkMutex					m_mutex;

	SESSIONMAP					m_sessionMap;

	DWORD						m_dwCurCount;

	DWORD						m_dwMaxCount;

	SESSIONMAP_IT				m_sessionIt;

};


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
template<class TKEY>
inline bool CLkSessionMap<TKEY>::AddSession(TKEY keyVal, CLkSession * pSession)
{
	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();


	if( false == m_sessionMap.insert( SESSIONMAP_VAL( keyVal, pSession ) ).second )
	{
		return false;
	}


	++m_dwCurCount;

	if( m_dwCurCount > m_dwMaxCount )
	{
		m_dwMaxCount = m_dwCurCount;
	}


	pSession->Acquire();


	return true;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
template<class TKEY>
inline void CLkSessionMap<TKEY>::RemoveSession(TKEY keyVal)
{
	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();


	SESSIONMAP_IT it = m_sessionMap.find( keyVal );
	if( it == m_sessionMap.end() )
	{
		return;
	}


	--m_dwCurCount; 


	it->second->Release();


	m_sessionMap.erase( it );
}



//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
template<class TKEY>
inline CLkSession * CLkSessionMap<TKEY>::FindSession(TKEY TKEY)
{
	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();


	SESSIONMAP_IT it = m_sessionMap.find( keyVal );
	if( it == m_sessionMap.end() )
	{
		return NULL;
	}


	return it->second;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
//
// Network Processor 쓰레드 이외의 쓰레드에서 Session에 대한 참조를 해야 할 경우 사용한다
// 사용후 반드시 Session의 Reference를 Release해 주어야 한다
//
//-----------------------------------------------------------------------------------
template<class TKEY>
inline CLkSession * CLkSessionMap<TKEY>::AcquireSession(TKEY TKEY)
{
	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();


	SESSIONMAP_IT it = m_sessionMap.find( TKEY );
	if( it == m_sessionMap.end() )
	{
		return NULL;
	}


	it->second->Acquire();

	return it->second;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
template<class TKEY>
inline CLkSession * CLkSessionMap<TKEY>::GetFirst()
{
	m_sessionIt = m_sessionMap.begin();
	if( m_sessionIt == m_sessionMap.end() )
	{
		return NULL;
	}


	return m_sessionIt->second;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
template<class TKEY>
inline CLkSession * CLkSessionMap<TKEY>::GetNext()
{
	LK_ASSERT( m_sessionIt != m_sessionMap.end() );

	if( ++m_sessionIt == m_sessionMap.end() )
	{
		return NULL;
	}


	return m_sessionIt->second;
}
