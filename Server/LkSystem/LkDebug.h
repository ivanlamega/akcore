//***********************************************************************************
//
//	File		:	LkDebug.h
//
//	Begin		:	2005-12-06
//
//	Copyright	:	ⓒ NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	각종 디버그 관련 유틸리티
//
//***********************************************************************************

#pragma once


#include "LkMiniDump.h"

#include <crtdbg.h>
#include <stdio.h>


#define __LK_DEBUG_PRINT__


//-----------------------------------------------------------------------------------
// static variable
//-----------------------------------------------------------------------------------
extern const unsigned int PRINT_BUF_SIZE;
extern unsigned int s_dwCurFlag;
extern FILE * s_curStream;
//-----------------------------------------------------------------------------------



#ifdef __LK_DEBUG_PRINT__

	void LkSetPrintStream(FILE * fp);
	void LkSetPrintFlag(unsigned int dwFlag);
	void LkDebugPrint(unsigned int dwFlag, LPCTSTR lpszText, ...);

	#define LK_PRINT			LkDebugPrint

#else


	inline void LkSetPrintStream(FILE * fp) {}
	inline void LkSetPrintFlag(unsigned int dwFlag) {}
	inline void LkDebugPrint(unsigned int dwFlag, LPCTSTR lpszText, ...) {}

	#define LK_PRINT			1 ? (void) 0 : LkDebugPrint


#endif


#if defined( _DEBUG ) && defined( _DEVEL )

	#define LK_DBGREPORT(msg, ...)																	\
	{																								\
		if( 1 == _CrtDbgReport( _CRT_ASSERT, __FILE__, __LINE__, __FUNCTION__, msg, __VA_ARGS__ ) ) \
			_CrtDbgBreak();																			\
	}

	#define LK_ASSERT(x)			_ASSERT(x)

#elif defined( _DEBUG )

	#define LK_DBGREPORT(msg, ...)

	#define LK_ASSERT(x)																			\
			(void) ( (!!(x)) ||																		\
					(CLkMiniDump::Snapshot(), 0))

#else

	#define LK_DBGREPORT(msg, ...)

	#define LK_ASSERT(x)																			\
			(void) ( (!!(x)) ||																		\
					(CLkMiniDump::Snapshot(), 0))

#endif


#define PRINT_SYSTEM		0x01
#define PRINT_ALL			0xFFFFFFFF
