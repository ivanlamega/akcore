#include "stdafx.h"
#include <list>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "Character.h"

#ifndef PLAYERS_INVENTORY_H
#define PLAYERS_INVENTORY_H

class PlayersInventory{
	//----------------Constructor & Destructor --------------//
	public:
		PlayersInventory(int CharID);
		~PlayersInventory();
	//Members & Functions
	private:
		DWORD ZennyToTrade;
		BYTE ItemsCount;
		BYTE TotalItemsTrade;
		int CharID;
		int myBagStatus;
		int LastItemCreated;
		std::vector<sITEM_DATA> vItemsToTrade;
		sITEM_PROFILE aItemProfile[LK_MAX_COUNT_USER_HAVE_INVEN_ITEM];
		sITEM_BRIEF sItemBrief[EQUIP_SLOT_TYPE_COUNT];
	//Public Functions
	public:
		void LoadCharInventory();
		void AddItemToTrade(sITEM_DATA sItemTrade);
		void AddItemToInventory(sITEM_DATA sItemAdd);
		void SetBagStatus(int statusCode);
		void SetLastCreatedItem(int ITEMHANDLE);
		void RemoveItemFromInventory(int itemHandle);
		void SetAmountZennyToTrade(DWORD dwAmount);
		void RemoveUpdateTrade(int itemHandle, bool bRemove, int iStackCount = 0);
		int CheckAvailableSlot(int iBagToCheck);
		int CheckAvailableBag();
		int GetBagStatus();
		int GetLastItemCreated();
		bool ExistItemByHandle(int iHandle);
		BYTE GetTotalItemsCount();
		BYTE GetTotalItemsTrade();		
		DWORD GetZennyToTrade();
		std::vector<sITEM_DATA> GetItemsToTrade();
		sITEM_PROFILE* GetInventory();
		sITEM_PROFILE* GetItemByPosPlace(int byPlace, int byPos);
		sITEM_BRIEF* GetEquippedItems();
};

#endif