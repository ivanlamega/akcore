#pragma once


#include <string>

class CLkStringW
{
public:
	CLkStringW(void);
	CLkStringW(const char * pszString);
	CLkStringW(const WCHAR* pwszString);
	virtual ~CLkStringW(void);

public:
	std::wstring &		GetString(void) { return m_str; }
	int					Format(const WCHAR *format, ...);
	CLkStringW  &		operator=(const char * pszString);
	CLkStringW  &		operator=(const WCHAR* pwszString);
	bool				operator==(CLkStringW& string);
	const wchar_t* c_str(void) const { return m_str.c_str(); }

	bool operator<(const CLkStringW &right) const
	{
		if ( m_str<right.m_str ) return true;
		else return false;
	}

private:
	std::wstring		m_str;
};
