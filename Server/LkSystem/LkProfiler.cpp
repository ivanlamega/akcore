//***********************************************************************************
//
//	File		:	LkProfiler.cpp
//
//	Begin		:	2006-07-10
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#include "stdafx.h"
#include "LkError.h"

#include "LkProfiler.h"
#include "LkDebug.h"


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void GetProfileTickCount(__int64 * pCounter)
{
	QueryPerformanceCounter( (LARGE_INTEGER *) pCounter );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline float GetProfileTickRate()
{
	static float fFrequency = -1.0f;

	if( -1.0f == fFrequency )
	{
		__int64 frequence = 0;
		QueryPerformanceFrequency ( (LARGE_INTEGER *) &frequence );
		fFrequency = (float) frequence;
	} 

	return fFrequency;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkProfileNode::CLkProfileNode(const char * lpszNodeName, CLkProfileNode * pParent)
:
m_lpszNodeName( lpszNodeName ),
m_pParent( pParent )
{
	Init();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkProfileNode::~CLkProfileNode()
{
	Destroy();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkProfileNode::Init()
{
	m_dwTotalCalls = 0;

	m_fTotalTime = 0.0f;

	m_startTime = 0;

	m_dwRecursionCounter = 0;

	m_pChild = NULL;

	m_pSibling = NULL;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkProfileNode::Destroy()
{
	if( m_pSibling )
	{
		SAFE_DELETE( m_pSibling );
	}

	if( m_pChild )
	{
		SAFE_DELETE( m_pChild );
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkProfileNode::Reset()
{
	m_dwTotalCalls = 0;
	m_fTotalTime = 0.0f;

	if ( m_pChild )
	{
		m_pChild->Reset();
	}
	
	if ( m_pSibling )
	{
		m_pSibling->Reset();
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkProfileNode::Call()
{
	m_dwTotalCalls++;

	if( m_dwRecursionCounter++ == 0)
	{
		GetProfileTickCount( &m_startTime );
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	: Recursive�� �ƴϸ� true
//-----------------------------------------------------------------------------------
bool CLkProfileNode::Return()
{
	if ( --m_dwRecursionCounter == 0 && m_dwTotalCalls != 0 )
	{ 
		__int64 time;

		GetProfileTickCount( &time );
		time -= m_startTime;

		m_fTotalTime += (float) time / GetProfileTickRate();
	}

	return ( m_dwRecursionCounter == 0 );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkProfileNode * CLkProfileNode::GetSubNode(const char * lpszNodeName)
{
	// Find SubNode
	CLkProfileNode * pChild = m_pChild;
	while ( pChild )
	{
		if( pChild->GetName() == lpszNodeName )
		{
			return pChild;
		}

		pChild = pChild->m_pSibling;
	}


	// Create New Node
	CLkProfileNode * pNewNode = new CLkProfileNode( lpszNodeName, this );
	if( NULL == pNewNode )
	{
		LK_ASSERT( 0 );
		return NULL;
	}

	pNewNode->m_pSibling = pChild;
	pChild = pNewNode;

	return pNewNode;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkProfileIterator::CLkProfileIterator( CLkProfileNode * start )
{
	m_pCurParent = start;
	m_pCurChild = m_pCurParent->GetChild();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkProfileIterator::First(void)
{
	m_pCurChild = m_pCurParent->GetChild();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkProfileIterator::Next(void)
{
	m_pCurChild = m_pCurChild->GetSibling();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
bool CLkProfileIterator::IsDone(void)
{
	return m_pCurChild == NULL;
}


//-----------------------------------------------------------------------------------
//		Purpose	: Make the given child the new parent
//		Return	:
//-----------------------------------------------------------------------------------
void CLkProfileIterator::EnterChild( int index )
{
	m_pCurChild = m_pCurParent->GetChild();

	while ( (m_pCurChild != NULL) && (index != 0) )
	{
		index--;
		m_pCurChild = m_pCurChild->GetSibling();
	}

	if ( m_pCurChild != NULL )
	{
		m_pCurParent = m_pCurChild;
		m_pCurChild = m_pCurParent->GetChild();
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	: Make the current parent's parent the new parent
//		Return	: 
//-----------------------------------------------------------------------------------
void CLkProfileIterator::EnterParent()
{
	if( m_pCurParent->GetParent() != NULL )
	{
		m_pCurParent = m_pCurParent->GetParent();
	}

	m_pCurChild = m_pCurParent->GetChild();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkProfiler::CLkProfiler()
:
m_root( "Root", NULL )
{
	Init();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkProfiler::~CLkProfiler()
{
	CloseProfiler();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkProfiler * CLkProfiler::GetInstance()
{
	static CLkProfiler	s_profiler;

	return &s_profiler;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkProfiler::Init()
{
	m_file = NULL;

	m_pCurNode = &m_root;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int	CLkProfiler::OpenProfiler(const char * lpszFileName)
{
	if( m_file )
	{
		return LK_ERR_SYS_PROFILE_INITIALIZE_FAIL;
	}

	int rc = fopen_s( &m_file, lpszFileName, "w" );
	if( LK_SUCCESS != rc )
	{
		return rc;
	}

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkProfiler::CloseProfiler()
{
	if( m_file )
	{
		fclose( m_file );
	}

	Init();
}


//-----------------------------------------------------------------------------------
//		Purpose	: This resets everything except for the tree structure.  All of the timing data is reset
//		Return	:
//-----------------------------------------------------------------------------------
void CLkProfiler::Reset()
{
	m_root.Reset(); 

	GetProfileTickCount( &m_resetTime );
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
float CLkProfiler::GetTimeSinceReset( void )
{
	__int64 time;

	GetProfileTickCount( &time );
	time -= m_resetTime;

	return (float) time / GetProfileTickRate();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkProfiler::StartProfile(const char * lpszNodeName)
{
	if( lpszNodeName != m_pCurNode->GetName() )
	{
		m_pCurNode = m_pCurNode->GetSubNode( lpszNodeName );
	} 

	m_pCurNode->Call();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkProfiler::StopProfile()
{
	if( m_pCurNode->Return() )
	{
		m_pCurNode = m_pCurNode->GetParent();
	}
}
