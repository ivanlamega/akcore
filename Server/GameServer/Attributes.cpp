#include "stdafx.h"
#include "Attributes.h"
#include "GameServer.h"

PlayerAttributes::PlayerAttributes(HSESSION PlayerSession)
{
	this->PlayerSession = PlayerSession;
}

PlayerAttributes::~PlayerAttributes()
{
}
/**
 *  Load Attributes From DB this will set all sPlayerAttribute with Database Values
 *  To Return this values you need use getAvatarAttribute();
*/
void PlayerAttributes::LoadAttributesFromDB(int charID)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	MySQLConnWrapper *db = new MySQLConnWrapper;
	db->setConfig(app->GetConfigFileHost(), app->GetConfigFileUser(), app->GetConfigFilePassword(), app->GetConfigFileDatabase());
	db->connect();
	db->switchDb(app->GetConfigFileDatabase());

	db->prepare("SELECT * FROM characters WHERE CharID = ?");
	db->setInt(1, charID);
	db->execute();
	db->fetch();

	//Load All Attributes One time only - Luiz  IN ORDER --Kalisto
	this->sPlayerAttribute.byBaseStr = db->getInt("BaseStr");
	this->sPlayerAttribute.byLastStr = db->getInt("LastStr");
	this->sPlayerAttribute.byBaseCon = db->getInt("BaseCon");
	this->sPlayerAttribute.byLastCon = db->getInt("LastCon");
	this->sPlayerAttribute.byBaseFoc = db->getInt("BaseFoc");
	this->sPlayerAttribute.byLastFoc = db->getInt("LastFoc");
	this->sPlayerAttribute.byBaseDex = db->getInt("BaseDex");
	this->sPlayerAttribute.byLastDex = db->getInt("LastDex");
	this->sPlayerAttribute.byBaseSol = db->getInt("BaseSol");
	this->sPlayerAttribute.byLastSol = db->getInt("LastSol");
	this->sPlayerAttribute.byBaseEng = db->getInt("BaseEng");
	this->sPlayerAttribute.byLastEng = db->getInt("LastEng");
	
	this->sPlayerAttribute.wBaseMaxLP = db->getInt("BaseMaxLP");
	this->sPlayerAttribute.wLastMaxLP = db->getInt("LastMaxLP");
	this->sPlayerAttribute.wBaseMaxEP = db->getInt("BaseMaxEP");
	this->sPlayerAttribute.wLastMaxEP = db->getInt("LastMaxEP");
	this->sPlayerAttribute.wBaseMaxRP = db->getInt("BaseMaxRP");
	this->sPlayerAttribute.wLastMaxRP = db->getInt("LastMaxRP");
	
	this->sPlayerAttribute.wBaseLpRegen = 1;
	this->sPlayerAttribute.wLastLpRegen = 1;
	this->sPlayerAttribute.wBaseLpSitdownRegen = 1;
	this->sPlayerAttribute.wLastLpSitdownRegen = 1;
	this->sPlayerAttribute.wBaseLpBattleRegen = 1;
	this->sPlayerAttribute.wLastLpBattleRegen = 1;

	this->sPlayerAttribute.wBaseEpRegen = 1;
	this->sPlayerAttribute.wLastEpRegen = 1;
	this->sPlayerAttribute.wBaseEpSitdownRegen = 1;
	this->sPlayerAttribute.wLastEpSitdownRegen = 1;
	this->sPlayerAttribute.wBaseEpBattleRegen = 1;
	this->sPlayerAttribute.wLastEpBattleRegen = 1;

	this->sPlayerAttribute.wBaseRpRegen = 1;
	this->sPlayerAttribute.wLastRpRegen = 1;
	this->sPlayerAttribute.wLastRpDimimutionRate = 1;

	this->sPlayerAttribute.wBasePhysicalOffence = db->getInt("BasePhysicalOffence");
	this->sPlayerAttribute.wLastPhysicalOffence = db->getInt("BasePhysicalOffence");
	this->sPlayerAttribute.wBasePhysicalDefence = db->getInt("BasePhysicalDefence");
	this->sPlayerAttribute.wLastPhysicalDefence = db->getInt("BasePhysicalDefence");

	this->sPlayerAttribute.wBaseEnergyOffence = db->getInt("BaseEnergyOffence");
	this->sPlayerAttribute.wLastEnergyOffence = db->getInt("BaseEnergyOffence");
	this->sPlayerAttribute.wBaseEnergyDefence = db->getInt("BaseEnergyDefence");
	this->sPlayerAttribute.wLastEnergyDefence = db->getInt("BaseEnergyDefence");

	this->sPlayerAttribute.wBaseAttackRate = db->getInt("BaseAttackRate");
	this->sPlayerAttribute.wLastAttackRate = db->getInt("LastAttackRate");
	this->sPlayerAttribute.wBaseDodgeRate = db->getInt("BaseDodgeRate");
	this->sPlayerAttribute.wLastDodgeRate = db->getInt("LastDodgeRate");
	this->sPlayerAttribute.wBaseBlockRate = db->getInt("BaseBlockRate");
	this->sPlayerAttribute.wLastBlockRate = db->getInt("LastBlockRate");

	this->sPlayerAttribute.wBaseCurseSuccessRate = 1;
	this->sPlayerAttribute.wLastCurseSuccessRate = 1;
	this->sPlayerAttribute.wBaseCurseToleranceRate = 1;
	this->sPlayerAttribute.wLastCurseToleranceRate = 1;

	this->sPlayerAttribute.wBasePhysicalCriticalRate = db->getInt("BasePhysicalCriticalRate");
	this->sPlayerAttribute.wLastPhysicalCriticalRate = db->getInt("LastPhysicalCriticalRate");
	this->sPlayerAttribute.wBaseEnergyCriticalRate = db->getInt("BaseEnergyCriticalRate");
	this->sPlayerAttribute.wLastEnergyCriticalRate = db->getInt("LastEnergyCriticalRate");
	
	this->sPlayerAttribute.fLastRunSpeed = (float)db->getDouble("LastRunSpeed");

	this->sPlayerAttribute.wBaseAttackSpeedRate = (float)db->getInt("BaseAttackSpeedRate");
	this->sPlayerAttribute.wLastAttackSpeedRate = (float)db->getInt("LastAttackSpeedRate");
	this->sPlayerAttribute.fLastAttackRange = (float)db->getInt("LastAttackRange");
	this->sPlayerAttribute.fBaseAttackRange = (float)db->getInt("BaseAttackRange");

	this->sPlayerAttribute.fCastingTimeChangePercent = 1.0f;
	this->sPlayerAttribute.fCoolTimeChangePercent = 1.0f;
	this->sPlayerAttribute.fKeepTimeChangePercent = 1.0f;
	this->sPlayerAttribute.fDotValueChangePercent = 1.0f;
	this->sPlayerAttribute.fDotTimeChangeAbsolute = 1.0f;
	this->sPlayerAttribute.fRequiredEpChangePercent = 1.0f;

	this->sPlayerAttribute.fHonestOffence = 1.0f;
	this->sPlayerAttribute.fHonestDefence = 1.0f;
	this->sPlayerAttribute.fStrangeOffence = 1.0f;
	this->sPlayerAttribute.fStrangeDefence = 1.0f;
	this->sPlayerAttribute.fWildOffence = 1.0f;
	this->sPlayerAttribute.fWildDefence = 1.0f;
	this->sPlayerAttribute.fEleganceOffence = 1.0f;
	this->sPlayerAttribute.fEleganceDefence = 1.0f;
	this->sPlayerAttribute.fFunnyOffence = 1.0f;
	this->sPlayerAttribute.fFunnyDefence = 1.0f;

	this->sPlayerAttribute.wParalyzeToleranceRate = 1;
	this->sPlayerAttribute.wTerrorToleranceRate = 1;
	this->sPlayerAttribute.wConfuseToleranceRate = 1;
	this->sPlayerAttribute.wStoneToleranceRate = 1;
	this->sPlayerAttribute.wCandyToleranceRate = 1;

	this->sPlayerAttribute.fParalyzeKeepTimeDown = 1.0f;
	this->sPlayerAttribute.fTerrorKeepTimeDown = 1.0f;
	this->sPlayerAttribute.fConfuseKeepTimeDown = 1.0f;
	this->sPlayerAttribute.fStoneKeepTimeDown = 1.0f;
	this->sPlayerAttribute.fCandyKeepTimeDown = 1.0f;
	this->sPlayerAttribute.fBleedingKeepTimeDown = 1.0f;
	this->sPlayerAttribute.fPoisonKeepTimeDown = 1.0f;
	this->sPlayerAttribute.fStomachacheKeepTimeDown = 1.0f;

	this->sPlayerAttribute.fCriticalBlockSuccessRate = 1.0f;

	this->sPlayerAttribute.wGuardRate = 1;

	this->sPlayerAttribute.fSkillDamageBlockModeSuccessRate = 1.0f;
	this->sPlayerAttribute.fCurseBlockModeSuccessRate = 1.0f;
	this->sPlayerAttribute.fKnockdownBlockModeSuccessRate = 1.0f;
	this->sPlayerAttribute.fHtbBlockModeSuccessRate = 1.0f;

	this->sPlayerAttribute.fSitDownLpRegenBonusRate = 1.0f;
	this->sPlayerAttribute.fSitDownEpRegenBonusRate = 1.0f;

	this->sPlayerAttribute.fPhysicalCriticalDamageBonusRate = 1.0f;
	this->sPlayerAttribute.fEnergyCriticalDamageBonusRate = 1.0f;

	this->sPlayerAttribute.fItemUpgradeBonusRate = 1.0f;
	this->sPlayerAttribute.fItemUpgradeBreakBonusRate = 1.0f;

	
}
//This method will return a sAVATAR_ATTRIBUTE struct
sAVATAR_ATTRIBUTE PlayerAttributes::GetAvatarAttribute()
{
	return this->sPlayerAttribute;
}
//This method will return a sAVATAR_ATTRIBUTE_LINK
//Initially the sPlayerAttributeLink is not filled, you need call UpdateAvatarAttribute(RwUint32 Handle) to Fill
sAVATAR_ATTRIBUTE_LINK PlayerAttributes::GetAvatarAttributeLink()
{
	return this->sPlayerAttributeLink;
}
/**
* As said you need send your handle and this will update all attributes and will send a Packet(GU_AVATAR_ATTRIBUTE)
*/
void PlayerAttributes::UpdateAvatarAttributes(RwUInt32 Handle)
{
	CLkPacket packet(sizeof(sGU_AVATAR_ATTRIBUTE_UPDATE));
	sGU_AVATAR_ATTRIBUTE_UPDATE * res = (sGU_AVATAR_ATTRIBUTE_UPDATE *)packet.GetPacketData();
	PlayersMain* plr = g_pPlayerManager->GetPlayer(Handle);
	memcpy(&this->sPlayerAttribute, &plr->GetPcProfile()->avatarAttribute, sizeof(sAVATAR_ATTRIBUTE));

	CLkBitFlagManager changedFlag;
	
	changedFlag.Create(&this->sPlayerAttribute, ATTRIBUTE_TO_UPDATE_COUNT);
	this->sPlayerAttributeLink = CLkAvatar::GetInstance()->ConvertAttributeToAttributeLink(&this->sPlayerAttribute);

	DWORD buffer[1024];//Thanks Daneos
	DWORD datasize;

	for (BYTE byIndex = ATTRIBUTE_TO_UPDATE_FIRST; byIndex <= ATTRIBUTE_TO_UPDATE_LAST; byIndex++)
	{
		changedFlag.Set(byIndex);
	}

	if (CLkAvatar::GetInstance()->SaveAvatarAttribute(&changedFlag, &this->sPlayerAttributeLink, &buffer, &datasize) == false)
	{
		printf("ERROR IN UPDATE ATTRIBUTE");
	}

	memcpy(res->abyFlexibleField, &buffer, ((UCHAR_MAX - 1) / 8 + 1) + sizeof(sAVATAR_ATTRIBUTE));

	res->byAttributeTotalCount = 101;
	res->hHandle = Handle;
	res->wOpCode = GU_AVATAR_ATTRIBUTE_UPDATE;

	packet.SetPacketLen(sizeof(sGU_AVATAR_ATTRIBUTE_UPDATE));
	g_pApp->Send(this->PlayerSession, &packet);
	plr = NULL;
	delete plr;
}
/*
* This function will update the Attributes by Chip Info - Need be Implemented - Luiz45
*/
void PlayerAttributes::UpdateStatsUsingScouterChips(RwUInt32 Handle, TBLIDX OptionTblidx,bool bDecrease)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();
	CItemOptionTable* pItemOptionTable = app->g_pTableContainer->GetItemOptionTable();
	sITEM_OPTION_TBLDAT* sItemOptTbl = reinterpret_cast<sITEM_OPTION_TBLDAT*>(pItemOptionTable->FindData(OptionTblidx));
	PlayersMain* plr = g_pPlayerManager->GetPlayer(Handle);
	//Let's Check what we gonna update
	switch (sItemOptTbl->byScouterInfo)
	{
		case SCOUTER_INFO_LP:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.wBaseMaxLP -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.wBaseMaxLP += sItemOptTbl->nValue[0];
		}
		break;
		case SCOUTER_INFO_EP:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.wBaseMaxEP -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.wBaseMaxEP += sItemOptTbl->nValue[0];
		}
		break;
		case SCOUTER_INFO_ATTACK_RATE:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.wLastAttackRate -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.wLastAttackRate += sItemOptTbl->nValue[0];
		}
		break;
		case SCOUTER_INFO_ATTACK_SPEED:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.wLastAttackSpeedRate -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.wLastAttackSpeedRate += sItemOptTbl->nValue[0];
		}
		break;
		case SCOUTER_INFO_BLOCK_RATE:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.wLastBlockRate -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.wLastBlockRate += sItemOptTbl->nValue[0];
		}
		break;
		case SCOUTER_INFO_CON:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.byLastCon -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.byLastCon += sItemOptTbl->nValue[0];
		}
		break;		
		case SCOUTER_INFO_CURSE_SUCCESS_RATE:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.wLastCurseSuccessRate -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.wLastCurseSuccessRate += sItemOptTbl->nValue[0];
		}
			break;
		case SCOUTER_INFO_CURSE_TOLERANCE_RATE:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.wLastCurseToleranceRate -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.wLastCurseToleranceRate += sItemOptTbl->nValue[0];
		}
			break;
		case SCOUTER_INFO_DEX:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.byLastDex -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.byLastDex += sItemOptTbl->nValue[0];
		}
			break;
		case SCOUTER_INFO_DODGE_RATE:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.wLastDodgeRate -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.wLastDodgeRate += sItemOptTbl->nValue[0];
		}
			break;
		case SCOUTER_INFO_ENG:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.byLastEng -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.byLastEng += sItemOptTbl->nValue[0];
		}
			break;
		case SCOUTER_INFO_FOC:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.byLastFoc -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.byLastFoc += sItemOptTbl->nValue[0];
		}
			break;
		case SCOUTER_INFO_ITEM_BREAK_RATE_DOWN:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.fItemUpgradeBreakBonusRate -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.fItemUpgradeBreakBonusRate += sItemOptTbl->nValue[0];
		}
			break;
		case SCOUTER_INFO_SOL:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.byLastSol -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.byLastSol += sItemOptTbl->nValue[0];
		}
			break;
		case SCOUTER_INFO_STR:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.byLastStr -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.byLastStr += sItemOptTbl->nValue[0];			
		}
			break;
		case SCOUTER_INFO_UPGRADE_RATE_UP:
		{
			if (bDecrease)
				plr->GetPcProfile()->avatarAttribute.fItemUpgradeBonusRate -= sItemOptTbl->nValue[0];
			else
				plr->GetPcProfile()->avatarAttribute.fItemUpgradeBonusRate += sItemOptTbl->nValue[0];
		}
			break;
		default:
		{
			//Not Implemented yet
			printf("Case not implemented yet, case struct number: %i", sItemOptTbl->byScouterInfo);
		}
			break;
	}
	memcpy(&this->sPlayerAttribute, &plr->GetPcProfile()->avatarAttribute, sizeof(sAVATAR_ATTRIBUTE));
	this->UpdateAvatarAttributes(Handle);
}