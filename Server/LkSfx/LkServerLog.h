//***********************************************************************************
//
//	File		:	LkServerLog.h
//
//	Begin		:	2007-03-14
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#pragma once

#include "LkLog.h"

#include "LkMemoryPool.h"
#include "LkMutex.h"

#include <deque>


//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
class CLkServerLogData
{
	DECLARE_DYNAMIC_MEMORYPOOL_THREADSAFE( CLkServerLogData );

	enum 
	{
		SERVER_BUFSIZE_LOG = 2048
	};

public:

	CLkServerLogData():
#ifdef _WIN64
	sequence( 0xFFFFFFFFFFFFFFFF )
#else
	sequence( 0xFFFFFFFF )
#endif
	{
		achLogText[0] = '\0';
	}

	~CLkServerLogData() {}


public:

	int				GetBufSize() { return SERVER_BUFSIZE_LOG; }

public:

	DWORD			dwSource;
	BYTE			byChannel;
	WORD			wStrLen;
	CHAR 			achLogText[SERVER_BUFSIZE_LOG + 1];

#ifdef _WIN64
	DWORD64			sequence;
	static DWORD64	curSequence;
#else
	DWORD			sequence;
	static DWORD	curSequence;
#endif

};


//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
class CLkServerLog : public CLkLog
{
public:

	CLkServerLog(void);

	virtual ~CLkServerLog(void);


public:

// overrides

	void							Log(BYTE byLogChannel, bool bDate, LPCTSTR lpszFile, int nLine,	LPCTSTR lpszFunc, LPCTSTR lpszText, ...);


protected:

	bool							PushLog(CLkServerLogData* pLogData);

	CLkServerLogData*				PopLog();

	CLkServerLogData*				PeekLog();


private:

	void							Init();

	void							Destroy();


private:

	CLkMutex						m_mutex;

	std::deque<CLkServerLogData*>	m_logQueue;
};
