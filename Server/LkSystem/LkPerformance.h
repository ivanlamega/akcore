//***********************************************************************************
//
//	File		:	LkPerformance.h
//
//	Begin		:	2007-02-08
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#pragma once

#include "LkPdh.h"


class CLkPerformance
{
public:

	CLkPerformance(void);

	~CLkPerformance(void);


public:

	int							Create(HANDLE hProcess = NULL);

public:

	SYSTEM_INFO *				GetSystemInfo() { return &m_systemInfo; }
	MEMORYSTATUSEX *			GetMemoryStatus(bool bRefresh = true);
	void						RefreshMemoryStatus() { GetMemoryStatus(true); }

	void						UpdateLog();


public:

	// cpu
	DWORD  						GetProcessProcessorLoad();
	DWORD						GetSystemProcessorLoad();

	// memory
	size_t						GetProcessMemoryUsage();
	DWORDLONG					GetSystemMemoryTotalPhy() { return m_memoryStatus.ullTotalPhys; }
	DWORDLONG					GetSystemMemoryAvailPhy() { return m_memoryStatus.ullAvailPhys; }
	DWORDLONG					GetSystemMemoryTotalVir() { return m_memoryStatus.ullTotalVirtual; }
	DWORDLONG					GetSystemMemoryAvailVir() { return m_memoryStatus.ullAvailVirtual; }
	DWORDLONG					GetSystemMemoryTotalPageFile() { return m_memoryStatus.ullTotalPageFile; }
	DWORDLONG					GetSystemMemoryAvailPageFile() { return m_memoryStatus.ullAvailPageFile; }
	DWORD 						GetSystemMemoryLoad() { return m_memoryStatus.dwMemoryLoad; }

	// network
	//LONG						GetSystemBandwidth(int nID);
	//LONG						GetSystemRecvBandwidth(int nID);
	//LONG						GetSystemSendBandwidth(int nID);
	//LONG						GetSystemTotalBandwidth(int nID);	

private:

	void						Init();

	void						Destroy();


private:

	HANDLE						m_hProcess;


	CLkPdh						m_pdhProcessUsage;

	CLkPdh						m_pdhSystemProcessorUsage;


	SYSTEM_INFO					m_systemInfo;

	MEMORYSTATUSEX				m_memoryStatus;
};