#ifndef __LK_NAVI_DATA_MNG_H__
#define __LK_NAVI_DATA_MNG_H__


#include "LkNaviData.h"


class CLkNaviDataMng
{
public:
	class CAutoCS
	{
	protected:
		CRITICAL_SECTION*			m_pCS;

	public:
		CAutoCS( CRITICAL_SECTION* pCS ) { m_pCS = pCS; EnterCriticalSection( m_pCS ); }
		~CAutoCS( void ) { LeaveCriticalSection( m_pCS ); }
	};

public:
	typedef std::list< CLkNaviInfo* > listdef_NAVI_INFO_LIST;

protected:
	CRITICAL_SECTION				m_cs;

	std::string						m_strPathName;

	// Export 용 변수
	CLkNaviInfo*					m_pCreatedWorldInfo;
	listdef_NAVI_INFO_LIST			m_defCreatedObjList;

	// Import 용 변수
	CLkNaviWorldInfo*				m_pLoadedWorldInfo;
	listdef_NAVI_INFO_LIST			m_defLoadedObjList;

public:
	CLkNaviDataMng( void );
	~CLkNaviDataMng( void );

public:
	bool							Create( const char* pPathName );
	void							Delete( void );

	//////////////////////////////////////////////////////////////////////////
	//
	//	Export
	//
	//////////////////////////////////////////////////////////////////////////

	// Info entity 생성
	CLkNaviWorldOutDoorInfo*		Create_WorldOutDoor( void );
	CLkNaviWorldInDoorInfo*		Create_WorldInDoor( void );
	CLkNaviGroupOutDoorInfo*		Create_GroupOutDoor( void );
	CLkNaviGroupInDoorInfo*		Create_GroupInDoor( void );
	CLkNaviPropOutDoorInfo*		Create_PropOutDoor( void );
	CLkNaviPropInDoorInfo*			Create_PropInDoor( void );

	// Info entity 제거
	void							Delete_NaviInfo( CLkNaviInfo*& pNaviInfo );
	void							Delete_AllNaviInfoExceptWorldInfo( void );
	void							Delete_AllNaviInfo( void );

	// 현재 생성된 모든 데이터를 파일에 저장한다
	bool							Flush( void );

	//////////////////////////////////////////////////////////////////////////
	//
	//	Import
	//
	//////////////////////////////////////////////////////////////////////////

	CLkNaviWorldInfo*				GetLoadedWorld( void );

	CLkNaviWorldInfo*				Load_World( void );
	CLkNaviWorldOutDoorInfo*		Load_WorldOutDoor( void );
	CLkNaviWorldInDoorInfo*		Load_WorldInDoor( void );
	CLkNaviGroupOutDoorInfo*		Load_GroupOutDoor( unsigned int uiGroupID );
	CLkNaviGroupInDoorInfo*		Load_GroupInDoor( void );
	CLkNaviPropOutDoorInfo*		Load_PropOutDoor( unsigned int uiFieldID, const char* pPath = NULL );
	CLkNaviPropInDoorInfo*			Load_PropInDoor( unsigned int uiBlockID, const char* pPath = NULL );

	void							Unload_NaviInfo( CLkNaviInfo*& pNaviInfo );
	void							Unload_AllNaviInfo( void );
};


inline CLkNaviWorldInfo* CLkNaviDataMng::GetLoadedWorld( void )
{
	return m_pLoadedWorldInfo;
}


#endif