//-----------------------------------------------------------------------------------
//		Char Server by Daneos @ Ragezone 
//-----------------------------------------------------------------------------------

#include "stdafx.h"
#include "CharServer.h"

#include "LkSfx.h"
#include "LkFile.h"

#include "LkPacketUC.h"
#include "LkPacketCU.h"
#include "ResultCode.h"

#include <iostream>
#include <map>
#include <list>

using namespace std;

//-----------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------
CClientSession::~CClientSession()
{
	//LK_PRINT(PRINT_APP, "CClientSession Destructor Called");
}


int CClientSession::OnAccept()
{
	//LK_PRINT( PRINT_APP, "%s", __FUNCTION__ );

	return LK_SUCCESS;
}


void CClientSession::OnClose()
{
	//LK_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	CCharServer * app = (CCharServer*)LkSfxGetApp();
}


int CClientSession::OnDispatch(CLkPacket * pPacket)
{
	CCharServer * app = (CCharServer*) LkSfxGetApp();
	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	//printf("~~~ opcode %i received ~~~ \n", pHeader->wOpCode);

	switch( pHeader->wOpCode )
	{
	case UC_LOGIN_REQ:
	{
		CClientSession::SendCharServerReq(pPacket);
	}
		break;
	case UC_CHARACTER_SERVERLIST_ONE_REQ:
	{
		CClientSession::SendServerListOneReq(pPacket);
	}
		break;
	case UC_CHARACTER_SERVERLIST_REQ:
	{
		printf("UC CHAR SERVER LIST REQ");
		CClientSession::SendServerListReq(pPacket,app);
	}
		break;
	case UC_CHARACTER_LOAD_REQ:
	{
		CClientSession::SendCharLoadReq(pPacket, app);
	}
		break;
	case UC_CHARACTER_ADD_REQ:
	{
		CClientSession::SendCharCreateReq(pPacket, app);
	}
		break;
	case UC_CHARACTER_DEL_REQ:
	{
		CClientSession::SendCharDeleteReq(pPacket, app);
	}
		break;
	case UC_CHARACTER_EXIT_REQ:
	{										
		CClientSession::SendCharExitReq(pPacket);
	}
		break;
	case UC_CONNECT_WAIT_CHECK_REQ:
	{
		CClientSession::SendCharWaitCheckReq(pPacket);
	}
		break;
	case UC_CHARACTER_SELECT_REQ:
	{
		CClientSession::SendCharSelectReq(pPacket);
	}
		break;
	case UC_CHARACTER_RENAME_REQ:
	{
		CClientSession::SendCharRenameReq(pPacket, app);
	}
		break;
	case UC_CONNECT_WAIT_CANCEL_REQ:
	{
		CClientSession::SendCancelWaitReq(pPacket);
	}
		break;
	default:
		return CLkSession::OnDispatch( pPacket );
	}

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		CharServerMain
//-----------------------------------------------------------------------------------
int CharServerMain(int argc, _TCHAR* argv[])
{
	CLkFileStream traceFileStream;
	CCharServer app;
// LOG FILE
	int rc = traceFileStream.Create( "charlog" );
	if( LK_SUCCESS != rc )
	{
		LK_PRINT(PRINT_APP, "log file CreateFile error %d(%s)", rc, LkGetErrorMessage( rc ) );
		return rc;
	}
// CHECK INT FILE
	LkSetPrintStream( traceFileStream.GetFilePtr() );
	LkSetPrintFlag( PRINT_APP | PRINT_SYSTEM );
	rc = app.Create(argc, argv, ".\\Server.ini");
	if( LK_SUCCESS != rc )
	{
		LK_PRINT(PRINT_APP, "Server Application Create Fail %d(%s)", rc, LkGetErrorMessage(rc) );
		return rc;
	}

	// CONNECT TO MYSQL
	app.db = new MySQLConnWrapper;
	app.db->setConfig(app.GetConfigFileHost(), app.GetConfigFileUser(), app.GetConfigFilePassword(), app.GetConfigFileDatabase());
	try
	{
		app.db->connect();
		printf("Connected to database server.\n\r");
	}
	catch (exception e)
	{
		printf("couldn't connect to database server ErrID:%s\n\r", e.what());
	}
	try
	{
		app.db->switchDb(app.GetConfigFileDatabase());
	}
	catch (exception e)
	{
		printf("Couldn't switch database to %s Error:%s\n\r", app.GetConfigFileDatabase(), e.what());
	}
	app.db2 = new MySQLConnWrapper;
	app.db2->setConfig(app.GetConfigFileHost(), app.GetConfigFileUser(), app.GetConfigFilePassword(), app.GetConfigFileDatabase());
	try
	{
		app.db2->connect();
		printf("Connected to database server.\n\r");
	}
	catch (exception e)
	{
		printf("couldn't connect to database server ErrID:%s\n\r", e.what());
	}
	try
	{
		app.db2->switchDb(app.GetConfigFileDatabase());
	}
	catch (exception e)
	{
		printf("Couldn't switch database to %s Error:%s\n\r", app.GetConfigFileDatabase(), e.what());
	}
	app.Start();
	Sleep(500);
	std::cout << "\n\n" << std::endl;
	std::cout << "\t  ____                              ____        _ _ " << std::endl;
	std::cout << "\t |  _ \\ _ __ __ _  __ _  ___  _ __ | __ )  __ _| | |" << std::endl;
	std::cout << "\t | | | | '__/ _` |/ _` |/ _ \\| '_ \\|  _ \\ / _` | | |" << std::endl;
	std::cout << "\t | |_| | | | (_| | (_| | (_) | | | | |_) | (_| | | |" << std::endl;
	std::cout << "\t |____/|_|  \\__,_|\\__, |\\___/|_| |_|____/ \\__,_|_|_|" << std::endl;
	std::cout << "\t                  |___/                             " << std::endl;
	std::cout << "\t______           AKCore :O 2014					______\n\n" << std::endl;
	app.WaitForTerminate();

	return 0;
}
