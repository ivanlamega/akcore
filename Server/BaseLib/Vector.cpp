#include "stdafx.h"
#include "Vector.h"


//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
CLkVector CLkVector::ZERO(0.0f, 0.0f, 0.0f);
CLkVector CLkVector::UNIT_X(1.0f, 0.0f, 0.0f);
CLkVector CLkVector::UNIT_Y(0.0f, 1.0f, 0.0f);
CLkVector CLkVector::UNIT_Z(0.0f, 0.0f, 1.0f);
CLkVector CLkVector::INVALID(INVALID_FLOAT, INVALID_FLOAT, INVALID_FLOAT);
CLkVector CLkVector::INVALID_XZ(INVALID_FLOAT, 0.0f, INVALID_FLOAT);
//-----------------------------------------------------------------------------------

