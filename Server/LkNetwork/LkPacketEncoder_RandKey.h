//***********************************************************************************
//
//	File		:	LkPacketEncoder_RandKey.h
//
//	Begin		:	2007-01-31
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#pragma once

#include "LkPacketEncoder.h"

#include "LkRandomGenerator.h"


class CLkPacketEncoder_RandKey : public CLkPacketEncoder
{
public:

	CLkPacketEncoder_RandKey(bool bSeedSwap = false);

	~CLkPacketEncoder_RandKey(void);


public:

	int						RxDecrypt(void * pvPacketHeader);

	int						TxEncrypt(void * pvPacketHeader);

	int						RxDecrypt(CLkPacket& rPacket);

	int						TxEncrypt(CLkPacket& rPacket);


private:

	CLkRandomGenerator		m_rxBodyKeyGenerator;

	CLkRandomGenerator		m_txBodyKeyGenerator;

	CLkRandomGenerator		m_rxHeaderKeyGenerator;

	CLkRandomGenerator		m_txHeaderKeyGenerator;
};
