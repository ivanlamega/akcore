//***********************************************************************************
//
//	File		:	LkLinkList.h
//
//	Begin		:	2005-11-30
//
//	Copyright	:	ⓒ NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	Linked List
//
//***********************************************************************************

#pragma once


//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
class CLkLinkObject
{
public:

	CLkLinkObject()
		:m_pPrev(NULL),m_pNext(NULL) {}

	virtual ~CLkLinkObject() {}


public:

	//
	CLkLinkObject *		GetNext() const { return m_pNext; }

	//
	void					SetNext(CLkLinkObject * pNext) { m_pNext = pNext; }

	//
	CLkLinkObject *		GetPrev() const { return m_pPrev; }

	//
	void					SetPrev(CLkLinkObject * pPrev) { m_pPrev = pPrev; }


private:

	CLkLinkObject *		m_pPrev;

	CLkLinkObject *		m_pNext;
};


//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
class CLkLinkList
{
public:

	CLkLinkList(void)
		:m_pHead(NULL), m_pTail(NULL), m_iCount(0), m_iMaxCount(0) {}

	virtual ~CLkLinkList(void) {}

public:

	// 비었는가
	bool					IsEmpty() const { return m_iCount == 0; }

	// 현재 리스트에 있는 개수
	int						GetCount() const { return m_iCount; }

	// 리스트의 최대 사용 개수
	int						GetMaxCount() const { return m_iMaxCount; }

	// 맨 처음
	CLkLinkObject *		GetFirst() const { return m_pHead; }

	// 맨 끝
	CLkLinkObject *		GetLast() const { return m_pTail; }


public:

	// 맨뒤에 추가
	void					Append(CLkLinkObject * pLinkObject);

	// 맨앞에 추가
	void					Prepend(CLkLinkObject * pLinkObject);

	// 원하는 위치 앞에 추가
	void					InsertBefore(CLkLinkObject * pBaseObject, CLkLinkObject * pLinkObject);

	// 원하는 위치 뒤에 추가
	void					InsertAfter(CLkLinkObject * pBaseObject, CLkLinkObject * pLinkObject);

	// 하나 제거
	void					Remove(CLkLinkObject * pLinkObject);

	// 모두 제거
	void					RemoveAll();

	//
	bool					Find(CLkLinkObject * pBaseObject);



public:

	//
	CLkLinkObject *		Pop(bool bFront = false);

	//
	void					Push(CLkLinkObject * pLinkObject, bool bFront = true);

	//
	CLkLinkObject *		PopFront();

	//
	CLkLinkObject *		PopBack();

	//
	void					PushBack(CLkLinkObject * pLinkObject) { Append(pLinkObject); }

	//
	void					PushFront(CLkLinkObject * pLinkObject) { Prepend(pLinkObject); }



	// 테스트용 로직 점검
	static void				UnitTest();


private:

	CLkLinkObject *		m_pHead;

	CLkLinkObject *		m_pTail;

	int						m_iCount;

	int						m_iMaxCount;

};


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkLinkList::Append(CLkLinkObject * pLinkObject)
{
	if( !m_pHead )
	{
		m_pHead = pLinkObject;
	}

	pLinkObject->SetPrev( m_pTail );

	if( m_pTail )
	{
		m_pTail->SetNext( pLinkObject );
	}

	m_pTail = pLinkObject;

	pLinkObject->SetNext( NULL );


	++m_iCount;

	if( m_iCount > m_iMaxCount)
	{
		m_iMaxCount = m_iCount;
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkLinkList::Prepend(CLkLinkObject * pLinkObject)
{
	if( !m_pTail )
	{
		m_pTail = pLinkObject;
	}

	pLinkObject->SetNext( m_pHead );

	if( m_pHead )
	{
		m_pHead->SetPrev( pLinkObject );
	}

	m_pHead = pLinkObject;

	pLinkObject->SetPrev( NULL );


	++m_iCount;

	if( m_iCount > m_iMaxCount)
	{
		m_iMaxCount = m_iCount;
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkLinkList::InsertBefore(CLkLinkObject * pBaseObject, CLkLinkObject * pLinkObject)
{
	if( !pBaseObject )
	{
		Prepend( pLinkObject );
	}
	else
	{
		CLkLinkObject * pPrev = pBaseObject->GetPrev();

		pLinkObject->SetNext( pBaseObject );
		pLinkObject->SetPrev( pPrev );
		pBaseObject->SetPrev( pLinkObject );

		if( !pPrev )
		{
			m_pHead = pLinkObject;
		}
		else
		{
			pPrev->SetNext( pLinkObject );
		}

		++m_iCount;

		if( m_iCount > m_iMaxCount)
		{
			m_iMaxCount = m_iCount;
		}
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkLinkList::InsertAfter(CLkLinkObject * pBaseObject, CLkLinkObject * pLinkObject)
{
	if( !pBaseObject )
	{
		Append( pLinkObject );
	}
	else
	{
		CLkLinkObject * pNext = pBaseObject->GetNext();

		pLinkObject->SetPrev( pBaseObject );
		pLinkObject->SetNext( pNext );
		pBaseObject->SetNext( pLinkObject );

		if( !pNext )
		{
			m_pTail = pLinkObject;
		}
		else
		{
			pNext->SetPrev( pLinkObject );
		}

		++m_iCount;

		if( m_iCount > m_iMaxCount)
		{
			m_iMaxCount = m_iCount;
		}
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkLinkList::Remove(CLkLinkObject * pLinkObject)
{
	CLkLinkObject * pPrev = pLinkObject->GetPrev();
	CLkLinkObject * pNext = pLinkObject->GetNext();

	if( pNext )
	{
		pNext->SetPrev( pPrev );
	}
	else
	{
		m_pTail = pPrev;
	}

	if( pPrev )
	{
		pPrev->SetNext( pNext );
	}
	else
	{
		m_pHead = pNext;
	}

	pLinkObject->SetPrev( NULL );
	pLinkObject->SetNext( NULL );

	--m_iCount;

}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkLinkList::RemoveAll()
{
	while (m_iCount > 0)
	{
		Remove( m_pHead );
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:	속도를 고려한 부분에서는 사용을 피할 것
//		Return	:
//-----------------------------------------------------------------------------------
inline bool CLkLinkList::Find(CLkLinkObject * pBaseObject)
{
	CLkLinkObject * pObject = m_pHead;

	while( pObject )
	{
		if( pObject == pBaseObject )
		{
			return true;
		}

		pObject = pObject->GetNext();
	}

	return false;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkLinkObject * CLkLinkList::PopFront()
{
	CLkLinkObject * pLinkObject = m_pHead;

	if( pLinkObject )
	{
		Remove( pLinkObject );
	}

	return pLinkObject;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkLinkObject * CLkLinkList::PopBack()
{
	CLkLinkObject * pLinkObject = m_pTail;

	if( pLinkObject )
	{
		Remove( pLinkObject );
	}

	return pLinkObject;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline CLkLinkObject * CLkLinkList::Pop(bool bFront)
{
	return bFront ? PopFront() : PopBack();
}

//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
inline void CLkLinkList::Push(CLkLinkObject * pLinkObject, bool bFront)
{
	return bFront ? Prepend(pLinkObject) : Append(pLinkObject);
}

