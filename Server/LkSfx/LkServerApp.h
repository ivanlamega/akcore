//***********************************************************************************
//
//	File		:	LkServerApp.h
//
//	Begin		:	2007-01-17
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#pragma once


#include <tchar.h>

#include "LkNetwork.h"

#include "LkThread.h"
#include "LkSingleton.h"
#include "LkPerformance.h"

class CLkSessionFactory;
class CLkServerLog;

class CLkServerApp : public CLkRunObject, public CLkSingleton<CLkServerApp>
{
public:

	CLkServerApp();

	virtual ~CLkServerApp();


public:


// virtual 

	virtual int						OnInitApp();

	virtual int						OnCreate();

	virtual int						OnCommandArgument(int argc, _TCHAR* argv[]);

	virtual int						OnConfiguration(const char * lpszConfigFile);

	virtual int						OnAppStart();

	virtual void					OnRun();


public:


	int								Create(int argc, _TCHAR* argv[], const char * lpszConfigFile = NULL);

	void							Destroy();

	void							Start(bool bAutoDelete = false);

	void							Stop();

	void							WaitForTerminate();

	int								Send(HSESSION hSession, CLkPacket * pPacket) { return m_network.Send(hSession, pPacket); }

	int								SendTo(CLkSession * pSession, CLkPacket * pPacket) { return m_network.Send(pSession, pPacket); }


	CLkNetwork *					GetNetwork() { return &m_network; }

	CLkPerformance *				GetPerformance() { return &m_performance; }

	CLkServerLog *					GetServerLog() { return m_pServerLog; }

	void							SetConfigFileName(const char * lpszAppName, const char * lpszFileName);


protected:

	void							Init();


private:

	void							run() { this->OnRun(); }


protected:

	CLkSessionFactory *			m_pSessionFactory;

	CLkServerLog *					m_pServerLog;

	CLkNetwork 					m_network;

	CLkPerformance					m_performance;

	int								m_nMaxSessionCount;

	std::string						m_strConfigFile;


private:

	CLkThread *					m_pAppThread;
};

#define LkSfxGetApp()			CLkServerApp::GetInstance()
#define g_pApp					LkSfxGetApp()