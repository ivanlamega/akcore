//***********************************************************************************
//
//	File		:	SessionList.cpp
//
//	Begin		:	2007-01-02
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************


#include "stdafx.h"
#include "LkSessionList.h"

#include "LkNetwork.h"
#include "LkSession.h"

#include "LkLog.h"
#include "LkError.h"



//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkSessionList::CLkSessionList()
{
	Init();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkSessionList::~CLkSessionList()
{
	Destroy();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSessionList::Create(CLkNetwork * pNetwork, int nSessionSize, int nExtraSize)
{
	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();


	if( false == m_sessionList.Create( nSessionSize, nExtraSize) )
	{
		return LK_FAIL;
	}


	m_pNetworkRef = pNetwork;


	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkSessionList::Destroy()
{
	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();


	CLkSession ** ppSession = NULL;
	for( LISTIT it = m_sessionList.Begin(); it != m_sessionList.End(); it = m_sessionList.Next( it ))
	{
		ppSession = m_sessionList.GetPtr( it );
		if( *ppSession )
		{
			SAFE_DELETE( *ppSession );
		}
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkSessionList::Init()
{
	m_pNetworkRef = NULL;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkSession * CLkSessionList::Find(LISTIT pos)
{
	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();

	CLkSession ** ppSession = m_sessionList.GetPtr( pos );
	if( NULL == ppSession )
	{
		return NULL;
	}

	return *ppSession;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
bool CLkSessionList::Add(CLkSession * pSession)
{
	if( NULL == pSession )
	{
		LK_LOG_ASSERT("NULL == pSession");
		return false;
	}

	if( INVALID_HSESSION != pSession->GetHandle() )
	{
		LK_LOG_ASSERT("INVALID_HSESSION != pSession->GetHandle(), pSession->GetHandle() = %u", pSession->GetHandle());
		return false;
	}


	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();

	LISTIT it = m_sessionList.Insert( pSession );
	if( it == m_sessionList.End() )
	{
		return false;
	}

	pSession->m_hSession = it;


	return true;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
bool CLkSessionList::Remove(CLkSession * pSession)
{
	if( NULL == pSession )
	{
		LK_LOG_ASSERT("NULL == pSession");
		return false;
	}

	if( INVALID_HSESSION ==  pSession->GetHandle() )
	{
		return true;
	}


	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();

	m_sessionList.Remove( pSession->GetHandle() );

	pSession->m_hSession = INVALID_HSESSION;


	return true;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSessionList::GetCurCount()
{
	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();

	return m_sessionList.GetSize();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSessionList::GetMaxCount()
{
	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();

	return m_sessionList.GetMaxSize();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkSessionList::ValidCheck(DWORD dwTickTime)
{
	UNREFERENCED_PARAMETER( dwTickTime );

	CLkAutoMutex mutex( &m_mutex );
	mutex.Lock();

	CLkSession * pSession = NULL;
	for( LISTIT it = m_sessionList.Begin(); it != m_sessionList.End(); )
	{
		pSession = *( m_sessionList.GetPtr( it ) );

		if( pSession )
		{
			if( false == pSession->ValidCheck( dwTickTime ) )
			{
				LK_PRINT(PRINT_SYSTEM, "The session[%X] should be disconnected due to timeout.", pSession);

				pSession->Disconnect( false );	
			}

			if( true == pSession->IsShutdownable() )
			{
				pSession->Shutdown();

				m_pNetworkRef->PostNetEventMessage( (WPARAM)NETEVENT_CLOSE, (LPARAM)pSession );

				it = m_sessionList.Remove( it );

				continue;
			}
		}

		it = m_sessionList.Next( it );
	}

}