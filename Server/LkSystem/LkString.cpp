//***********************************************************************************
//
//	File		:	LkString.cpp
//
//	Begin		:	2005-12-01
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#include "stdafx.h"
#include "LkString.h"

#include <stdio.h>
#include <stdarg.h>

const unsigned int	MAX_FORMAT_STR_BUFF = 2048;


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkString & CLkString::operator=(const char * lpszStr)
{
	m_str.assign( lpszStr );

	return *this;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkString & CLkString::operator=(const WCHAR * pwszString)
{
	int nStrLen = WideCharToMultiByte( ::GetACP(), 0, pwszString, -1, NULL, 0, NULL, NULL );
	char * pString = new char[ nStrLen ];
	if( pString )
	{
		WideCharToMultiByte( ::GetACP(), 0, pwszString, -1, pString, nStrLen, NULL, NULL );

		m_str.assign( pString );

		delete[] pString;
	}

	return *this;
}

//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkString::CLkString(const WCHAR * pwszString)
{
	*this = pwszString;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkString::Format(const char *format, ...)
{
	int nRV = 0;
	char szBuf[MAX_FORMAT_STR_BUFF] = { 0x00, };

    va_list valist;

    memset(szBuf, 0x00, sizeof(szBuf));

    va_start(valist, format);

#if ( _MSC_VER >= 1400 ) // VS8+
    nRV = vsprintf_s(szBuf, MAX_FORMAT_STR_BUFF, format, valist);
#else
    nRV = vsprintf(szBuf, format, valist);
#endif

    va_end(valist);

    if( nRV > 0 )
		m_str.assign( szBuf );
	else
		m_str.clear();

    return nRV;
}