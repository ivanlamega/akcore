//***********************************************************************************
//
//	File		:	SessionList.h
//
//	Begin		:	2007-01-02
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#pragma once

#include "LkLinkArray.h"
#include "LkMutex.h"


class CLkNetwork;
class CLkSession;

class CLkSessionList
{
public:

	CLkSessionList();

	virtual ~CLkSessionList();


public:

	int									Create(CLkNetwork * pNetwork, int nSessionSize, int nExtraSize);

	void								Destroy();


public:

	bool								Add(CLkSession * pSession);

	bool								Remove(CLkSession * pSession);

	CLkSession *						Find(HSESSION hSession);

	void								ValidCheck(DWORD dwTickTime);


public:

	int									GetCurCount();

	int									GetMaxCount();


protected:

	void								Init();


private:

	typedef CLkLinkArray_Static<CLkSession*> LIST;
	typedef LIST::POSITION LISTIT;

	LIST								m_sessionList;

	CLkMutex							m_mutex;

	CLkNetwork *							m_pNetworkRef;

};

