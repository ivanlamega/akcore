#pragma once

#include "SharedType.h"
#include <conio.h>
#include <stdio.h>
#include <dos.h>
#include <ctype.h>
#include <windows.h>

#ifndef CHAT_FUNCTIONS_CLASS_H
#define CHAT_FUNCTIONS_CLASS_H
enum eSERVER_TEXT_TYPE
{
	SERVER_TEXT_SYSTEM,		// Only in Chat Window
	SERVER_TEXT_NOTICE,		// Only on screen = NOTIFY
	SERVER_TEXT_SYSNOTICE,	// On Chatwindow and Screen
	SERVER_TEXT_EMERGENCY,	// With Tori on screen = NPC NOTIFY

	SERVER_TEXT_TYPE_COUNT,
};

class CChatServer;
class CClientSession;
class CLkPacket;

class ChatFunctionsClass
{
public:
	ChatFunctionsClass(){};
	~ChatFunctionsClass(){};

public:
	void						AddRemoveFriend(int CharID, int TargetID, bool bRemove = false,bool toBlackList = false);
	void						AddRemoveBlackList(int CharID, int TargetID, bool bRemove = false);
	void						ErrorHandler(CClientSession * session, eSERVER_TEXT_TYPE errorType, const wchar_t * message);
	sDBO_GUILD_MEMBER_INFO*		GetGuildMemberInfo(int GuildID, int CharID);
};

#endif