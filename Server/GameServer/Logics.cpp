#include "stdafx.h"
#include "Logics.h"

/**
* -Luiz45
* Create All Game Logics Classes every logic for everything must be placed here
* without that what we are??
**/
GameLogics::GameLogics()
{
}
/*
* -Luiz45
* Unload our Classe from the memory(Best way to prevent Memory Leak)
*/
GameLogics::~GameLogics()
{
}
/*
* -Luiz45
* This function will check if the item already exist on player
* Return true if exist, false if not exist
*/
bool GameLogics::Logic_ItemExist(TBLIDX itemTblidx, RwUInt32 charID)
{
	PlayersMain* plr = g_pPlayerManager->GetPlayerByID(charID);
	bool bExist = false;
	for (int i = 0; i < plr->cPlayerInventory->GetTotalItemsCount(); i++)
	{
		if (plr->cPlayerInventory->GetInventory()[i].tblidx == itemTblidx)
		{
			bExist = true;
			break;
		}
	}
	plr = NULL;
	delete plr;
	return bExist;
}

/**
* -Kalisto
* This function checks for what class the TBLIDX is in
* and returns the class ie ITEM_RECOvERY or ITEM_GLOVE etc.
*/
eITEM_TYPE Logic_GetItemClass(TBLIDX itemTblidx)
{
	CGameServer * app = (CGameServer*)LkSfxGetApp();	
	 CItemTable* itemTable = app->g_pTableContainer->GetItemTable();
	 sITEM_TBLDAT* itemDat = reinterpret_cast<sITEM_TBLDAT*>(itemTable->FindData(itemTblidx));
	
	 delete app;
	 delete itemTable;
	 return (eITEM_TYPE)itemDat->byItem_Type;
}

/**
* -Kalisto
* This function checks the players inventory and sees if
* he has any available space in the bag.
* returns true for available space and false if inventory is full
*/
bool Logic_GetBagAvailable(PlayersInventory inventory)
{
	return false;
}