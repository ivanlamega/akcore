#include "stdafx.h"
#include "LkSfx.h"
#include "LkPacketUA.h"
#include "LkPacketAU.h"
#include "ResultCode.h"

#include "AuthServer.h"



//--------------------------------------------------------------------------------------//
//		Get the account ID and log in to Char Server									//
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharLogInReq(CLkPacket * pPacket, CAuthServer * app) 
{
	LK_PRINT(PRINT_APP, "Client Received Login Request");

	sUA_LOGIN_REQ * req = (sUA_LOGIN_REQ *)pPacket->GetPacketData();

	CLkPacket packet(sizeof(sAU_LOGIN_RES));
	sAU_LOGIN_RES * res = (sAU_LOGIN_RES *)packet.GetPacketData();


	res->wOpCode = AU_LOGIN_RES;
	memcpy(res->awchUserId, req->awchUserId, LK_MAX_SIZE_USERID_UNICODE);
	strcpy_s((char*)res->abyAuthKey, LK_MAX_SIZE_AUTH_KEY, "Dbo");


	app->db->prepare("CALL AuthLogin (? ,?, @acc_id, @result_code)");
	app->db->setString(1, Lk_WC2MB(req->awchUserId));
	app->db->setString(2, Lk_WC2MB(req->awchPasswd));
	app->db->execute();
	app->db->execute("SELECT @acc_id, @result_code");
	app->db->fetch(); 

	res->wResultCode = app->db->getInt("@result_code");
	res->accountId = app->db->getInt("@acc_id");
	
	string isEnabled = app->GetConfigFileEnabledMultipleServers();
	if (isEnabled == "true")
	{
		//Loading Max Server Files Configuration
		for(int i =0;i<app->GetConfigFileMaxServers();i++)
		{
			const char* CharIP = app->ServersConfig[i][0].c_str();
			const DWORD CharPort = atoi(app->ServersConfig[i][1].c_str());
			strcpy_s(res->aServerInfo[i].szCharacterServerIP, LK_MAX_LENGTH_OF_IP, CharIP);
			res->aServerInfo[i].wCharacterServerPortForClient = CharPort;
			res->aServerInfo[i].dwLoad = 0;		
		}
		res->byServerInfoCount = app->GetConfigFileMaxServers();
		res->lastServerFarmId = INVALID_SERVERFARMID;
	}
	else
	{
		res->byServerInfoCount = 1;
		strcpy_s(res->aServerInfo[0].szCharacterServerIP, LK_MAX_LENGTH_OF_IP, app->GetConfigFileExternalIP());
		res->dwAllowedFunctionForDeveloper = 0x01;
		res->aServerInfo[0].wCharacterServerPortForClient = 20300;
		res->aServerInfo[0].dwLoad = 0;
		res->lastServerFarmId = 0;
	}
	
	

	packet.SetPacketLen(sizeof(sAU_LOGIN_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);
		if (LK_SUCCESS != rc)
		{
			LK_PRINT(PRINT_APP, "Failed to send packet %d(%s)", rc, LkGetErrorMessage(rc));
		}
		else{
			LK_PRINT(PRINT_APP, "User %S send to charserver", req->awchUserId);
		 }
}

//--------------------------------------------------------------------------------------//
//		Disconnect from Auth Server
//--------------------------------------------------------------------------------------//
void CClientSession::SendLoginDcReq(CLkPacket * pPacket) 
{
	CLkPacket packet(sizeof(sAU_LOGIN_DISCONNECT_RES));
	sAU_LOGIN_DISCONNECT_RES * res = (sAU_LOGIN_DISCONNECT_RES *)packet.GetPacketData();
	res->wOpCode = AU_LOGIN_DISCONNECT_RES;

	packet.SetPacketLen(sizeof(sAU_LOGIN_DISCONNECT_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);
	this->Disconnect(true);
}
