#pragma once



#include "MobManager.h"
#include "LkPacketEncoder_RandKey.h"
#include "LkFile.h"
#include "Memoryhandle.h"
#include "LkPacketUG.h"
#include "LkPacketGU.h"
#include "ResultCode.h"

#include "GsFunctions.h"
#include "QueryClass.h"

#include "LkItem.h"
#include "Battle.h"
// LOAD TABLES
#include "DragonBallRewardTable.h"
#include "DragonBallTable.h"
#include "Table.h"
#include "NPCTable.h"
#include "TableContainer.h"
#include "WorldTable.h"
#include "MobTable.h"
#include "PCTable.h"
#include "SkillTable.h"
#include "HTBSetTable.h"
#include "TableFileNameList.h"
#include "WorldMapTable.h"
#include "WorldPlayTable.h"
#include "DungeonTable.h"
#include "WorldZoneTable.h"
#include "DirectionLinkTable.h"
#include "ObjectTable.h"
#include "PortalTable.h"
#include "MerchantTable.h"
#include "ItemTable.h"
#include "NewbieTable.h" // For Tutorial
#include "UseItemTable.h" //Added for use items
#include "SystemEffectTable.h" //Added for use some Effects validations and buffs
#include "TimeQuestTable.h"
#include "FormulaTable.h"
#include "ItemMixMachineTable.h"
#include "ExpTable.h"
#include "VehicleTable.h"//Added to use Vehicle -Luiz45
#include "QuestRewardTable.h"
#include "EachDropTable.h"
#include "ExcellentDropTable.h"
#include "LegendaryDropTable.h"
#include "NormalDropTable.h"
#include "SuperiorDropTable.h"
#include "TypeDropTable.h"
#include "MobMovePatternTable.h"
#include "QuestDropTable.h"
#include "BasicDropTable.h"
#include "ItemOptionTable.h"
#include "DynamicObjectTable.h"
// END TABLES
#include "MobActivity.h"
#include "LkBitFlagManager.h"
#include "PlayerParty.h"
#include <MMSystem.h>
#include <iostream>
#include <map>
#include "ObjectManager.h"
#include "PlayerManager.h"
#include "MainClass.h"
#include "TLQHandler.h"
//NTL Simulation Teste Code
//#include "LkSLEvent.h"
//#include "LkSobManager.h"

#include <random> //For real randoms
//Packet Name Reading
#define PACKET_PSYCHIC 1
enum APP_LOG
{
	PRINT_APP = 2,
};
enum GAME_SESSION
{
	SESSION_CLIENT,
};
struct sSERVERCONFIG
{
	CLkString		strClientAcceptAddr;
	WORD			wClientAcceptPort;
	CLkString		ExternalIP;
	CLkString		Host;
	CLkString		User;
	CLkString		Password;
	CLkString		Database;
	int				PlayerLimit;
	int				PlayerSaveInterval;
	int				PlayerSaveStatsSaveOnlyOnLogout;
};

enum eREWARD_TYPE
{
	eREWARD_TYPE_NORMAL_ITEM,
	eREWARD_TYPE_QUEST_ITEM,
	eREWARD_TYPE_EXP,
	eREWARD_TYPE_SKILL,
	eREWARD_TYPE_ZENY,
	eREWARD_TYPE_CHANGE_CLASS,
	eREWARD_TYPE_BUFF,
	eREWARD_TYPE_PROBABILITY,
	eREWARD_TYPE_REPUTATION,
	eREWARD_TYPE_CHANGE_ADULT,
	eREWARD_TYPE_GET_CONVERT_CLASS_RIGHT,

	eREWARD_TYPE_INVALID				= 0xffffffff
};

enum eDIRECTION_TYPE
{
	DIRECTION_FLASH, // 2D 연출
	DIRECTION_CINEMATIC, // 씨네마틱 연출
	DIRECTION_PLAY, // 서버에서 지정한 애니메이션등의 연출 (씨네마틱과는 달리 동기화 된다 ) -> 삭제 예정

	MAX_DIECTION_TYPE,
	INVALID_DIRECTION_TYPE = 0xFF,
};

const DWORD					MAX_NUMOF_GAME_CLIENT = 5;
const DWORD					MAX_NUMOF_SERVER = 1;
const DWORD					MAX_NUMOF_SESSION = MAX_NUMOF_GAME_CLIENT + MAX_NUMOF_SERVER;

class CGameServer;
class CTableContainer;
class CTableFileNameList;
class CLkBitFlagManager;

//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
class CClientSession : public CLkSession
{
public:

	CClientSession(bool bAliveCheck = true , bool bOpcodeCheck = true)
		:CLkSession( SESSION_CLIENT )
	{
		//SetControlFlag( CONTROL_FLAG_USE_SEND_QUEUE );
		//SetControlFlag( CONTROL_FLAG_USE_RECV_QUEUE );
		if( bAliveCheck )
		{
			SetControlFlag( CONTROL_FLAG_CHECK_ALIVE );
		}

		if( bOpcodeCheck )
		{
			SetControlFlag( CONTROL_FLAG_CHECK_OPCODE );
		}

		SetPacketEncoder( &m_packetEncoder );
	}

	~CClientSession();


public:
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CLkPacket * pPacket);

	//
	RwUInt32					GetavatarHandle() { return avatarHandle; }
	RwUInt32					GetTargetSerialId() { return m_uiTargetSerialId; }

	//
	//Client Packet functions
	void						SendGameEnterReq(CLkPacket * pPacket, CGameServer * app);
	void						SendAvatarCharInfo(CLkPacket * pPacket, CGameServer * app);
	//void						CheckPlayerStat(CGameServer * app, sPC_TBLDAT *pTblData, int level, RwUInt32 playerHandle, bool bUpdate = false);
	void						SendAvatarItemInfo(CLkPacket * pPacket, CGameServer * app);
	void						SendAvatarSkillInfo(CLkPacket * pPacket, CGameServer * app);
	void						SendAvatarHTBInfo(CLkPacket * pPacket, CGameServer * app);
	void						SendAvatarBuffInfo(CLkPacket * pPacket, CGameServer * app);
	void						SendAvatarQuestList(CLkPacket * pPacket, CGameServer * app);
	void						SendSlotInfo(CLkPacket * pPacket, CGameServer * app);
	void						SendAvatarInfoEnd(CLkPacket * pPacket);
	void						SendAuthCommunityServer(CLkPacket * pPacket, CGameServer * app);
	void						SendServerChangeReq(CLkPacket * pPacket, CGameServer * app);

	void						SendWorldEnterReq(CLkPacket * pPacket, CGameServer * app);
//	void						SendPlayerCreate(CLkPacket * pPacket, CGameServer * app);
	void						SendNpcCreate(CLkPacket * pPacket, CGameServer * app);
	void						SendMonsterCreate(CLkPacket * pPacket, CGameServer * app);
	void						SendEnterWorldComplete(CLkPacket * pPacket);
	//Character Funcs
	void						SendCharReadyReq(CLkPacket * pPacket, CGameServer * app);
	void						SendTutorialHintReq(CLkPacket * pPacket, CGameServer * app);
	void						SendCharReady(CLkPacket * pPacket);
	void						SendCharMove(CLkPacket * pPacket, CGameServer * app);
	void						SendCharDestMove(CLkPacket * pPacket, CGameServer * app);
	void						SendCharMoveSync(CLkPacket * pPacket, CGameServer * app);
	void						SendCharChangeHeading(CLkPacket * pPacket, CGameServer * app);
	void						SendCharJump(CLkPacket * pPacket, CGameServer * app);
	void						SendCharDashKeyBoard(CLkPacket * pPacket, CGameServer * app);
	void						SendCharDashMouse(CLkPacket * pPacket, CGameServer * app);
	void						SendCharChangeDirOnFloating(CLkPacket * pPacket, CGameServer * app);
	void						SendCharFalling(CLkPacket * pPacket, CGameServer * app);
	void						SendCharExitReq(CLkPacket * pPacket, CGameServer * app);
	void						SendGameLeaveReq(CLkPacket * pPacket, CGameServer * app);
	void						RecvServerCommand(CLkPacket * pPacket, CGameServer * app);
	void						SendUpdateCharSpeed(float fSpeed, CGameServer * app);
	void						SendCharTargetSelect(CLkPacket * pPacket);
	void						SendCharTargetFacing(CLkPacket * pPacket);
	void						SendCharTargetInfo(CLkPacket * pPacket);
	void						SendCharSitDown(CLkPacket * pPacket, CGameServer * app);
	void						SendCharStandUp(CLkPacket * pPacket, CGameServer * app);
	void						SendCharSkillUpgrade(CLkPacket * pPacket, CGameServer * app);
	void						SendCharFollowMove(CLkPacket * pPacket, CGameServer * app);
	void						SendExcuteTriggerObject(CLkPacket * pPacket, CGameServer * app);
	void						SendCharBindReq(CLkPacket * pPacket, CGameServer * app);
	void						SendPortalStartReq(CLkPacket * pPacket, CGameServer * app);
	void						SendPortalAddReq(CLkPacket * pPacket, CGameServer * app);
	void						SendPortalTelReq(CLkPacket * pPacket, CGameServer * app);
	void						SendAttackBegin(CLkPacket * pPacket, CGameServer * app);
	void						SendAttackEnd(CLkPacket * pPacket, CGameServer * app);
	void						SendCharToggleFighting(CLkPacket * pPacket, CGameServer * app);
	void						AddAttackBegin(RwUInt32 uiSerialId, RwUInt32 m_uiTargetSerialId);
	void						RemoveAttackBegin(RwUInt32 uiSerialId, RwUInt32 m_uiTargetSerialId);
	void						SendCharActionAttack(RwUInt32 uiSerialId, RwUInt32 m_uiTargetSerialId, CLkPacket * pPacket);
	void						SendMobActionAttack(RwUInt32 uiSerialId, RwUInt32 m_uiTargetSerialId, CLkPacket * pPacket);
	void						SendCharUpdateFaintingState(CLkPacket * pPacket, CGameServer * app, RwUInt32 uiSerialId, RwUInt32 m_uiTargetSerialId);
	void						SendCharUpdateLp(CLkPacket * pPacket, CGameServer * app, RwUInt16 wLp, RwUInt32 m_uiTargetSerialId);
	void						SendGmtUpdateReq(CLkPacket * pPacket, CGameServer * app);
	void						SendCharRevivalReq(CLkPacket * pPacket, CGameServer * app);
	//Guild
	void						SendGuildCreateReq(CLkPacket * pPacket, CGameServer * app);
	void						SendCreateDojo(CLkPacket * pPacket, CGameServer * app);
	//Party
	void						SendPartyInviteReq(CLkPacket * pPacket, CGameServer * app);
	void						SendCreatePartyReq(CLkPacket * pPacket, CGameServer * app);
	void						SendDisbandPartyReq(CLkPacket * pPacket, CGameServer * app);
	void						SendPartyLeaveReq(CLkPacket * pPacket, CGameServer * app);
	void						SendPartyResponse(CLkPacket * pPacket, CGameServer * app);
	void						SendPartyChangeDiff(CLkPacket * pPacket, CGameServer * app);
	void						SendPartyChangeZenny(CLkPacket * pPacket, CGameServer * app);
	void						SendPartyChangeItem(CLkPacket * pPacket, CGameServer * app);
	//Mail System
	void						SendCharMailStart(CLkPacket * pPacket, CGameServer * app);
	void						SendCharMailLoadReq(CLkPacket * pPacket, CGameServer * app);
	void						SendCharMailReloadReq(CLkPacket * pPacket, CGameServer * app);
	void						SendCharMailReadReq(CLkPacket * pPacket, CGameServer * app);
	void						SendCharMailSendReq(CLkPacket * pPacket, CGameServer * app);
	void						SendCharMailDelReq(CLkPacket * pPacket, CGameServer * app);
	void						SendCharMailItemReceiveReq(CLkPacket * pPacket, CGameServer * app);
	void						SendCharMailMultiDelReq(CLkPacket * pPacket, CGameServer * app);
	void						SendCharMailLockReq(CLkPacket * pPacket, CGameServer * app);
	void						SendCharMailReturnReq(CLkPacket * pPacket, CGameServer * app);
	//Afk Status
	void						SendCharAwayReq(CLkPacket * pPacket, CGameServer * app);
	//Shop
	void						SendShopStartReq(CLkPacket * pPacket, CGameServer * app);
	void						SendShopBuyReq(CLkPacket * pPacket, CGameServer * app);
	void						SendShopEndReq(CLkPacket * pPacket, CGameServer * app);
	void						SendShopSellReq(CLkPacket * pPacket, CGameServer * app);


	void						SendShopSkillReq(CLkPacket * pPacket, CGameServer * app);
	void						SendCharLearnSkillReq(CLkPacket * pPacket, CGameServer * app);
	//ITEMS
	void						SendItemMoveReq(CLkPacket * pPacket, CGameServer * app);
	void						SendItemDeleteReq(CLkPacket * pPacket, CGameServer * app);
	void						SendItemStackReq(CLkPacket * pPacket, CGameServer * app);
	void						SendItemUseReq(CLkPacket * pPacket, CGameServer * app);
	void						SendItemUpgradeReq(CLkPacket * pPacket, CGameServer *app);
	void						SendBudokaiSocialAction(CLkPacket * pPacket, CGameServer *app);
	//MISC
	void						SendRollDice(CLkPacket * pPacket, CGameServer * app);
	void						SendFogOfWarRes(CLkPacket * pPacket, CGameServer * app);
	void						SendRideOnBusRes(CLkPacket * pPacket, CGameServer * app);
	void						SendRideOffBusRes(CLkPacket * pPacket, CGameServer * app);
	void						SendBusLocation(CLkPacket * pPacket, CGameServer * app);
	void						SendNetPyStart(CLkPacket * pPacket, CGameServer * app);
	
	// SCOUTER
	void						SendScouterIndicatorReq(CLkPacket * pPacket, CGameServer * app);
	// DRAGON BALL
	void						SendDragonBallCheckReq(CLkPacket * pPacket, CGameServer * app);
	void						SendDragonBallRewardReq(CLkPacket * pPacket, CGameServer * app);
	// SKILL
	void						SendCharSkillAction(CLkPacket * pPacket, CGameServer * app, sSKILL_TBLDAT* skill, int RpSelectedType);
	void						SendCharSkillRes(CLkPacket * pPacket, CGameServer * app);
	void						SendCharSkillCasting(CLkPacket * pPacket, CGameServer * app, sSKILL_TBLDAT* skill, int RpSelectedType);
	void						SendCharSkillTransformCancel(CLkPacket * pPacket, CGameServer * app);
	void						SendSocialSkillRes(CLkPacket *pPacket, CGameServer * app);
	void						SendRpCharge(CLkPacket *pPacket, CGameServer * app);
	void						SendCharSkillBuffDrop(CLkPacket *pPacket, CGameServer * app);

	//HTB
	void						SendCharSkillHTBLearn(CLkPacket * pPacket, CGameServer * app);
	void						SendHTBStartReq(CLkPacket * pPacket, CGameServer * app);
	void						SendHTBFoward(CLkPacket * pPacket, CGameServer * app);
	void						SendHTBRpBall(CLkPacket * pPacket, CGameServer * app);
	void						SendCharUpdateHTBState(int SkillID, CGameServer * app);
	void						SendHTBSendbagState(CGameServer * app);

	// QuickSlots
	void						SendCharUpdQuickSlot(CLkPacket * pPacket, CGameServer * app);
 	void						SendCharDelQuickSlot(CLkPacket * pPacket, CGameServer * app);
	// Vehicle Things
	void						SendEngineStartRes(CLkPacket * pPacket, CGameServer * app);
	void						SendEngineStopRes(CLkPacket * pPacket, CGameServer * app);
	void						SendVehicleDPCancelRes(CLkPacket * pPacket, CGameServer * app);	
	void						SendVehicleStuntNFY(CLkPacket * pPacket, CGameServer * app);
	void						SendVehicleEndRes(CLkPacket * pPacket, CGameServer * app);
	// MUDOSA
	void						SendGambleBuyReq(CLkPacket * pPacket, CGameServer * app);
	// BANK
	void						SendBankStartReq(CLkPacket * pPacket, CGameServer * app);
	void						SendBankEndReq(CLkPacket * pPacket, CGameServer * app);
	void						SendBankBuyReq(CLkPacket * pPacket, CGameServer * app);
	void						SendBankLoadReq(CLkPacket * pPacket, CGameServer * app);
	void						SendBankMoneyReq(CLkPacket * pPacket, CGameServer * app);
	void						SendBankMoveReq(CLkPacket * pPacket, CGameServer * app);
	void						SendBankStackReq(CLkPacket * pPacket, CGameServer * app);
	void						SendBankDeleteReq(CLkPacket * pPacket, CGameServer * app);
	// TRADE
	void						SendTradeStartRes(CLkPacket * pPacket, CGameServer * app);
	void						SendTradeOkRes(CLkPacket * pPacket, CGameServer * app);
	void						SendTradeDenyRes(CLkPacket * pPacket, CGameServer * app);
	void						SendTradeFinish(CLkPacket * pPacket, CGameServer * app);
	void						SendTradeCancelRes(CLkPacket * pPacket, CGameServer * app);
	void						SendTradeAddRes(CLkPacket * pPacket, CGameServer * app);
	void						SendTradeDelRes(CLkPacket * pPacket, CGameServer * app);
	void						SendTradeItemModify(CLkPacket * pPacket, CGameServer * app);
	void						SendTradeZennyModify(CLkPacket * pPacket, CGameServer * app);
	// LOOT
	void						SendMobLoot(CLkPacket * pPacket, CGameServer * app, RwUInt32 m_uiTargetSerialId);
	void						SendZennyPickUpReq(CLkPacket * pPacket, CGameServer * app);
	void						SendItemPickUpReq(CLkPacket * pPacket, CGameServer * app);
	// LEVEL UP
	void						SendPlayerLevelUpCheck(CGameServer * app, int exp);
	// Wafog
	void						SendAvatarWarFogInfo(CLkPacket * pPacket, CGameServer * app);
	// QUEST
	void						SendPlayerQuestReq(CLkPacket * pPacket, CGameServer * app);
	void						SendObjectVisitQuest(CLkPacket * pPacket, CGameServer * app);
	void						SendTSUpdateState(CLkPacket * pPacket, CGameServer * app);
	//HoiPoi
	void						SendHoiPoiJob(CLkPacket * pPacket, CGameServer * app);
	void						SendHoiPoiJobReset(CLkPacket * pPacket, CGameServer * app);
	//Tutorial
	void						SendTutorialPlayQuit(CLkPacket * pPacket, CGameServer * app);
	void						SendDirectPlay(CLkPacket * pPacket, CGameServer * app);
	//Party Dungeon
	void						SendInitPartyDungeon(CLkPacket * pPacket, CGameServer * app);
	//TIMEQUEST
	void						SendTimeQuestList(CLkPacket * pPacket, CGameServer * app);
	void						LeaveTimeQuestRoom(CLkPacket * pPacket, CGameServer * app);
	void						JoinTimeQuestRoom(CLkPacket * pPacket, CGameServer * app);
	void						SendTimeQuestTeleport(CLkPacket * pPacket, CGameServer * app);
	//Private Shop
	void						SendPrivateShopCreate(CLkPacket * pPacket, CGameServer * app);
	void						SendPrivateShopExit(CLkPacket * pPacket, CGameServer * app);
	// DUEL
	void						SendFreeBattleReq(CLkPacket * pPacket, CGameServer * app);
	void						SendFreeBattleAccpetReq(CLkPacket * pPacket, CGameServer * app);
	// Admin Functions
	void						SendServerAnnouncement(wstring sMsg, CGameServer * app);
	void						SendServerAnnouncement(int type, wstring sMsg);
	void						SendServerBroadcast(wstring wsMsg, CGameServer * app);
	void						CreateItemById(uint32_t tblidx, int playerId);
	void						CreateItemById(uint32_t tblidx);
	void						AddSkillById(uint32_t tblidx, int playerId);
	void						AddSkillById(uint32_t tblidx);
	void						CreateMonsterById(unsigned int uiMobId, int PlayerId);
	void						CreateMonsterById(unsigned int uiMobId, unsigned int distance);
	void						SendTestDirectPlay(uint32_t tblidx, int playerId, bool sync);
	void						SendTestDirectPlay(uint32_t tblidx, bool sync, int playType);
	void						UpdateStats(wstring wszStat, int iAmount);

	//Game Server functions
	sGU_OBJECT_CREATE			characterspawnInfo;
	//For the shenron
	RwUInt32					myShenronHandle;
	//Other Classes
	PlayersMain					*cPlayersMain;
	GsFunctionsClass			*gsf;
	TLQHandler					*tlqManager = NULL;
private:
	CLkPacketEncoder_RandKey	m_packetEncoder;
	RwUInt32					avatarHandle;


public:
	int		IsMonsterIDInsideList(TBLIDX MonsterSpawnID)
	{
		MyMonsterList * mymonsterlist;
		if (my_monsterList.empty() == false)
		{
			MYMONSTERLISTIT it = my_monsterList.begin(), end = my_monsterList.end();

			for (it; it != end; it++)
			{
				mymonsterlist = (*it);
				if(mymonsterlist->MonsterID == MonsterSpawnID)
				{
					return mymonsterlist->MobTBLIDX;
				}
			}
		}
		return 0;
	}
	bool		IsMonsterInsideList(TBLIDX MonsterSpawnID)
	{
		MyMonsterList * mymonsterlist;
		if (my_monsterList.empty() == false)
		{
			MYMONSTERLISTIT it = my_monsterList.begin(), end = my_monsterList.end();
			for (it; it != end; ++it)
			{
				mymonsterlist = (*it);
				if(mymonsterlist->MonsterID == MonsterSpawnID)
				{
					return true;
				}
			}
		}
		return false;
	}
	void		InsertIntoMyMonsterList(TBLIDX MonsterSpawnID, CLkVector Position, TBLIDX mobid)
	{
		MyMonsterList * myml = new MyMonsterList;

		myml->MonsterID = MonsterSpawnID;
		myml->Position = Position;
		myml->MobTBLIDX = mobid;

		my_monsterList.push_back(myml);
	}
	bool		RemoveFromMyMonsterList(TBLIDX MonsterSpawnID)
	{
		MyMonsterList * mymonsterlist;
		MYMONSTERLISTIT it = my_monsterList.begin(), end = my_monsterList.end();
		for(it; it != end; it++ )
		{
			mymonsterlist = (*it);
			if(mymonsterlist->MonsterID == MonsterSpawnID)
			{
				it = my_monsterList.erase(it);
				return true;
			}
		}
		return true;
	}
typedef struct _MyMonsterList
{
	TBLIDX			MonsterID;
	CLkVector		Position;
	TBLIDX			MobTBLIDX;

}MyMonsterList;
typedef std::list<MyMonsterList*> MYMONSTERLIST;
typedef MYMONSTERLIST::const_iterator &MYMONSTERLISTIT;
MYMONSTERLIST					my_monsterList;
	void		InsertIntoMyNpcList(TBLIDX NpcSpawnID, CLkVector Position)
	{
		MyNpcList * mynl = new MyNpcList;

		mynl->NpcID = NpcSpawnID;
		mynl->Position = Position;

		my_npcList.push_back(mynl);
	}
	bool		IsNpcInsideList(TBLIDX NpcSpawnID)
	{
		MyNpcList * mynpclist;
		MYNPCLISTIT it = my_npcList.begin(), end = my_npcList.end();
		for(it; it != end; it++ )
		{
			mynpclist = (*it);
			if(mynpclist->NpcID == NpcSpawnID)
			{
				return true;
			}
		}
		return false;
	}
	typedef struct _MyNpcList
	{
		TBLIDX			NpcID;
		CLkVector		Position;

	}MyNpcList;
	typedef std::list<MyNpcList*> MYNPCLIST;
	typedef MYNPCLIST::const_iterator &MYNPCLISTIT;
	MYNPCLIST					my_npcList;
	void		FillNewList(MYMONSTERLIST *mylist)
	{
		for( MYMONSTERLISTIT it = my_monsterList.begin(); it != my_monsterList.end(); it++ )
		{
			mylist->push_back(*it);
		}
	}
};
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//

class CGameSessionFactory : public CLkSessionFactory
{
public:

	CLkSession * CreateSession(SESSIONTYPE sessionType)
	{
		CLkSession * pSession = NULL;
		switch( sessionType )
		{
		case SESSION_CLIENT: 
			{
				pSession = new CClientSession;
			}
			break;

		default:
			break;
		}

		return pSession;
	}
};

//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//

class CGameServer : public CLkServerApp
{

public:
	//For Drops
	struct ITEMDROPEDFROMMOB
	{
		int hItemSerial;
		TBLIDX itemTblidx;
		BYTE   byGrade;
		BYTE   byRank;
		BYTE   byBattleAttribute;
		int    StackCount;
	};
	int					OnInitApp()
	{
		m_nMaxSessionCount = MAX_NUMOF_SESSION;
		m_pSessionFactory = new CGameSessionFactory;
		if( NULL == m_pSessionFactory )
		{
			return LK_ERR_SYS_MEMORY_ALLOC_FAIL;
		}
		return LK_SUCCESS;
	}
	const char*		GetConfigFileHost()
	{
		return m_config.Host.c_str();
	}
	const char*		GetConfigFileUser()
	{
		return m_config.User.c_str();
	}
	const char*		GetConfigFilePassword()
	{
		return m_config.Password.c_str();
	}
	const char*		GetConfigFileDatabase()
	{
		return m_config.Database.c_str();
	}
	//For Multiple Server - Luiz45
	const string GetConfigFileEnabledMultipleServers()
	{
		return EnableMultipleServers.GetString();
	}
	const DWORD GetConfigFileMaxServers()
	{
		return MAX_NUMOF_SERVER;
	}
	const char*		GetConfigFileExternalIP()
	{
		return m_config.ExternalIP.c_str();
	}
	int					OnCreate()
	{
		int rc = LK_SUCCESS;	
		if (GetConfigFileEnabledMultipleServers() == "true")
		{
			for (int i = 0; i < GetConfigFileMaxServers(); i++)
			{
				int ports = atoi(ServersConfig[i][1].c_str());
				//Just Test our Binding Addres to avoid any errors or crashs - Luiz45
				CLkSockAddr addr; // The address structure for a CNTL socket
				addr.SetSockAddr(ServersConfig[i][0].c_str(), htons(ports));

				CLkSocket* testSocket = new CLkSocket();
				testSocket->Create(CLkSocket::eSOCKET_TCP);
				
				int rc = testSocket->Bind(addr);
				if (0 != rc)
				{
					continue;
				}
				else
				{
					//THen destroy that socket to allow us use the old socket - Luiz45
					testSocket->~CLkSocket();
				}
				// End of Test
				rc = m_clientAcceptor.Create(ServersConfig[i][0].c_str(), ports, SESSION_CLIENT, MAX_NUMOF_GAME_CLIENT, 5, 2, MAX_NUMOF_GAME_CLIENT);
				if (LK_SUCCESS != rc)
				{
					continue;
					return rc;
				}
				rc = m_network.Associate(&m_clientAcceptor, true);
				if (LK_SUCCESS != rc)
				{
					continue;
					return rc;
				}
				break;
			}			
		}
		else
		{
			rc = m_clientAcceptor.Create(m_config.strClientAcceptAddr.c_str(), m_config.wClientAcceptPort, SESSION_CLIENT, MAX_NUMOF_GAME_CLIENT, 5, 2, MAX_NUMOF_GAME_CLIENT);
			if (LK_SUCCESS != rc)
			{
				return rc;
			}
			rc = m_network.Associate(&m_clientAcceptor, true);
			if (LK_SUCCESS != rc)
			{
				return rc;
			}
		}
		
		return LK_SUCCESS;
	}
	void				OnDestroy()
	{
	}
	int					OnCommandArgument(int argc, _TCHAR* argv[])
	{
		return LK_SUCCESS;
	}
	int					OnConfiguration(const char * lpszConfigFile)
	{
		CLkIniFile file;
		int rc = file.Create( lpszConfigFile );
		if( LK_SUCCESS != rc )
		{
			return rc;
		}
		//For multiple servers - Luiz45
		if (!file.Read("ServerOptions", "EnableMultipleServers", EnableMultipleServers))
		{
			return LK_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("ServerOptions", "MaxServerAllowed", MAX_NUMOF_SERVER))
		{
			return LK_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (EnableMultipleServers.GetString() == "true")
		{
			for (int i = 0; i < MAX_NUMOF_SERVER; i++)
			{
				string strNm = "Game Server" + std::to_string(i);
				if (!file.Read(strNm.c_str(), "Address", ServersConfig[i][0]))
				{
					return LK_ERR_SYS_CONFIG_FILE_READ_FAIL;
				}
				if (!file.Read(strNm.c_str(), "Port", ServersConfig[i][1]))
				{
					return LK_ERR_SYS_CONFIG_FILE_READ_FAIL;
				}
			}
		}
		if( !file.Read("IPAddress", "Address", m_config.ExternalIP) )
		{
			return LK_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if( !file.Read("Game Server", "Address", m_config.strClientAcceptAddr) )
		{
			return LK_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if( !file.Read("Game Server", "Port",  m_config.wClientAcceptPort) )
		{
			return LK_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if( !file.Read("DATABASE", "Host",  m_config.Host) )
		{
			return LK_ERR_DBC_HANDLE_ALREADY_ALLOCATED;
		}
		if( !file.Read("DATABASE", "User",  m_config.User) )
		{
			return LK_ERR_SYS_MEMORY_ALLOC_FAIL;
		}
		if( !file.Read("DATABASE", "Password",  m_config.Password) )
		{
			return LK_ERR_SYS_LOG_SYSTEM_INITIALIZE_FAIL;
		}
		if( !file.Read("DATABASE", "Db",  m_config.Database) )
		{
			return LK_ERR_DBC_CONNECTION_CONNECT_FAIL;
		}
		/*if( !file.Read("PERFORMANCE", "PlayerLimit",  m_config.PlayerLimit) )
		{
			return LK_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if( !file.Read("PERFORMANCE", "PlayerSaveInterval",  m_config.PlayerSaveInterval) )
		{
			return LK_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if( !file.Read("PERFORMANCE", "PlayerSaveStatsSaveOnlyOnLogout",  m_config.PlayerSaveStatsSaveOnlyOnLogout) )
		{
			return LK_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}*/
		return LK_SUCCESS;
	}
	int					OnAppStart()
	{
		if(CreateTableContainer(1))
		{
			g_pPlayerManager = new CPlayerManager;
			g_pPlayerManager->Init();
			LK_PRINT(PRINT_APP, "Player Manager Initalzed\n\r");
			g_pMobManager = new CMobManager;
			LK_PRINT(PRINT_APP, "Mob Manager Initalzed\n\r");
			g_pMobManager->Init();
			g_pObjectManager = new  CObjectManager;
			LK_PRINT(PRINT_APP, "Object Manager Initalzed\n\r");
			g_pObjectManager->Init();
			return LK_SUCCESS;
		}else{
		printf("FAILED LOADING TABLES !!! \n");
		return LK_SUCCESS;
		}
	}

private:
	CLkAcceptor				m_clientAcceptor;
	CLkLog  					m_log;
	sSERVERCONFIG				m_config;
	DWORD						MAX_NUMOF_SERVER = 1;//This will be defined how many servers we can load
	CLkString					EnableMultipleServers;//Added for enabling multiple server - Luiz45
	DWORD						MAX_NUMOF_GAME_CLIENT = 3;
	DWORD						MAX_NUMOF_SESSION = MAX_NUMOF_GAME_CLIENT + MAX_NUMOF_SERVER;
public:
	MySQLConnWrapper			*db;
	MobActivity					*mob;
	QueryClass					*qry;
	//CLkSobManager*				m_objectManager;//Teste Code
	CLkString					ServersConfig[99][2];//For Config of multiple server 0->{Ip,Port} - Luiz45
public:
	void						UpdateClient(CLkPacket * pPacket, CClientSession *	pSession);
	CLkPacket *				pPacket;
	CClientSession *			pSession;
	CTableContainer	*			g_pTableContainer;
	double						dExpMultiplier = 1;//For Exp Event from command admin console
	double						dZennyMultiplier = 1;//For Zenny Event from command admin console
	bool						DBEvent = false;//For DBO Event from command admin console
	bool						DBScramble = false;//For Scramble event from command admin console
	bool						CreateTableContainer(int byLoadMethod);

	void						Run()
	{
		DWORD dwTickCur, dwTickOld = ::GetTickCount();

		while( IsRunnable() )
		{		
			dwTickCur = ::GetTickCount();
			if( dwTickCur - dwTickOld >= 1000 )
			{
				UpdateClient(pPacket,pSession);
				dwTickOld = dwTickCur;
			}
		Sleep(100);
		}
	}

	bool						AddUser(const char * lpszUserID, CClientSession * pSession)
	{
		if( false == m_userList.insert( USERVAL(CLkString(lpszUserID), pSession)).second )
		{
			return false;
		}
		return true;		
	}
	void						RemoveUser(const char * lpszUserID)
	{
		m_userList.erase( CLkString(lpszUserID) );
	}
	bool						FindUser(const char * lpszUserID)
	{
		USERIT it = m_userList.find( CLkString(lpszUserID) );
		if( it == m_userList.end() )
			return false;
		return true;
	}
	void						UserBroadcast(CLkPacket * pPacket)
	{
		for( USERIT it = m_userList.begin(); it != m_userList.end(); it++ )
		{
			try
			{
				it->second->PushPacket(pPacket);
			}
			catch (exception e)
			{
				printf("UserBroadcast Failed");
			}
		}
	}
	PlayersMain*						GetUserSessionByCharId(int charID)
	{
		for (USERIT it = m_userList.begin(); it != m_userList.end(); it++)
		{
			if (it->second->cPlayersMain->GetCharID() == charID)
			{
				return it->second->cPlayersMain;
			}
		}
	}
	PlayersMain*						GetUserSession(int handle)
	{
		for( USERIT it = m_userList.begin(); it != m_userList.end(); it++ )
		{
			if(it->second->cPlayersMain->GetAvatarHandle() == handle)
			{
				return it->second->cPlayersMain;				
			}
		}
	}
	void						UserBroadcastothers(CLkPacket * pPacket, CClientSession * pSession)
	{
		for( USERIT it = m_userList.begin(); it != m_userList.end(); it++ )
		{
			if(pSession->GetavatarHandle() != it->second->GetavatarHandle())
				try
				{
					it->second->PushPacket(pPacket);
				}
				catch (exception e)
				{
					printf("Invalid Packet Sent \n\r");
				}
		}
	}
	void						UserBroadcasFromOthers(eOPCODE_GU opcode, CClientSession * pSession)
	{
		if(opcode == GU_OBJECT_CREATE)
		{
			for( USERIT it = m_userList.begin(); it != m_userList.end(); it++ )
			{
				if(pSession->GetavatarHandle() != it->second->GetavatarHandle())
				{
					CLkPacket packet(sizeof(sGU_OBJECT_CREATE));
					sGU_OBJECT_CREATE * sPacket = (sGU_OBJECT_CREATE *)packet.GetPacketData();
					memcpy(sPacket, &it->second->characterspawnInfo, sizeof(sGU_OBJECT_CREATE));
					sPacket->handle = it->second->GetavatarHandle();

					packet.SetPacketLen( sizeof(sGU_OBJECT_CREATE) );
					pSession->PushPacket(&packet);
				}
			}
		}
	}

	void		InsertMonsterIntoAllMyMonsterLists(CLkPacket * pPacket, TBLIDX MonsterSpawnID, CLkVector Position, TBLIDX MonsterID)
	{
		for( USERIT it = m_userList.begin(); it != m_userList.end(); it++ )
		{
			it->second->InsertIntoMyMonsterList(MonsterSpawnID, Position, MonsterID);
		}
	}
	void		RemoveMonsterFromAllMyMonsterLists(TBLIDX MonsterSpawnID)
	{
		for( USERIT it = m_userList.begin(); it != m_userList.end(); it++ )
		{
			it->second->RemoveFromMyMonsterList(MonsterSpawnID);
		}
	}
	bool			AddNewZennyAmount(int handle, int amount)
	{
		if( false == amount_zenny.insert( ZENNYAMOUNTVAL(handle, amount)).second )
		{
			return false;
		}
		return true;		
	}
	void			RemoveZenny(const int handle)
	{
		amount_zenny.erase(handle);
	}
	int				FindZenny(const int handle)
	{
		int amount_ = 0;
		ZENNYAMOUNTIT it = amount_zenny.find(handle);
		if( it == amount_zenny.end() )
			return -1;
		else
			return amount_zenny[handle];
	}

bool	AddNewItemDrop(int handle, RwUInt32 tblidx,int byGrade,int byRank)
{	
	ITEMDROPEDFROMMOB itemTMP;
	itemTMP.hItemSerial = handle;
	itemTMP.byGrade = byGrade;
	itemTMP.byRank  = byRank;
	itemTMP.itemTblidx = tblidx;
	itemTMP.StackCount = 1;
	itemTMP.byBattleAttribute = 0;
	if (false == item_pickup.insert(ITEMPICKUPVAL(handle, itemTMP)).second)
	{
		return false;
	}
	return true;
}
void	RemoveItemPickup(const int handle)
{
	item_pickup.erase(handle);
}
ITEMDROPEDFROMMOB*		FindItemPickup(const int handle)
{
	int tblidx_ = 0;
	ITEMPICKUPIT it = item_pickup.find(handle);
	if (it == item_pickup.end())
		return NULL;
	else
		return &item_pickup[handle];
}


	typedef std::map<CLkString, CClientSession*> USERLIST;
	typedef USERLIST::value_type USERVAL;
	typedef USERLIST::iterator &USERIT;
	USERLIST					m_userList;

	typedef std::map<int, int> ZENNYAMOUNT;
	typedef ZENNYAMOUNT::value_type ZENNYAMOUNTVAL;
	typedef ZENNYAMOUNT::iterator ZENNYAMOUNTIT;
	ZENNYAMOUNT					amount_zenny;

	
	typedef std::map<int, ITEMDROPEDFROMMOB> ITEMPICKUP;
	typedef ITEMPICKUP::value_type ITEMPICKUPVAL;
	typedef ITEMPICKUP::iterator ITEMPICKUPIT;
	ITEMPICKUP					item_pickup;

	template<class T>
	bool Test(T const& test, std::initializer_list<T> const& values){
		return std::find(std::begin(values), std::end(values), test) != std::end(values);
	}

	void	packetPsychic(WORD wOpCode)
	{
		//Check to see if wOpCode is above heartbeat packets and does not read movement packets
		switch (wOpCode)
		{
			case 1: case 4009: case 4010: case 4011: case 4012: case 4013: case 4014: case 4015: case 4016: case 4017: case 4018: case 4111:
			{
				return;
			}
			default:
				LK_PRINT(PRINT_SYSTEM, "Packet Recieved %s [%u]\n\r", LkGetPacketName_UG(wOpCode), wOpCode);
		}
	}
};

