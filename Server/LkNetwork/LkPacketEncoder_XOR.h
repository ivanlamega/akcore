//***********************************************************************************
//
//	File		:	LkPacketEncoder_XOR.h
//
//	Begin		:	2007-01-29
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	YOSHIKI
//
//	Desc		:	
//
//***********************************************************************************


#pragma once

#include "LkPacketEncoder.h"

class CLkPacketEncoder_XOR : public CLkPacketEncoder
{
public:

	CLkPacketEncoder_XOR(void);

	~CLkPacketEncoder_XOR(void);


public:

	int						RxDecrypt(CLkPacket& rPacket);

	int						TxEncrypt(CLkPacket& rPacket);
};
