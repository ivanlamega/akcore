//***********************************************************************************
//
//	File		:	LkError.cpp
//
//	Begin		:	2005-11-30
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	Error ó�� ����
//
//***********************************************************************************

#include "stdafx.h"
#include "LkError.h"

#ifdef LK_DEFINE_ERROR
	#undef LK_DEFINE_ERROR
	#define LK_DEFINE_ERROR(x)		#x,
#endif

char * ntl_error_string[ MAX_LK_ERROR - LK_ERR_BEGIN ] = 
{
	"LK_ERR_BEGIN",

	#include "LkErrorCodes.h"
};

const char * GetLkErrorString(int nErrorCode )
{
	if( nErrorCode >= MAX_LK_ERROR )
		return "Lk Error message not found";

	return ntl_error_string[ nErrorCode - LK_ERR_BEGIN ];
}



const unsigned int	MAX_ERR_STR_BUFF = 256;


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void LkGetErrorString( CLkString & rStrError, int iErrorCode )
{
	if( iErrorCode >= LK_ERR_BEGIN )
	{
		rStrError = GetLkErrorString( iErrorCode );
	}
	else
	{
		char szBuf[MAX_ERR_STR_BUFF] = { 0x00, };

		FormatMessage(	FORMAT_MESSAGE_FROM_SYSTEM,
						0,
						iErrorCode,
						MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
						szBuf,
						MAX_ERR_STR_BUFF,
						NULL);

		rStrError = szBuf;
	}
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
const char * LkGetErrorMessage( int iErrorCode )
{
	if( iErrorCode >= LK_ERR_BEGIN )
	{
		return const_cast<char*>( GetLkErrorString( iErrorCode ) );
	}
	else
	{
		static char szBuf[MAX_ERR_STR_BUFF] = { 0x00, };

		FormatMessage(	FORMAT_MESSAGE_FROM_SYSTEM,
						0,
						iErrorCode,
						MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
						szBuf,
						MAX_ERR_STR_BUFF,
						NULL);

		return szBuf;
	}
}

