//***********************************************************************************
//
//	File		:	LkSocket.cpp
//
//	Begin		:	2005-12-13
//
//	Copyright	:	ⓒ NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	Socket Class
//
//***********************************************************************************

#include "stdafx.h"
#include "LkSocket.h"

#include <mstcpip.h>


//-----------------------------------------------------------------------------------
// static variable
//-----------------------------------------------------------------------------------
LPFN_ACCEPTEX				CLkSocket::m_lpfnAcceptEx				= NULL;
LPFN_CONNECTEX				CLkSocket::m_lpfnConnectEx				= NULL;
LPFN_DISCONNECTEX			CLkSocket::m_lpfnDisconnectEx			= NULL;
LPFN_GETACCEPTEXSOCKADDRS	CLkSocket::m_lpfnGetAcceptExSockAddrs	= NULL;
LPFN_TRANSMITFILE			CLkSocket::m_lpfnTransmitFile			= NULL;
//-----------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkSocket::CLkSocket()
{
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkSocket::~CLkSocket()
{
	Close();
}

//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::StartUp()
{
	WSADATA wsaData;

	if ( 0 != WSAStartup( MAKEWORD( 2, 2 ), &wsaData ) )
	{
		// Winsock DLL 못 찾음
		return WSAGetLastError();
	}

	// Winsock이 2.2를 지원하는지 확인
	if ( LOBYTE( wsaData.wVersion ) != 2 ||	HIBYTE( wsaData.wVersion ) != 2 )
	{
		WSACleanup();
		return WSAGetLastError(); 
	}


	// Load MS Winsock Extension API 
	int nResult = LoadExtensionAPI();
	if( LK_SUCCESS != nResult )
	{
		WSACleanup();
		return nResult;
	}

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::CleanUp()
{
	if( 0 != WSACleanup() )
		return WSAGetLastError();

	return LK_SUCCESS;
}

//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::LoadExtensionAPI()
{
	int nResult = 0;

	// AcceptEx Load
	GUID funcAcceptEx = WSAID_ACCEPTEX;
	nResult = LoadExtensionFunction( funcAcceptEx , (LPVOID*) &m_lpfnAcceptEx );
	if( LK_SUCCESS != nResult )
		return nResult;

	// ConnectEx Load
	GUID funcConnectEx = WSAID_CONNECTEX;
	nResult = LoadExtensionFunction( funcConnectEx, (LPVOID*) &m_lpfnConnectEx );
	if( LK_SUCCESS != nResult )
		return nResult;

	// DisconnectEx
	GUID funcDisconnectEx = WSAID_DISCONNECTEX;
	nResult = LoadExtensionFunction( funcDisconnectEx, (LPVOID*) &m_lpfnDisconnectEx );
	if( LK_SUCCESS != nResult )
		return nResult;

	// GetAcceptExAddr
	GUID funcGetAcceptExAddr = WSAID_GETACCEPTEXSOCKADDRS;
	nResult = LoadExtensionFunction( funcGetAcceptExAddr, (LPVOID*) &m_lpfnGetAcceptExSockAddrs );
	if( LK_SUCCESS != nResult )
		return nResult;

	// TransmitFile
	GUID funcTransmitFile = WSAID_TRANSMITFILE;
	nResult = LoadExtensionFunction( funcTransmitFile, (LPVOID*) &m_lpfnTransmitFile );
	if( LK_SUCCESS != nResult )
		return nResult;

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::LoadExtensionFunction(GUID functionID, LPVOID *pFunc)
{
	CLkSocket socket;
	if( 0 != socket.Create(CLkSocket::eSOCKET_TCP) )
		return WSAGetLastError();

	DWORD dwBytes = 0;
	int nResult = WSAIoctl(	socket,
		SIO_GET_EXTENSION_FUNCTION_POINTER,
		&functionID,
		sizeof(GUID),
		pFunc,
		sizeof(LPVOID),
		&dwBytes,
		0,
		0);

	if( 0 != nResult )
		return WSAGetLastError();

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::Create(int nSocketType)
{
	UNREFERENCED_PARAMETER( nSocketType );

	m_socket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);

	if( INVALID_SOCKET == m_socket )
		return WSAGetLastError();

	return LK_SUCCESS;
}

//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::Bind(CLkSockAddr& rSockAddr)
{
	if( 0 != bind(m_socket, (struct sockaddr *) rSockAddr, sizeof(struct sockaddr) ) )
	{
		return WSAGetLastError();
	}

	return LK_SUCCESS;
}

//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::Listen(int nBackLog)
{
	if( 0 != listen(m_socket, nBackLog) )
	{
		return WSAGetLastError();
	}

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::Close()
{
	if( INVALID_SOCKET == m_socket )
	{
		return LK_SUCCESS;
	}

	if( SOCKET_ERROR == closesocket( m_socket ) )
	{
		return WSAGetLastError();
	}

	m_socket = INVALID_SOCKET;

	return LK_SUCCESS;
}

//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::Shutdown(int how)
{
	if( INVALID_SOCKET == m_socket )
	{
		return LK_SUCCESS;
	}

	if( SOCKET_ERROR == shutdown( m_socket, how ) )
	{
		return WSAGetLastError();
	}

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::GetPeerName(CLkString & rAddress, WORD & rPort)
{
	struct sockaddr_in sockAddr;
	int nSockAddrLen = sizeof(sockAddr);

	if( 0 != getpeername(m_socket, (struct sockaddr*) &sockAddr, &nSockAddrLen) )
	{
		return WSAGetLastError();
	}

	rAddress	= inet_ntoa( sockAddr.sin_addr );
	rPort		= ntohs( sockAddr.sin_port );

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::GetLocalName(CLkString & rAddress, WORD & rPort)
{
	struct sockaddr_in sockAddr;
	int nSockAddrLen = sizeof(sockAddr);

	if( 0 != getsockname(m_socket, (struct sockaddr*) &sockAddr, &nSockAddrLen) )
	{
		return WSAGetLastError();
	}

	rAddress	= inet_ntoa( sockAddr.sin_addr );
	rPort		= ntohs( sockAddr.sin_port );

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::GetPeerAddr(CLkSockAddr & rAddr)
{
	struct sockaddr_in sockAddr;
	int nSockAddrLen = sizeof(sockAddr);

	if( 0 != getpeername(m_socket, (struct sockaddr*) &sockAddr, &nSockAddrLen) )
	{
		return WSAGetLastError();
	}

	rAddr.SetSockAddr( inet_ntoa( sockAddr.sin_addr ), ntohs( sockAddr.sin_port ) ); 

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::GetLocalAddr(CLkSockAddr & rAddr)
{
	struct sockaddr_in sockAddr;
	int nSockAddrLen = sizeof(sockAddr);

	if( 0 != getsockname(m_socket, (struct sockaddr*) &sockAddr, &nSockAddrLen) )
	{
		return WSAGetLastError();
	}

	rAddr.SetSockAddr( inet_ntoa( sockAddr.sin_addr ), ntohs( sockAddr.sin_port ) ); 

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::SetNonBlocking(BOOL bActive)
{
	unsigned long i = bActive;
	int result = ioctlsocket( m_socket, FIONBIO, &i );

	if( SOCKET_ERROR == result )
		return WSAGetLastError();

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::SetReuseAddr(BOOL bActive)
{
	int result = setsockopt( m_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&bActive, sizeof(bActive) );

	if( SOCKET_ERROR == result )
		return WSAGetLastError();	

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::SetLinger(BOOL bActive, WORD wTime)
{
	struct linger so_linger;

	so_linger.l_onoff	= (u_short)bActive;
	so_linger.l_linger	= wTime;

	int result = setsockopt( m_socket, SOL_SOCKET, SO_LINGER, (char *)&so_linger, sizeof(so_linger) );

	if( SOCKET_ERROR == result )
		return WSAGetLastError();	

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::SetTCPNoDelay(BOOL bActive)
{
	int result = setsockopt( m_socket, IPPROTO_TCP, TCP_NODELAY, (char*)&bActive, sizeof(bActive) );

	if( SOCKET_ERROR == result )
		return WSAGetLastError();

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::SetKeepAlive(BOOL bActive)
{
	int result = setsockopt( m_socket, SOL_SOCKET, SO_KEEPALIVE, (char*)&bActive, sizeof(bActive) );

	if( SOCKET_ERROR == result )
		return WSAGetLastError();

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::SetKeepAlive(DWORD dwKeepAliveTime, DWORD dwKeepAliveInterval)
{
	tcp_keepalive keepAlive = { TRUE, dwKeepAliveTime, dwKeepAliveInterval };

	DWORD dwBytesReturned;
	int result = WSAIoctl( m_socket, SIO_KEEPALIVE_VALS, &keepAlive, sizeof(keepAlive), 0, 0, &dwBytesReturned, NULL, NULL );

	if( SOCKET_ERROR == result )
		return WSAGetLastError();

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
//  [1/5/2007 zeroera] : 설명 : Accept가 실제로 일어나지 않으면 클라이언트에 ACK + SYN가 가지 않게되어
// BackLog로 인한 선행 Connect가 일어나지 않는다.
// 일정 시간이내에 Accept가 호출되지 않으면 클라이언트는 TIMEOUT 된다
// performance decrease 가 있었음 : 현재는 윈도우 패치로 수정 됌
//-----------------------------------------------------------------------------------
int CLkSocket::SetConditionalAccept(BOOL bActive)
{
	int result = setsockopt( m_socket, SOL_SOCKET, SO_CONDITIONAL_ACCEPT, (char*)&bActive, sizeof(bActive) );

	if( SOCKET_ERROR == result )
		return WSAGetLastError();

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::GetCurReadSocketBuffer()
{
	unsigned long nRead = 0;

	ioctlsocket( m_socket, FIONREAD , &nRead );

	return nRead;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkSocket::Connect(struct sockaddr_in * sockaddr)
{
	int rc = connect(m_socket, (struct sockaddr *)sockaddr, sizeof(struct sockaddr_in));
	if( SOCKET_ERROR == rc )    
	{
		rc = WSAGetLastError();
		return rc;
	}

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		FuncName	:
//		Desc		: 
//		Parameter	:
//		Return		:
//-----------------------------------------------------------------------------------
int CLkSocket::SendStream(unsigned char *pSendBuffer, int nSendSize, bool bSendOut)
{
	int nResult = 0;
	bool bProcess = true;
	BYTE * pBuffer = pSendBuffer;

	if( bSendOut )
	{
		while( bProcess )
		{
			nResult = send( m_socket, (const char *) pBuffer, nSendSize, 0 );

			if( SOCKET_ERROR == nResult )
			{
				bProcess = false;
				return SOCKET_ERROR;
			}
			else if( nResult < nSendSize )
			{
				pBuffer += nResult;
				nSendSize -= nResult;

				bProcess = true;       			    		
			}
			else
			{
				bProcess = false;
			}
		}
	}
	else
	{
		nResult = send( m_socket, (const char *) pBuffer, nSendSize, 0 );

		if( SOCKET_ERROR == nResult )
		{
			return SOCKET_ERROR;
		}
	}

	return nResult;	
}


//-----------------------------------------------------------------------------------
//		FuncName	:
//		Desc		: 
//		Parameter	:
//		Return		:
//-----------------------------------------------------------------------------------
int CLkSocket::RecvStream(BYTE * pRecvBuffer, int nRecvSize)
{
	return recv( m_socket, (char *) pRecvBuffer, nRecvSize, 0 );
}