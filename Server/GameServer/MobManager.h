#pragma once

#include "LkThread.h"
#include <thread>


#include "MonsterClass.h" ///monster class CMonster

#ifndef MOB_MANAGER_H
#define MOB_MANAGER_H   
static RwUInt32 m_uiMobId = 300;


class CMobManager : public CLkRunObject
{

public:
	CMobManager();
	~CMobManager();

public:
	void					Init();
	void					Release();

	void					CreateMobThread();
	void					Run();
	void					DoWork();
	void					CreateMonsterList();
	void					CreateNpcList();
	void					SpawnNpcAtLogin(CLkPacket * pPacket, CClientSession * pSession);
	void					SpawnMonsterAtLogin(CLkPacket * pPacket, CClientSession * pSession);

	RwUInt32				CreateUniqueId(void);
	bool					RunSpawnCheck(CLkPacket * pPacket, sVECTOR3 curPos, CClientSession * pSession);
	bool					CreatureRangeCheck(sVECTOR3 mycurPos, CLkVector othercurPos);
	float					Distance(const sVECTOR3 mycurPos, const CLkVector othercurPos);
	bool					FindCreature(RwUInt32 handle);
	TBLIDX					FindNpc(RwUInt32 handle);
	CMonster::MonsterData*	GetMobByHandle(RwUInt32 m_uiTargetSerialId);
	bool					UpdateDeathStatus(RwUInt32 mobID, bool death_status);
	void					CreateMobByTblidx(RwUInt32 spawnTblidx, RwUInt32 avatarHandle);
	void					CreateMobByMobTblidx(RwUInt32 Tblidx, RwUInt32 avatarHandle);
	void					CreateNPCByTblidx(RwUInt32 spawnTblidx, RwUInt32 avatarHandle);



	std::map<DWORD, CMonster::MonsterData*> m_map_Monster;
	typedef std::map<DWORD, CMonster::MonsterData*>::const_iterator IterType;
	IterType i;

	std::map<DWORD, CMonster::MonsterData*> m_map_spawned_Monster;

	std::map<DWORD, CMonster::MonsterData*> m_map_Npc;
private:
	bool m_bRun;
	CLkThread * pThread;
	CLkThread * pThread2;
	CLkThread * pThread3;
	CLkThread * pThread4;
	CLkThread * pThread5;
	
};
extern CMobManager * g_pMobManager;

#endif //MOB_MANAGER_H