//***********************************************************************************
//
//	File		:	LkPacketEncoder.h
//
//	Begin		:	2007-01-27
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#pragma once


struct PACKETHEADER;
class CLkPacket;
class CLkPacketEncoder
{
public:

	CLkPacketEncoder(void) {}

	virtual ~CLkPacketEncoder(void) {}


public:

	virtual int				RxDecrypt(void * pvPacketHeader) { UNREFERENCED_PARAMETER( pvPacketHeader ); return LK_SUCCESS; }

	virtual int				TxEncrypt(void * pvPacketHeader) { UNREFERENCED_PARAMETER( pvPacketHeader ); return LK_SUCCESS; }

	virtual int				RxDecrypt(CLkPacket& rPacket);

	virtual int				TxEncrypt(CLkPacket& rPacket);

};
