#include "stdafx.h"
#include "Vector.h"
#include <list>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "GameServer.h"
#include "Attributes.h"
#include "Character.h"
#include "Inventory.h"
#include "SkillsQS.h"

#ifndef PLAYERS_MAIN_H
#define PLAYERS_MAIN_H   

class PlayersMain{
	///-------------Constructor & Destructor-------------///
	public:
		PlayersMain(HSESSION PlayerSession, int CharID, int AccountID);
		~PlayersMain();
		///-------------------------------------///
	private:
		//Members
    	int byChainAttack;
		int AccountID;
		int RPCounter;
		int RpBallFull;
		int iWorldID;
		int CharIDForDuel;
		char acWarFogFlag[LK_MAX_SIZE_WAR_FOG];
		bool isDead;
		bool isSitted;
		bool isfighting;
		bool isInTutorial;
		bool bTradeOk;
		bool bPlayerInTrade;
		bool bDuelStatus;
		bool bSkillInUse;
		bool bStartSkillCheck;
		DWORD lastFightTime;
		DWORD ChargingID;
		DWORD PlayerThreadID;
		DWORD loginTime;
		DWORD lastRegenTime;
		DWORD lastRPUpdate;
		DWORD BuffTimer;
		HANDLE ChargingThread;
		HANDLE PlayerThread;
		TBLIDX WorldTblx;
		TBLIDX tblEquipedChips[8];
		RwUInt32 avatarHandle;
		RwUInt32 mob_SpawnTime;
		RwUInt32 attackTarget;
    	HSESSION PlayerSession;
		sPC_PROFILE* sPlayerProfile;
		sPC_TBLDAT* sPcData;
		sCHARSTATE* sPlayerState;
		sVECTOR3 vCurLoc;
		sVECTOR3 vLastLoc;
		sVECTOR3 vCurDir;
		sVECTOR3 vLastDir;
		string sPlayerName;
		string sGuildName;
		void FillProfileWithInfo();
		void FillProfileWithAttribute();	
		void FillCharState();
	public:
		PlayersInventory* cPlayerInventory;
		PlayersSkills* cPlayerSkills;
		PlayerAttributes* cPlayerAttribute;
		PlayersMain* GetRefreshPointer(){ return this; };
		CClientSession* myCCSession;
		void CreatePlayerProfile();
		void SavePlayerData(CGameServer* app);
		void SetChainAttack(int number);
		int GetCharID();
		int GetCharIdForDuel();
		int GetAccountID();
		bool GetPlayerDead();
		bool GetPlayerSitGetUp();
		bool GetPlayerFight();		
		bool GetTradeOK();
		bool GetPlayerIsInTrade();
		bool GetDuelStatus();
		bool GetSkillInUse();
		bool CanStartBuffCheck();
		bool HaveAnySpaceInScouter();
		bool IsInTutorial();		
		bool GetIsCharging();
		int GetRpBallCounter();
		int GetRpBallFull();
		int GetWorldID();
		int ChainNumber(){ return byChainAttack; };
		char* GetWarFog();
		DWORD GetChargingID();
		DWORD GetPlayerThreadID();
		DWORD GetLastFightTime();
		DWORD GetLoginTime();
		DWORD GetLastRegen();
		DWORD GetLastRpUpdate();
		DWORD GetBuffTimer();
		HANDLE GetChargingThread();
		HANDLE GetPlayerThread();
		TBLIDX GetWorldTblx();
		TBLIDX* GetEquipedChips();
		RwUInt32 GetAttackTarget();
		RwUInt32 GetAvatarHandle();
		RwUInt32 GetMob_SpawnTime();
		RwUInt8 GetChainAttack();
		HSESSION GetSession();
		sPC_PROFILE* GetPcProfile();
		sCHARSTATE* GetCharState();
		sPC_TBLDAT* GetPcTblDat();
		sVECTOR3 GetPlayerPosition();
		sVECTOR3 GetPlayerLastPosition();
		sVECTOR3 GetPlayerDirection();
		sVECTOR3 GetPlayerLastDirection();
		string GetPlayerName();
		string GetGuildName();				
		void SetAvatarHandle(RwUInt32 AvatarHandle);
		void SetCharIDForDuel(int CharID);
		void SetPlayerName(string sPlayerName);		
		void SetPlayerPosition(sVECTOR3 sPlayerPosition);
		void SetPlayerLastPosition(sVECTOR3 sPlayerPosition);
		void SetPlayerDirection(sVECTOR3 sPlayerDirection);
		void SetPlayerLastDirection(sVECTOR3 sPlayerDirection);
		void SetGuildName(string sGuildName);		
		void SendRpBallInformation();
		void SendRpBallUpdate(int moreOrLess);
		void SetWorldTblidx(TBLIDX tblWorld);
		void SetWorldID(int iWorldID);
		void SetRPBall();
		void SetRpBallFull(int moreOrLess);
		void SetMob_SpawnTime(RwUInt32 id);
		void SetChargingID(DWORD id);
		void SetPlayerThreadID(DWORD id);
		void SetLastTimeFight(DWORD value);
		void SetLoginTime(DWORD value);
		void SetLastRegenTime(DWORD value);
		void SetLastRpUpdateTime(DWORD value);
		void SetBuffTimer(DWORD value);
		void SetChargindThread(HANDLE thread);
		void SetPlayerThread(HANDLE thread);
		void SetAttackTarget(RwUInt32 mobHandle);
		void SetPlayerWarFog(RwInt32 iFlagIndex);
		void SetLevelUP();
		void SetPlayerSit(bool SitGetUp);
		void SetPlayerDead(bool isDead);
		void SetPlayerFight(bool isFighting);
		void SetTradeOK(bool bTradeOK);
		void SetPlayerIsInTrade(bool bIsInTrade);
		void SetPlayerIsInTutorial(bool bReally);
		void SetDuelStatus(bool bStatus);
		void SetSkillInUse(bool inUse);
		void SetSkillCheck(bool bTrueFalse);
		void SetPlayerDamage(int value);
		void SetStats(sPC_TBLDAT *pTblData);
		void UpdateBaseAttributeWithEquip(TBLIDX itemTBL, BYTE byRank, BYTE byGrade, bool bRemove = false);
		
		//Player Thread Functions
		void SendThreadUpdateEPLP();
		void SendThreadUpdateOnlyEP();
		void SendThreadUpdateOnlyLP();
		void SendThreadUpdateRP();
		void SendThreadUpdateDeathStatus();
		void SendThreadRevivalStatus();
};

#endif