#include "Vector.h"
#include <list>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "GameServer.h"
#ifndef GAMELOGIC_LOGICS_H
#define GAMELOGIC_LOGICS_H   

class GameLogics{
	///-------------Constructor & Destructor-------------///
public:
	GameLogics();
	~GameLogics();
	///-------------------------------------///
private:
	//Members and Private Functions
public:
	//Methods and Members
	bool Logic_ItemExist(TBLIDX itemTblidx, RwUInt32 charID);
	eITEM_TYPE Logic_GetItemClass(TBLIDX itemTblidx);
	bool Logic_GetBagAvailable(PlayersInventory inventory);
	RwInt32 Logic_FindFirstEmptySlot(PlayersInventory inventory);

};

#endif