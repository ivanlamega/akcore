//***********************************************************************************
//
//	File		:	LkSessionFactory.h
//
//	Begin		:	2005-12-14
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	Base Session Factory Abstract Class
//
//***********************************************************************************

#pragma once

class CLkSession;

class CLkSessionFactory
{
public:

	virtual CLkSession *		CreateSession(SESSIONTYPE sessionType) = 0;
};

