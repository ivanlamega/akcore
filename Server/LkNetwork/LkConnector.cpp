//***********************************************************************************
//
//	File		:	LkConnector.cpp
//
//	Begin		:	2005-12-14
//
//	Copyright	:	ⓒ NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	Connector Class
//
//***********************************************************************************

#include "stdafx.h"
#include "LkConnector.h"

#include "LkNetwork.h"
#include "LkSession.h"
#include "LkSessionFactory.h"

#include "LkDebug.h"
#include "LkError.h"
#include "LkThread.h"

#include "Utils.h"


//---------------------------------------------------------------------------------------
// Network Monitor Thread class ( Network 클래스 내부용 )
//---------------------------------------------------------------------------------------
class CConnectorThread : public CLkRunObject
{
public:

	CConnectorThread(CLkConnector * pConnector) { SetArg( pConnector ); }

	void Run()
	{
		CLkConnector * pConnector = (CLkConnector*) GetArg();

		while( IsRunnable() ) 
		{
			if( TRUE == InterlockedCompareExchange( (LONG*)& pConnector->m_bConnected, TRUE, TRUE ) )
			{
				Wait( pConnector->m_dwProcessTime );
			}
			else
			{
				LK_PRINT(PRINT_SYSTEM, "%s Connector Try Connect", GetName());

				int rc = pConnector->DoConnect();
				if( LK_SUCCESS != rc )
				{
					LK_PRINT(PRINT_SYSTEM, "%s Connector Connect Fail :%d[%s]", GetName(),  rc, LkGetErrorMessage(rc));
					Wait( pConnector->m_dwRetryTime );
				}
				else
				{
					LK_PRINT(PRINT_SYSTEM, "%s Connector Connect Success", GetName() );
				}
			}

		} // end of while( IsRunnable() )

	}

};


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkConnector::CLkConnector()
{
	Init();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkConnector::~CLkConnector()
{
	Destroy();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkConnector::Init()
{
	m_pNetwork = NULL;

	m_pThread = NULL;


	m_dwRetryTime = 0;

	m_dwProcessTime = 0;

	m_dwTotalConnectCount = 0;


	m_bConnected = FALSE;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkConnector::Create(LPCTSTR lpszConnectAddr, WORD wConnectPort, SESSIONTYPE sessionType, DWORD dwRetryTime /* = 1000 */, DWORD dwProcessTime /* = 1000 */)
{
	m_connectAddr.SetSockAddr(lpszConnectAddr, htons(wConnectPort));

	m_sessionType = sessionType;

	m_dwProcessTime = dwProcessTime;

	m_dwRetryTime = dwRetryTime;


	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkConnector::Destroy()
{
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkConnector::CreateThread()
{
	CLkString strName;
	strName.Format("ConnectorThread [%s:%u] Type[%u]", m_connectAddr.GetDottedAddr(), m_connectAddr.GetPort(), m_sessionType);

	m_pThread = new CConnectorThread( this );
	if ( NULL == m_pThread )
	{
		LK_LOG_ASSERT("\"new CConnectorThread( this )\" failed.");
		return LK_ERR_SYS_MEMORY_ALLOC_FAIL;
	}

	CLkThread * pThread = CLkThreadFactory::CreateThread( m_pThread, strName.c_str(), true );		
	if( NULL == pThread )
	{
		LK_LOG_ASSERT("CLkThreadFactory::CreateThread( m_pThread, strName, false ) failed.(NULL == pThread)");
		SAFE_DELETE( m_pThread );
		return LK_ERR_NET_THREAD_CREATE_FAIL;
	}


	pThread->Start();


	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkConnector::DoConnect()
{
	CLkSession * pSession = m_pNetwork->GetSessionFactory()->CreateSession( m_sessionType );
	if( NULL == pSession )
	{
		return LK_ERR_NET_CONNECTION_POOL_ALLOC_FAIL;
	}


	int rc = pSession->Create( m_pNetwork );
	if( LK_SUCCESS != rc )
	{
		RELEASE_SESSION( pSession );
		return rc;
	}


	rc = pSession->PostConnect( this );
	if( LK_SUCCESS != rc )
	{
		RELEASE_SESSION( pSession );
		return rc;
	}


#if !defined( __USE_CONNECTEX__ )
	rc = pSession->CompleteConnect( m_sessionType );
	if( LK_SUCCESS != rc )
	{
		LK_PRINT( PRINT_SYSTEM, "Session[%X] CompleteConnect Error : Err:%d(%s)", pSession, rc, LkGetErrorMessage(rc) );
		pSession->Close();
		return rc;
	}
#endif


	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkConnector::OnAssociated(CLkNetwork * pNetwork)
{
	if( NULL == pNetwork )
	{
		LK_LOG_ASSERT("(NULL == pNetwork)");
		return LK_ERR_SYS_INPUT_PARAMETER_WRONG;
	}


	if( NULL != m_pNetwork )
	{
		LK_LOG_ASSERT("(NULL != m_pNetwork) m_pNetwork = %016x", m_pNetwork);
		return LK_ERR_SYS_OBJECT_ALREADY_CREATED;
	}


	m_pNetwork = pNetwork; 


	int rc = CreateThread();
	if( LK_SUCCESS != rc )
	{
		LK_LOG_ASSERT("CreateThread() failed.(LK_SUCCESS != rc) rc = %d", rc);
		return LK_ERR_NET_THREAD_CREATE_FAIL;
	}



	return LK_SUCCESS;
}
