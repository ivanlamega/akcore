//***********************************************************************************
//
//	File		:	LkNetBuffer.h
//
//	Begin		:	2006-08-25
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	
//
//	Desc		:	
//
//***********************************************************************************

#pragma once

#include "LkPacket.h"
#include "LkPacketEncoder.h"

#include "LkCircularQueue.h"
#include "LkError.h"
#include "LkQueue.h"
#include "LkDebug.h"


//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
const unsigned int	DEF_PACKET_MAX_COUNT = 100;
//-----------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
// 
//---------------------------------------------------------------------------------------
class CLkNetBuffer : public CLkCircularQueue<BYTE>
{
public:

	CLkNetBuffer()
		:m_nMaxPacketSize(PACKET_MAX_SIZE) {}

public:

	bool			Create(int nPacketCapacity = DEF_PACKET_MAX_COUNT, int nMaxPacketSize = PACKET_MAX_SIZE) 
	{ 
		if( false == CLkCircularQueue<BYTE>::Create( nMaxPacketSize *  nPacketCapacity, nMaxPacketSize ) )
		{
			return false;
		}
		m_nMaxPacketSize = nMaxPacketSize;

		return true;
	}

	bool			Resize(int nPacketCapacity = DEF_PACKET_MAX_COUNT, int nMaxPacketSize = PACKET_MAX_SIZE) 
	{
		if( false == CLkCircularQueue<BYTE>::Realloc( nMaxPacketSize *  nPacketCapacity, nMaxPacketSize ) )
		{
			return false;
		}
		m_nMaxPacketSize = nMaxPacketSize;

		return true;
	}

public:
	BYTE *			InGetQueueBufferPtr() { return GetQueueBufferPtr(); }

	BYTE *			InGetQueueExtraPtr() { return GetQueueExtraPtr(); }

	DWORD			GetMaxPacketCount() { return GetQueueSize() / m_nMaxPacketSize; }

	int				GetMaxPacketSize() { return m_nMaxPacketSize; }
 
private:

	int				m_nMaxPacketSize;
};







class CLkNetQueue : public CLkQueue<CLkPacket*>
{
public:

	CLkNetQueue():m_nMaxCount(0) {}

	virtual ~CLkNetQueue()
	{
		Destroy();
	}

	void Destroy()
	{
		CLkPacket * pPacket = PopPacket();
		while( pPacket )
		{
			SAFE_DELETE( pPacket );

			pPacket = PopPacket();
		}
	}

	bool PushPacket(CLkPacket* pPacket)
	{
		if( false == CLkQueue<CLkPacket*>::Push(pPacket) )
		{
			return false;
		}

		int nSize = GetSize();
		if( nSize > m_nMaxCount )
		{
			m_nMaxCount = nSize;
		}

		return true;
	}


	CLkPacket* PeekPacket()
	{
		return CLkQueue<CLkPacket*>::Peek();
	}

	CLkPacket* PopPacket()
	{
		return CLkQueue<CLkPacket*>::Pop();
	}


private:

	int				m_nMaxCount;

};

