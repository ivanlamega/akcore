//***********************************************************************************
//
//	File		:	LkNetwork.cpp
//
//	Begin		:	2005-12-15
//
//	Copyright	:	ⓒ NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	Network Manager Class
//
//***********************************************************************************

#include "stdafx.h"
#include "LkNetwork.h"

#include "LkSession.h"
#include "LkAcceptor.h"
#include "LkConnector.h"
#include "LkSessionFactory.h"
#include "LkNetworkProcessor.h"


#include "LkThread.h"
#include "LkDebug.h"
#include "LkError.h"
#include "LkQueue.h"

#include "Utils.h"


//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
const ULONG_PTR THREAD_CLOSE = (ULONG_PTR)(-1);	// thread terminate value
//---------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------
// Network Monitor Thread class ( Network 클래스 내부용 )
//---------------------------------------------------------------------------------------
class CLkNetworkMonitor : public CLkRunObject
{
public:

	CLkNetworkMonitor(CLkNetwork *  pNetwork) { SetArg( pNetwork); }

	void Run()
	{
		CLkNetwork * pNetwork = (CLkNetwork*) GetArg();
		if( NULL == pNetwork )
		{
			LK_LOG_ASSERT( "fail : NULL == pNetwork" );
			return;
		}

		DWORD dwTickCur = 0;
		DWORD dwTickOld = 0;
		DWORD dwTickDiff = 0;

		dwTickCur = dwTickOld = ::GetTickCount();

		while( IsRunnable() )
		{	
			Wait( 1000 );

			dwTickCur	= ::GetTickCount();
			dwTickDiff	= dwTickCur - dwTickOld;
			dwTickOld	= dwTickCur;

			if( NULL == pNetwork->GetSessionList() )
			{
				continue;
			}

			pNetwork->GetSessionList()->ValidCheck( dwTickDiff );
		}
	}

};
//-----------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkNetwork::CLkNetwork()
{
	Init();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkNetwork::~CLkNetwork()
{
	Destroy();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int	CLkNetwork::Create(CLkSessionFactory * pFactory, int nSessionSize, int nCreateThreads /* = 0 */, int nConcurrentThreads /* = 0  */)
{
	if( NULL == pFactory )
	{
		LK_LOG_ASSERT("(NULL == pFactory)");
		return LK_ERR_SYS_INPUT_PARAMETER_WRONG;
	}


	int rc = StartUp();
	if( LK_SUCCESS != rc )
	{
		LK_LOG_ASSERT("StartUp() failed.(LK_SUCCESS != rc) rc = %d", rc);
		return rc;
	}


	m_pSessionList = new CLkSessionList;
	if( NULL == m_pSessionList )
	{
		LK_LOG_ASSERT("\"new CLkSessionList\" failed.");
		return LK_ERR_SYS_MEMORY_ALLOC_FAIL;
	}


	m_pAcceptorList = new CLkAcceptorList;
	if( NULL == m_pAcceptorList )
	{
		LK_LOG_ASSERT("\"new CLkAcceptorList\" failed.");
		return LK_ERR_SYS_MEMORY_ALLOC_FAIL;
	}


	m_pConnectorList = new CLkConnectorList;
	if( NULL == m_pConnectorList )
	{
		LK_LOG_ASSERT("\"new CLkConnectorList\" failed.");
		return LK_ERR_SYS_MEMORY_ALLOC_FAIL;
	}


	rc = m_pSessionList->Create( this, nSessionSize, nSessionSize / 10 );
	if( LK_SUCCESS != rc )
	{
		LK_LOG_ASSERT("m_pSessionList->Create( this, nSessionSize, nSessionSize / 10 ) failed.(LK_SUCCESS != rc) rc = %d", rc);
		return rc;
	}


	rc = m_iocp.Create(this, nCreateThreads, nConcurrentThreads );
	if( LK_SUCCESS != rc )
	{
		LK_LOG_ASSERT("m_iocp.Create(this, nCreateThreads, nConcurrentThreads ) failed.(LK_SUCCESS != rc) rc = %d", rc);
		return rc;
	}


	rc = CreateMonitorThread();
	if( LK_SUCCESS != rc )
	{
		LK_LOG_ASSERT("CreateMonitorThread() failed.(LK_SUCCESS != rc) rc = %d", rc);
		return rc;
	}


	rc = CreateDispatcherThread();
	if( LK_SUCCESS != rc )
	{
		LK_LOG_ASSERT("CreateDispatcherThread() failed.(LK_SUCCESS != rc) rc = %d", rc);
		return rc;
	}


	m_pSessionFactoryRef = pFactory;


	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkNetwork::Destroy()
{
	SAFE_DELETE( m_pSessionList );

	if (NULL != m_pAcceptorList)
	{
		for( CLkAcceptorList::iterator it = m_pAcceptorList->begin(); it != m_pAcceptorList->end(); it++ )
		{
			SAFE_DELETE( it->second );
		}
		SAFE_DELETE( m_pAcceptorList );
	}

	if (NULL != m_pConnectorList)
	{
		for( CLkConnectorList::iterator it = m_pConnectorList->begin(); it != m_pConnectorList->end(); it++ )
		{
			SAFE_DELETE( it->second );
		}
		SAFE_DELETE( m_pConnectorList );
	}

	//SAFE_DELETE( m_pNetworkMonitor );

	//SAFE_DELETE( m_pNetworkProcessor );

	//Shutdown();
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
void CLkNetwork::Init()
{
	m_pSessionFactoryRef = NULL;

	m_pNetworkMonitor = NULL;

	m_pNetworkProcessor = NULL;

	m_pSessionList = NULL;

	m_pAcceptorList = NULL;

	m_pConnectorList = NULL;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkNetwork::StartUp()
{
	int rc = CLkSocket::StartUp();
	if( LK_SUCCESS != rc )
	{
		LK_LOG_ASSERT("CLkSocket::StartUp() failed.(LK_SUCCESS != rc) rc = %d", rc);
		return rc;
	}

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkNetwork::Shutdown()
{
	int rc = CLkSocket::CleanUp();
	if( LK_SUCCESS != rc )
	{
		LK_LOG_ASSERT("CLkSocket::CleanUp() failed.(LK_SUCCESS != rc) rc = %d", rc);
		return rc;
	}

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkNetwork::CreateMonitorThread()
{
	CLkString strName;
	strName.Format("Network Monitor");

	m_pNetworkMonitor = new CLkNetworkMonitor(this);
	if ( NULL == m_pNetworkMonitor )
	{
		LK_LOG_ASSERT("\"new CLkNetworkMonitor(this)\" failed.");
		return LK_ERR_SYS_MEMORY_ALLOC_FAIL;
	}

	CLkThread * pThread = CLkThreadFactory::CreateThread( m_pNetworkMonitor, strName.c_str(), true );		
	if( NULL == pThread )
	{
		LK_LOG_ASSERT("CLkThreadFactory::CreateThread( m_pNetworkMonitor, strName, false) failed.(NULL == pThread)");
		SAFE_DELETE( m_pNetworkMonitor );
		return LK_ERR_NET_THREAD_CREATE_FAIL;
	}

	pThread->Start();

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkNetwork::CreateDispatcherThread()
{
	CLkString strName;
	strName.Format("Network Processor");

	m_pNetworkProcessor = new CLkNetworkProcessor( this );
	if ( NULL == m_pNetworkProcessor )
	{
		LK_LOG_ASSERT("\"new CLkNetworkProcessor( this )\" failed.");
		return LK_ERR_SYS_MEMORY_ALLOC_FAIL;
	}

	int rc = m_pNetworkProcessor->Create();
	if(  LK_SUCCESS != rc )
	{
		LK_LOG_ASSERT("m_pNetworkProcessor->Create() failed.(LK_SUCCESS != rc) rc = %d", rc);
		return LK_ERR_NET_THREAD_CREATE_FAIL;
	}

	CLkThread * pThread = CLkThreadFactory::CreateThread( m_pNetworkProcessor, strName.c_str(), true );		
	if( NULL == pThread )
	{
		LK_LOG_ASSERT("CLkThreadFactory::CreateThread( m_pNetworkProcessor, strName, false ) failed.(NULL == pThread)");
		SAFE_DELETE( m_pNetworkProcessor );
		return LK_ERR_NET_THREAD_CREATE_FAIL;
	}

	pThread->Start();

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	: 
//		Return	:
//-----------------------------------------------------------------------------------
int CLkNetwork::Associate(CLkConnection * pConnection, bool bAssociate)
{
	CLkSession * pSession = check_cast<CLkSession*>( pConnection );
	if( NULL == pSession )
	{
		LK_LOG_ASSERT("(NULL == pSession) pConnection = %016x", pConnection);
		return LK_ERR_SYS_INPUT_PARAMETER_WRONG;
	}


	if( bAssociate )
	{
		if( false == m_pSessionList->Add( pSession ) )
		{
			LK_LOG_ASSERT("m_pSessionList->Add( pSession ) failed.");
			return  LK_ERR_NET_CONNECTION_ADD_FAIL;
		}

		int rc = m_iocp.Associate( pSession->GetSocket().GetRawSocket(), pSession );
		if( LK_SUCCESS != rc )
		{	
			LK_LOG_ASSERT("m_iocp.Associate( pSession->GetSocket().GetRawSocket(), pSession ) failed.(LK_SUCCESS != rc), rc = %d", rc);
			return rc;
		}
	}
	else
	{
		if( false == m_pSessionList->Remove( pSession ) )
		{
			LK_LOG_ASSERT("m_pSessionList->Remove( pSession ) failed.");
			return  LK_ERR_NET_CONNECTION_ADD_FAIL;
		}
	}


	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkNetwork::Associate(CLkAcceptor * pAcceptor, bool bAssociate)
{
	if( NULL == pAcceptor )
	{
		LK_LOG_ASSERT("(NULL == pAcceptor)");
		return LK_ERR_SYS_INPUT_PARAMETER_WRONG;
	}


	if( bAssociate )
	{
		CLkAcceptorList::iterator it = m_pAcceptorList->insert( CLkAcceptorList::value_type( pAcceptor->GetSessionType(), pAcceptor ) );
		if( m_pAcceptorList->end() == it )
		{
			LK_LOG_ASSERT("m_pAcceptorList->insert( CLkAcceptorList::value_type( pAcceptor->GetSessionType(), pAcceptor ) ) failed.(m_pAcceptorList->end() == it), pAcceptor->GetSessionType() = %u", pAcceptor->GetSessionType());
			return LK_ERR_NET_ACCEPTOR_ALREADY_CREATED;
		}


		int rc = pAcceptor->OnAssociated( this );
		if( LK_SUCCESS != rc )
		{
			LK_LOG_ASSERT("pAcceptor->OnAssociated( this ) failed.(LK_SUCCESS != rc), rc = %d", rc);
			return rc;
		}


		rc = m_iocp.Associate( pAcceptor->GetListenSocket().GetRawSocket(), pAcceptor );
		if( LK_SUCCESS != rc )
		{
			LK_LOG_ASSERT("m_iocp.Associate( pAcceptor->GetListenSocket().GetRawSocket(), pAcceptor ) failed.(LK_SUCCESS != rc), rc = %d", rc);
			return rc;
		}

	}


	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkNetwork::Associate(CLkConnector * pConnector, bool bAssociate)
{
	if( NULL == pConnector )
	{
		LK_LOG_ASSERT("(NULL == pConnector)");
		return LK_ERR_SYS_INPUT_PARAMETER_WRONG;
	}


	if( bAssociate )
	{
		CLkConnectorList::iterator it = m_pConnectorList->insert( CLkConnectorList::value_type(pConnector->GetSessionType(), pConnector) );
		if( m_pConnectorList->end() == it )
		{
			LK_LOG_ASSERT("m_pConnectorList->insert( CLkConnectorList::value_type(pConnector->GetSessionType(), pConnector) ) failed.(m_pConnectorList->end() == it), pConnector->GetSessionType() = %u", pConnector->GetSessionType());
			return LK_ERR_NET_CONNECTOR_ALREADY_CREATED;
		}


		int rc = pConnector->OnAssociated( this );
		if( LK_SUCCESS != rc )
		{
			LK_LOG_ASSERT("pConnector->OnAssociated() failed.(LK_SUCCESS != rc) rc = %d", rc);
			return rc;
		}
	}


	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkAcceptor * CLkNetwork::GetAcceptor(SESSIONTYPE sessionType, const char * lpszAddr, WORD wPort)
{
	CLkAcceptorList::iterator beg = m_pAcceptorList->lower_bound( sessionType );
	CLkAcceptorList::iterator end = m_pAcceptorList->upper_bound( sessionType );

	for( CLkAcceptorList::iterator it = beg; it != end; it++ )
	{
		CLkAcceptor * pAcceptor = it->second;
		if( NULL == pAcceptor )
		{
			LK_LOG_ASSERT("The acceptor object is NULL in m_pAcceptorList.(NULL == pAcceptor) sessionType = %u", sessionType);
			continue;
		}

		if( 0 != strncmp( pAcceptor->GetListenIP(), lpszAddr, strlen( lpszAddr ) ) )
			continue;

		if( wPort == pAcceptor->GetListenPort() )
			return pAcceptor;
	}

	return NULL;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
CLkConnector * CLkNetwork::GetConnector(SESSIONTYPE sessionType, const char * lpszAddr, WORD wPort)
{
	CLkConnectorList::iterator beg = m_pConnectorList->lower_bound( sessionType );
	CLkConnectorList::iterator end = m_pConnectorList->upper_bound( sessionType );

	for( CLkConnectorList::iterator it = beg; it != end; it++ )
	{
		CLkConnector * pConnector = it->second;
		if( NULL == pConnector )
		{
			LK_LOG_ASSERT("The connector object is NULL in m_pConnectorList.(NULL == pConnector) sessionType = %u", sessionType);
			continue;
		}

		if( 0 != strncmp( pConnector->GetConnectIP(), lpszAddr, strlen( lpszAddr ) ) )
			continue;

		if( wPort == pConnector->GetConnectPort() )
			return pConnector;
	}

	return NULL;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
//  [1/3/2007 zeroera] : 임시 : 이전 버전 호환용
//-----------------------------------------------------------------------------------
int CLkNetwork::Send(HSESSION hSession, CLkPacket * pPacket)
{
	if (NULL == pPacket)
	{
		LK_LOG_ASSERT("(NULL == pPacket)");
		return LK_ERR_NET_PACKET_INVALID;
	}

	if (NULL == m_pSessionList)
	{
		return LK_ERR_NET_NETWORK_NOT_CREATED;
	}

	CLkSession * pSession = m_pSessionList->Find( hSession );
	if( NULL == pSession )
	{
		return LK_ERR_NET_CONNECTION_NOT_EXIST;
	}

	int rc = pSession->PushPacket( pPacket );
	if( LK_SUCCESS != rc )
	{
		pSession->Disconnect( false );

		LK_LOGDL( LOG_NETWORK, "Session[%X] PushPacket Error : Err:%d(%s)", pSession, rc, LkGetErrorMessage(rc) );
		return rc;
	}

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkNetwork::Send(CLkSession * pSession, CLkPacket * pPacket)
{
	if (NULL == pSession)
	{
		return LK_ERR_NET_CONNECTION_NOT_EXIST;
	}

	if (NULL == pPacket)
	{
		LK_LOG_ASSERT("(NULL == pPacket)");
		return LK_ERR_NET_PACKET_INVALID;
	}

	int rc = pSession->PushPacket( pPacket );
	if( LK_SUCCESS != rc )
	{
		pSession->Disconnect( false );

		LK_LOGDL( LOG_NETWORK, "Session[%X] PushPacket Error : Err:%d(%s)", pSession, rc, LkGetErrorMessage(rc) );
		return rc;
	}

	return LK_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		Purpose	:
//		Return	:
//-----------------------------------------------------------------------------------
int CLkNetwork::PostNetEventMessage(WPARAM wParam, LPARAM lParam)
{
	return m_pNetworkProcessor->PostNetEvent( wParam, lParam );
}