#include "stdafx.h"
#include "LkNaviDataMng.h"
#include "UserDefinedAssert.h"


CLkNaviDataMng::CLkNaviDataMng( void )
{
	InitializeCriticalSection( &m_cs );

	m_pCreatedWorldInfo = NULL;
	m_pLoadedWorldInfo = NULL;
}

CLkNaviDataMng::~CLkNaviDataMng( void )
{
	Delete();

	DeleteCriticalSection( &m_cs );
}

bool CLkNaviDataMng::Create( const char* pPathName )
{
	Delete();

	CreateDirectory( pPathName, NULL );

	m_strPathName = pPathName;

	return true;
}

void CLkNaviDataMng::Delete( void )
{
	Delete_AllNaviInfo();
	Unload_AllNaviInfo();
}

CLkNaviWorldOutDoorInfo* CLkNaviDataMng::Create_WorldOutDoor( void )
{
	CAutoCS clCS( &m_cs );

	Assert( NULL == m_pCreatedWorldInfo );
	m_pCreatedWorldInfo = new CLkNaviWorldOutDoorInfo;
	return (CLkNaviWorldOutDoorInfo*)m_pCreatedWorldInfo;
}

CLkNaviWorldInDoorInfo* CLkNaviDataMng::Create_WorldInDoor( void )
{
	CAutoCS clCS( &m_cs );

	Assert( NULL == m_pCreatedWorldInfo );
	m_pCreatedWorldInfo = new CLkNaviWorldInDoorInfo;
	return (CLkNaviWorldInDoorInfo*)m_pCreatedWorldInfo;
}

CLkNaviGroupOutDoorInfo* CLkNaviDataMng::Create_GroupOutDoor( void )
{
	CAutoCS clCS( &m_cs );

	CLkNaviGroupOutDoorInfo* pInfoObj = new CLkNaviGroupOutDoorInfo;
	m_defCreatedObjList.push_back( pInfoObj );
	return pInfoObj;
}

CLkNaviGroupInDoorInfo* CLkNaviDataMng::Create_GroupInDoor( void )
{
	CAutoCS clCS( &m_cs );

	CLkNaviGroupInDoorInfo* pInfoObj = new CLkNaviGroupInDoorInfo;
	m_defCreatedObjList.push_back( pInfoObj );
	return pInfoObj;
}

CLkNaviPropOutDoorInfo* CLkNaviDataMng::Create_PropOutDoor( void )
{
	CAutoCS clCS( &m_cs );

	CLkNaviPropOutDoorInfo* pInfoObj = new CLkNaviPropOutDoorInfo;
	m_defCreatedObjList.push_back( pInfoObj );
	return pInfoObj;
}

CLkNaviPropInDoorInfo* CLkNaviDataMng::Create_PropInDoor( void )
{
	CAutoCS clCS( &m_cs );

	CLkNaviPropInDoorInfo* pInfoObj = new CLkNaviPropInDoorInfo;
	m_defCreatedObjList.push_back( pInfoObj );
	return pInfoObj;
}

void CLkNaviDataMng::Delete_NaviInfo( CLkNaviInfo*& pNaviInfo )
{
	CAutoCS clCS( &m_cs );

	listdef_NAVI_INFO_LIST::iterator it = m_defCreatedObjList.begin();
	for ( ; it != m_defCreatedObjList.end(); )
	{
		if ( *it == pNaviInfo )
		{
			delete *it;
			it = m_defCreatedObjList.erase( it );
			pNaviInfo = NULL;
		}
		else
		{
			++it;
		}
	}
}

void CLkNaviDataMng::Delete_AllNaviInfoExceptWorldInfo( void )
{
	CAutoCS clCS( &m_cs );

	listdef_NAVI_INFO_LIST::iterator it = m_defCreatedObjList.begin();
	for ( ; it != m_defCreatedObjList.end(); ++it )
	{
		delete *it;
	}
	m_defCreatedObjList.clear();
}

void CLkNaviDataMng::Delete_AllNaviInfo( void )
{
	Delete_AllNaviInfoExceptWorldInfo();

	EnterCriticalSection( &m_cs );

	if ( m_pCreatedWorldInfo )
	{
		delete m_pCreatedWorldInfo;
		m_pCreatedWorldInfo = NULL;
	}

	LeaveCriticalSection( &m_cs );
}

bool CLkNaviDataMng::Flush( void )
{
	EnterCriticalSection( &m_cs );

	if ( m_pCreatedWorldInfo && !m_pCreatedWorldInfo->Export( m_strPathName.c_str() ) )
	{
		return false;
	}

	listdef_NAVI_INFO_LIST::iterator it = m_defCreatedObjList.begin();
	for ( ; it != m_defCreatedObjList.end(); ++it )
	{
		CLkNaviInfo* pNaviInfo = *it;

		if ( pNaviInfo && !pNaviInfo->Export( m_strPathName.c_str() ) )
		{
			return false;
		}
	}

	LeaveCriticalSection( &m_cs );

	Delete_AllNaviInfoExceptWorldInfo();

	return true;
}

CLkNaviWorldInfo* CLkNaviDataMng::Load_World( void )
{
	CAutoCS clCS( &m_cs );

	m_pLoadedWorldInfo = new CLkNaviWorldOutDoorInfo;

	if ( m_pLoadedWorldInfo->Import( m_strPathName.c_str() ) )
	{
		return m_pLoadedWorldInfo;
	}

	delete m_pLoadedWorldInfo;

	m_pLoadedWorldInfo = new CLkNaviWorldInDoorInfo;

	if ( m_pLoadedWorldInfo->Import( m_strPathName.c_str() ) )
	{
		return m_pLoadedWorldInfo;
	}

	delete m_pLoadedWorldInfo;

	m_pLoadedWorldInfo = NULL;

	return m_pLoadedWorldInfo;
}

CLkNaviWorldOutDoorInfo* CLkNaviDataMng::Load_WorldOutDoor( void )
{
	CAutoCS clCS( &m_cs );

	m_pLoadedWorldInfo = new CLkNaviWorldOutDoorInfo;

	if ( !m_pLoadedWorldInfo->Import( m_strPathName.c_str() ) )
	{
		delete m_pLoadedWorldInfo;
		m_pLoadedWorldInfo = NULL;
	}

	return (CLkNaviWorldOutDoorInfo*)m_pLoadedWorldInfo;
}

CLkNaviWorldInDoorInfo* CLkNaviDataMng::Load_WorldInDoor( void )
{
	CAutoCS clCS( &m_cs );

	m_pLoadedWorldInfo = new CLkNaviWorldInDoorInfo;

	if ( !m_pLoadedWorldInfo->Import( m_strPathName.c_str() ) )
	{
		delete m_pLoadedWorldInfo;
		m_pLoadedWorldInfo = NULL;
	}

	return (CLkNaviWorldInDoorInfo*)m_pLoadedWorldInfo;
}

CLkNaviGroupOutDoorInfo* CLkNaviDataMng::Load_GroupOutDoor( unsigned int uiGroupID )
{
	CAutoCS clCS( &m_cs );

	CLkNaviGroupOutDoorInfo* pNaviObj = new CLkNaviGroupOutDoorInfo;

	pNaviObj->SetGroupID( uiGroupID );

	if ( !pNaviObj->Import( m_strPathName.c_str() ) )
	{
		delete pNaviObj;
		return NULL;
	}

	m_defLoadedObjList.push_back( pNaviObj );

	return pNaviObj;
}

CLkNaviGroupInDoorInfo* CLkNaviDataMng::Load_GroupInDoor( void )
{
	CAutoCS clCS( &m_cs );

	CLkNaviGroupInDoorInfo* pNaviObj = new CLkNaviGroupInDoorInfo;

	if ( !pNaviObj->Import( m_strPathName.c_str() ) )
	{
		delete pNaviObj;
		return NULL;
	}

	m_defLoadedObjList.push_back( pNaviObj );

	return pNaviObj;
}

CLkNaviPropOutDoorInfo* CLkNaviDataMng::Load_PropOutDoor( unsigned int uiFieldID, const char* pPath /*= NULL*/ )
{
	CAutoCS clCS( &m_cs );

	std::string strPath;
	
	if ( pPath )
	{
		strPath = pPath;
	}
	else
	{
		strPath = m_strPathName;
	}

	CLkNaviPropOutDoorInfo* pNaviObj = new CLkNaviPropOutDoorInfo;

	pNaviObj->SetFieldID( uiFieldID );

	if ( !pNaviObj->Import( strPath.c_str() ) )
	{
		delete pNaviObj;
		return NULL;
	}

	m_defLoadedObjList.push_back( pNaviObj );

	return pNaviObj;
}

CLkNaviPropInDoorInfo* CLkNaviDataMng::Load_PropInDoor( unsigned int uiBlockID, const char* pPath /*= NULL*/ )
{
	CAutoCS clCS( &m_cs );

	std::string strPath;

	if ( pPath )
	{
		strPath = pPath;
	}
	else
	{
		strPath = m_strPathName;
	}

	CLkNaviPropInDoorInfo* pNaviObj = new CLkNaviPropInDoorInfo;

	pNaviObj->SetBlockID( uiBlockID );

	if ( !pNaviObj->Import( strPath.c_str() ) )
	{
		delete pNaviObj;
		return NULL;
	}

	m_defLoadedObjList.push_back( pNaviObj );

	return pNaviObj;
}

void CLkNaviDataMng::Unload_NaviInfo( CLkNaviInfo*& pNaviInfo )
{
	CAutoCS clCS( &m_cs );

	if ( m_pLoadedWorldInfo == pNaviInfo )
	{
		delete m_pLoadedWorldInfo;

		m_pLoadedWorldInfo = NULL;

		pNaviInfo = NULL;

		return;
	}

	listdef_NAVI_INFO_LIST::iterator it = m_defLoadedObjList.begin();
	for ( ; it != m_defLoadedObjList.end(); )
	{
		if ( *it == pNaviInfo )
		{
			delete pNaviInfo;

			it = m_defLoadedObjList.erase( it );

			pNaviInfo = NULL;
		}
		else
		{
			++it;
		}
	}
}

void CLkNaviDataMng::Unload_AllNaviInfo( void )
{
	CAutoCS clCS( &m_cs );

	if ( m_pLoadedWorldInfo )
	{
		delete m_pLoadedWorldInfo;
		m_pLoadedWorldInfo = NULL;
	}

	listdef_NAVI_INFO_LIST::iterator it = m_defLoadedObjList.begin();
	for ( ; it != m_defLoadedObjList.end(); ++it )
	{
		delete *it;
	}
	m_defLoadedObjList.clear();
}

