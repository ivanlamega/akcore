//***********************************************************************************
//
//	File		:	LkLog.h
//
//	Begin		:	2006-01-05
//
//	Copyright	:	�� NTL-Inc Co., Ltd
//
//	Author		:	Hyun Woo, Koo   ( zeroera@ntl-inc.com )
//
//	Desc		:	
//
//***********************************************************************************

#pragma once

#include "LkSingleton.h"

#include "LkMiniDump.h"
#include "LkBase.h"
#include "LkString.h"
#include <list>
#include <crtdbg.h>


enum eLK_LOG_SOURCE
{
	LK_LOG_SOURCE_LOCAL = 0
};

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
enum eLK_LOG_CHANNEL
{
	LOG_FIRST = 0,

	LOG_GENERAL = LOG_FIRST,
	LOG_SYSTEM,
	LOG_WARNING,
	LOG_ASSERT,
	LOG_NETWORK,
	LOG_TRAFFIC,

	LOG_USER,

	LOG_LAST = LOG_USER - 1
};

struct sLOG_BRIEF
{
	int			nLine;
	CLkString	szFuntion;
	CLkString	szFile;
};

typedef std::list<sLOG_BRIEF*> LOGLIST;
typedef LOGLIST::iterator LOGLISTIT;

//-----------------------------------------------------------------------------------

LPCTSTR GetLogChannelString(BYTE byLogChannel);

//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
class CLkLogStream
{
public:

	CLkLogStream() {}

	virtual ~CLkLogStream() {}

};


//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
class CLkLogStream_FilePtr : public CLkLogStream
{
friend class CLkLog;

public:

	CLkLogStream_FilePtr(FILE *fp):m_fp(fp) {}

	virtual ~CLkLogStream_FilePtr() {}

private:

	FILE *				m_fp;
};
//-----------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------
class CLkLog : public CLkSingleton<CLkLog>
{
public:

	CLkLog() 
	{
		::InitializeCriticalSection(&m_loglock);
		m_dwDefaultLogSource = LK_LOG_SOURCE_LOCAL; 
		ResetList();

	}

	virtual ~CLkLog() 
	{
		ResetList();
		::DeleteCriticalSection(&m_loglock);
	}

public:

	virtual int			RegisterSource(DWORD dwSource, LPCTSTR lpszSourceName);

	virtual int			RegisterChannel(DWORD dwSource, BYTE byChannel, LPCTSTR lpszChannelName, LPCTSTR lpszFileNamePrefix, LPCTSTR lpszFileNameSuffix);

	virtual void		Log(BYTE byLogChannel, bool bDate, LPCTSTR lpszFile, int nLine, LPCTSTR lpszFunc, LPCTSTR lpszText, ...);


public:

	DWORD				GetDefaultLogSource() { return m_dwDefaultLogSource; }

	void				SetDefaultLogSource(DWORD dwDefaultLogSource);

	int					AttachLogStream(CLkLogStream & rLogStream);

	void				DetachLogStream();

	int					RegisterBaseChannel(DWORD dwSource, LPCTSTR lpszSourceName);

	void				ResetList();

	bool				IsNew(LPCTSTR lpszFile, int nLine, LPCTSTR lpszFunc);

public:

	static int			UnitTest();

private:

	CRITICAL_SECTION	m_loglock;

	DWORD				m_dwDefaultLogSource;
	LOGLIST				m_listLog;

};


//-----------------------------------------------------------------------------------
// D �� Date, L �� Location
//-----------------------------------------------------------------------------------
#define g_pLog CLkLog::GetInstance()
#define LK_LOG( LOGCHANNEL, LOGMSG, ... ) if( g_pLog ) g_pLog->Log( LOGCHANNEL, true, NULL, 0, NULL, LOGMSG, __VA_ARGS__ );
#define LK_LOGD( LOGCHANNEL, LOGMSG, ... ) if( g_pLog ) g_pLog->Log( LOGCHANNEL, true, NULL, 0, NULL, LOGMSG, __VA_ARGS__ )
#define LK_LOGL( LOGCHANNEL, LOGMSG, ... ) if( g_pLog ) g_pLog->Log( LOGCHANNEL, false, __FILE__, __LINE__, __FUNCTION__, LOGMSG, __VA_ARGS__ )
#define LK_LOGDL( LOGCHANNEL, LOGMSG, ... ) if( g_pLog ) g_pLog->Log( LOGCHANNEL, true, __FILE__, __LINE__, __FUNCTION__, LOGMSG, __VA_ARGS__ )

#if defined( _DEBUG ) && defined( _DEVEL )
	#define LK_LOG_ASSERT( LOGMSG, ... )																\
	{																									\
		if( g_pLog )																					\
			g_pLog->Log( LOG_ASSERT, true, __FILE__, __LINE__, __FUNCTION__, LOGMSG, __VA_ARGS__ );		\
		if( 1 == _CrtDbgReport( _CRT_ASSERT, __FILE__, __LINE__, __FUNCTION__, LOGMSG, __VA_ARGS__ ) )	\
			_CrtDbgBreak();																				\
	}
#elif defined( _DEBUG )
	#define LK_LOG_ASSERT( LOGMSG, ... )																\
	{																									\
		if( g_pLog )																					\
			g_pLog->Log( LOG_ASSERT, true, __FILE__, __LINE__, __FUNCTION__, LOGMSG, __VA_ARGS__ );		\
		if ( g_pLog->IsNew( __FILE__, __LINE__, __FUNCTION__ ) )										\
		{																								\
			CLkMiniDump::Snapshot();																	\
		}																								\
	}
#else
	#define LK_LOG_ASSERT( LOGMSG, ... )																\
	{																									\
		if( g_pLog )																					\
			g_pLog->Log( LOG_ASSERT, true, __FILE__, __LINE__, __FUNCTION__, LOGMSG, __VA_ARGS__ );		\
		if ( g_pLog->IsNew( __FILE__, __LINE__, __FUNCTION__ ) )										\
		{																								\
			CLkMiniDump::Snapshot();																	\
		}																								\
	}
#endif
//-----------------------------------------------------------------------------------